#ifndef CONFIGURATION_MANAGER_HPP
#define CONFIGURATION_MANAGER_HPP

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Enrico Sartori, Andrea Calanca
 * @date Feb 2019
 ****************************************************************************/

#include <map>
#include <stdexcept>
#include <typeinfo>

namespace selibdev
{

/**
 * \brief This class implements a Singleton that provides an access point to configure the internal parameters of the library from external code
 */
class ConfigurationManager
{
public:

    /** Enum used as dictionary keys for the library configuration
     * This enum contains the default configuration keys of the library.
     * It aims to configure the hardware parameters from the external code that includes the library
     */
    enum LibConfigs
    {
        ETHERCAT_CONN_NAME,

        LOG_PATH, 
        LOG_MAX_ROW, 
        
        COULOMB_FRICTION_P, 
        COULOMB_FRICTION_N, 
        VISCOUS_FRICTION_P, 
        VISCOUS_FRICTION_N, 
        
        CUR_SATURATION,

        STRAIN_GAUGE_GAIN,
        EXTERNAL_TORQUE_GAIN,
        KSPRING,
        LINK_INERTIA,

        MOTOR_INERTIA,
        MOTOR_TORQUE_CONST,

        STATIC_MOTOR_TORQUE_OFFSET,
        STATIC_STRAIN_GAUGE_OFFSET,
        STATIC_EXTERNAL_TORQUE_OFFSET
    };

    /**
     * Returns the singleton instance of the ConfigurationManager 
     */
    static ConfigurationManager * getInstance()
    { 
        static ConfigurationManager instance;

        return &instance; 
    }

    void setConfig(ConfigurationManager::LibConfigs key, std::string value);
    void setConfig(ConfigurationManager::LibConfigs key, double value);
    void setConfig(std::string key, std::string value);
    void setConfig(std::string key, double value);

    template<typename VALUE_T> 
    VALUE_T getConfig(ConfigurationManager::LibConfigs key)
    { return ( * getConfMap<ConfigurationManager::LibConfigs, VALUE_T>() )[key]; }

    template<typename VALUE_T> 
    VALUE_T getConfig(std::string key)
    { return ( * getConfMap<std::string, VALUE_T>() )[key]; }

protected:

    template<typename KEY_T, typename VALUE_T>
    std::map<KEY_T, VALUE_T> * getConfMap()
    {
        throw std::invalid_argument( std::string("Invalid combination of template types <") + std::string(typeid(KEY_T).name()) + std::string(", ") + std::string(typeid(VALUE_T).name()) + std::string(">") );
    }

    std::map<LibConfigs, std::string> stringLibConfigMap;
    std::map<LibConfigs, double> doubleLibConfigMap;
    std::map<std::string, std::string> stringUserConfigMap;
    std::map<std::string, double> doubleUserConfigMap;

private:

    ConfigurationManager() {}
    ~ConfigurationManager() {}
};

template<> std::map<ConfigurationManager::LibConfigs, std::string> * ConfigurationManager::getConfMap();
template<> std::map<ConfigurationManager::LibConfigs, double> * ConfigurationManager::getConfMap();
template<> std::map<std::string, std::string> * ConfigurationManager::getConfMap();
template<> std::map<std::string, double> * ConfigurationManager::getConfMap();

}

#endif // CONFIGURATION_MANAGER_HPP
