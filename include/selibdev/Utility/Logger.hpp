#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <vector>
#include <map>

namespace selibdev
{

/**
\brief This virtual class describes a generic Logger. 
It implements the management of records to log.
The storing of such records is let to childs.
*/
class Logger
{
public:

    Logger();
    virtual ~Logger() {}

    void saveLoggableRefs(std::map<std::string, const double *> * loggable_map); ///< save the reference of the variables to log

    virtual void logOpen() = 0;

    virtual void log();

    virtual void logClose();

    std::string loggerName, loggerEntityName, loggerProperties;

    std::vector<std::string> loggerKeys;

    bool loggerEnabled;

    int decimationFactor;

    bool averagingEnabled;

protected:

    std::vector<double> record;

    unsigned int logCounter; // Incremented each time a record is logged (it consider the decimation)

    bool loggerClosed;
    
    bool recordIsReady;

private:

    const double default_value = 0.0;

    std::vector<const double *> refs;

    std::vector<double> decimationRecord;

    unsigned int privateLogCounter; // Incremented each time log function is called
};

}

#endif // LOGGER_HPP