#ifndef LOGGABLE_H
#define LOGGABLE_H

#include "ConfigurationManager.hpp"
#include "MemoryLogger.hpp"

#include <map>
#include <vector>

namespace selibdev
{

/**
\brief This virtual class make a generic class a loggable class.
It allows to handle a map containing the reference to the loggable variable.
It provides a default logger that save all the variables added to the loggable_map.
*/
class Loggable
{
public:

    Loggable();
    virtual ~Loggable();

    const double * getEntryDouble(std::string key); ///< return the reference to the loggable variable
    
    void setLogger(selibdev::Logger *logger); ///< set a customized logger. When this method is called, the logger must contain the key of the variables to log.
    
    selibdev::Logger * getLogger(); ///< return the current logger (if a custom logger is not set the default one is given)
    
protected:

    virtual std::string cb_getLoggerEntityNameString(); ///< This method must be overrided by child classes, it sets the logger name string that will be part of the file name
    
    virtual std::string cb_getLoggerPropertiesString(); ///< This method must be overrided by child classes, it sets the properties string that will be part of the file name
    
    void addEntryDouble(std::string key, const double * ref); ///< add a variable to the loggable map, must be used in the constructors.
    
    void setLogger(); ///< Initialization of the default logger
    
private: 

    std::string getLoggerEntityNameString(); ///< This method must be overrided by child classes, it sets the logger name string that will be part of the file name
    
    std::string getLoggerPropertiesString(); ///< This method must be overrided by child classes, it sets the properties string that will be part of the file name
    
    selibdev::Logger * logger;

    bool isLoggerSet;

    std::map<std::string, const double *> loggable_map;
};

}

#endif // LOGGABLE_H