#ifndef MEMORY_LOGGER_HPP
#define MEMORY_LOGGER_HPP

#include "FileLogger.hpp"

namespace selibdev
{

/**
\brief This class describes a memory logger. 
It keep in memory an arbitrary number of records, that will be written in a csv file when the logger is closed.
*/
class MemoryLogger : public selibdev::FileLogger
{
public: 

    MemoryLogger(int mem_ROW_MAX) 
        : selibdev::FileLogger(),
          writeOnOutOfMemory(false)
        {
            this->mem_ROW_MAX = mem_ROW_MAX;

        }

    virtual ~MemoryLogger() {}

    virtual void logOpen() override; ///< create the matrix that contains the log records 

    virtual void log() override; ///< add the log record to the matrix in memory

    virtual void logClose() override; ///< write the matrix in a csv file

    bool writeOnOutOfMemory;

protected:
    void memoryFlush();

    unsigned int mem_COL_MAX, mem_ROW_MAX;

    std::vector<double> memLogData;

    bool isMemLogOpened;
};

}

#endif // MEMORY_LOGGER_HPP
