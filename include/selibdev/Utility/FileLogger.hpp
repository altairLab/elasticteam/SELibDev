#ifndef FILE_LOGGER_HPP
#define FILE_LOGGER_HPP

#include <fstream>

#include "../defines.h"
#include "Logger.hpp"

namespace selibdev
{

/**
\brief This class describes a csv file logger. 
It implements the storing of log records in a csv file.
*/
class FileLogger : public selibdev::Logger
{
public: 

    FileLogger() : selibdev::Logger() {}

    virtual ~FileLogger() {}

    virtual void logOpen() override; ///< open the file where the records will be saved
    
    virtual void log() override; ///< write the record in the log file
    
    virtual void logClose() override; ///< close the log file

    std::string loggerFilePath;

protected:

    std::string fileName, fullPath;

    std::ofstream logFile;

};

}

#endif // FILE_LOGGER_HPP