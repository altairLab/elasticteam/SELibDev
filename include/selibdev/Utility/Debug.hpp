#ifndef DEBUG_HPP
#define DEBUG_HPP

#include <iostream>
#include "../defines.h"

namespace selibdev
{

#if SELIB_TARGET==SELIB_TARGET_LINUX

#define debug_info(msg) (std::cout << msg << std::endl)
#define debug_error(msg) (std::cerr << msg << std::endl)

#elif SELIB_TARGET==SELIB_TARGET_EMBEDDED

#define debug_info(msg)
#define debug_error(msg)

#endif

}

#endif // DEBUG_HPP