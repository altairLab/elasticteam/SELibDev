#ifndef _HARDWARE_H
#define _HARDWARE_H

#include "Hardware/Ethercat/el1202.hpp"
#include "Hardware/Ethercat/el1252.hpp"
#include "Hardware/Ethercat/el2202.hpp"
#include "Hardware/Ethercat/el3008.hpp"
#include "Hardware/Ethercat/el3102.hpp"
#include "Hardware/Ethercat/el4004.hpp"
#include "Hardware/Ethercat/el5152.hpp"
#include "Hardware/Ethercat/elXXXX.hpp"
#include "Hardware/Ethercat/ethercatBoards.hpp"
#include "Hardware/Ethercat/RTRunner.hpp"
#include "Hardware/Ethercat/SEHardware.hpp"
#include "Hardware/ISEHardware.hpp"

#endif // _ETHERCAT_MODULES_H

