#ifndef ANALOGFILTER_H
#define ANALOGFILTER_H

#include <iostream>
#include <math.h>
#include <vector>

#include "../defines.h"
#include "Filter.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Davide Ciocca, Andrea Calanca
 * @date Oct 2016
 ****************************************************************************/

namespace selibdev
{

/**
\brief This class implements a digital filter
This class implements a digital filter using the matlab notation:
 b -> array of numerator coefficients of L transfer function
 a -> array of denominator coefficients of L transfer function
The left value of arrays a and b are the z^n coefficients es.:
[a2 a1 a0] -> a2*s^2 + a1*s + a0
Remark 1 : the maximum filter order is set by the static proprerty MAX_ORDER (= 8 by default)
*/
class AnalogFilter : public Filter
{
public:
    /** \brief Analog Filter constructor based on Laplace transfer function coefficients
        \param order Order of the filter (the maximum filter order is set by the static property MAX_ORDER = 8 by default)
        \param a array of denominator coefficients of Laplace transfer function
        \param b array of numerator coefficients of Laplace transfer function
    */
    AnalogFilter(int order, double *a, double *b);
    virtual ~AnalogFilter() {}

    double process(double input, double h); ///< data process, can be used even in the presence of jitter
    void clean(); ///< cleans up the filter internal states

    static AnalogFilter* getDifferentiatorHz(double f); ///< builds an approximate differentiator filtering with cut-off frequency f [Hz]
    static AnalogFilter* getDifferentiatorHz2(double f); ///< builds an approximate differentiator filtering with cut-off frequency f [Hz] (second order low pass)
    

private:
    int order; ///< the filter order

    std::vector<double> alpha;
    std::vector<double> beta;
    std::vector<double> beta_hat;

    std::vector<double> x;
    std::vector<double> x_old;
    std::vector<double> f;

    double y;

    void clean_F(); ///< cleans up the derivative vector F
};

}

#endif // ANALOGFILTER_H
