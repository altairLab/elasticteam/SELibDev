#ifndef DOBControlOL_H
#define DOBControlOL_H


#include "ControlBase.hpp"
#include "AnalogFilter.hpp"


/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Davide Ciocca, Andrea Calanca
 * @date Oct 2016
 ****************************************************************************/

namespace selibdev
{

/**
\brief Base class for DOB force control of Series Elastic Actuators.
*/
class DOBControlBase : public selibdev::SEControl
{
    public:
        double KP, KD, FF;
        bool dynamicFFEnabled;

        DOBControlBase(double KP, double KD, double QcutoffHz, selibdev::ISEHardware *hw);
        virtual ~DOBControlBase() {
            delete Q_filter;
            delete PninvQ_filter;
            delete PninvQ_filterFF;
        }

    protected:
        double QcutoffHz;
        double xi_q, w_q, w_q2;
        double Jl, dm, de;

        double *a_Q, *b_Q;
        double *a_PninvQ, *b_PninvQ;
        selibdev::AnalogFilter *Q_filter, *PninvQ_filter, *PninvQ_filterFF;

        double d_hat;

        virtual std::string cb_getLoggerPropertiesString() override;
};


/**
\brief DOB force controller for Series Elastic Actuators. It implemants the open loop configuration proposed in [1] considering a second order nominal model which account for the motor dynamics only

[1]
*/
class DOBControlOL : public DOBControlBase {
    public:
        DOBControlOL(double KP, double KD, double QcutoffHz, selibdev::ISEHardware *hw);
        virtual ~DOBControlOL() {}

        double ___process(double deltaTime);

    private:
        double Ud, Ud_prev, err, derr;

        virtual std::string cb_getLoggerEntityNameString() override;
};


/**
\brief DOB force controller for Series Elastic Actuators. It implemants the open loop configuration proposed in [1] considering a third order nominal model which account for the motor dynamics and the link inertia

[1]
*/
class DOBControlOL3order : public DOBControlBase
{
    public:

        DOBControlOL3order(double KP, double KD, double QcutoffHz, selibdev::ISEHardware *hw);
        virtual ~DOBControlOL3order() { delete PninvQ_FFfilter; }

        double ___process(double deltaTime);

    protected:
        selibdev::AnalogFilter *PninvQ_FFfilter;
        
        double Ud, d_hat, err, derr;
        double Ud_prev;

        virtual std::string cb_getLoggerEntityNameString() override;
};
}

#endif // DOBControlOL_H
