#ifndef ENERGYCONTROL_H
#define ENERGYCONTROL_H

#include "../defines.h"
#include "ControlBase.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

namespace selibdev
{


/**
This class implements the energy control law used in
A. Calanca and P. Fiorini, On The Maximum Efficiency Of Elastic Actuators
The control law injects power into the system untill a desired level of internal energy is reached
\brief This class implements the energy control law
*/
class EnergyControl: public SEControl
{
    public:
        EnergyControl(ISEHardware* hw);
		~EnergyControl(){delete filter;};

		double ___process(double dt);
		double energy_ref, k_e;

    protected:
        double energy, e, kv, out, w;
        double mu, dl, dlc, dm, dmc, fcomp;
        DigitalFilter* filter;

};

}

#endif // ENERGYCONTROL_H
