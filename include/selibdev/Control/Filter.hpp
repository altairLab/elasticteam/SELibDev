#ifndef FILTER_HPP
#define FILTER_HPP

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Enrico Sartori, Andrea Calanca
 * @date Feb 2019
 ****************************************************************************/

namespace selibdev
{

/**
 * \brief This class describes the interface of a generic Filter, it can be Analog or Digital 
 */
class Filter
{
public:

    Filter() {}

    virtual ~Filter() {}

    /**
     * \param input The input of the filter
     * \param h the time step
     */
    virtual double process(double input, double h) = 0;

    virtual void clean() = 0;

};

}

#endif // FILTER_HPP