#ifndef SEAIMPEDANCECONTROL_H
#define SEAIMPEDANCECONTROL_H

#include "ControlBase.hpp"
#include "SlidingMode.hpp"
#include "PassiveControl.hpp"
#include "AnalogFilter.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

namespace selibdev
{

// unused. implemented as a child of SEImpedanceControlBase

///**
//Base class for impedance control algorithms on stiff joi9nts. It provides desired stifness and damping proprierties and common log fonctionality for child impedance control algorithms (common file naming and data streaming)
//*/
//class ImpedanceControlBase: public MotorControlBase
//{
//    public:
//        ImpedanceControlBase(IMotorHardware* hw);
//        virtual ~ImpedanceControlBase() {}
//        virtual double __process(double theta, double dtheta, double dt);

//        void LogOpen();
//        void Log();

//        double k_des; ///< desired stiffness
//        double d_des; ///< desired damping
//        double theta_ref;
//        double tau_ref;

//    protected:
//        double log1, log2, log3;
//};



/**
\brief Base class for impedance control algorithms on SEAs. It provides desired stifness and damping proprierties and common log fonctionality for child impedance control algorithms (common file naming and data streaming)
*/

class SEImpedanceControlBase: public SEControl
{
	public:
        SEImpedanceControlBase(ISEHardware* hw);
		virtual ~SEImpedanceControlBase() {}

        double k_des; ///< desired stiffness
		double d_des; ///< desired damping
        double theta_ref; ///< desired equilibrium position
        double tau_ref; ///< desired offset torque
        double mgb_gravity_comp; ///< gravity compensation os computed as mgb * sin(theta_e); mgb:=  mass * 9.81 * barycenter_arm
};

/**
Under test - works only for direct drive motor
*/
class TrivialImpedanceControl: public SEImpedanceControlBase
{
	public:
        TrivialImpedanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw) {}
		virtual ~TrivialImpedanceControl() {}

		double ___process(double dt);
	private:
};

/**
This class implements admittance control for stiff joints (the spring is not there!). It is based on an inner PD force control, using the PID class.
*/
class StiffAdmittanceControl: public SEImpedanceControlBase
{
public:
	// StiffAdmittanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw), pid(0,50,1) {}
    StiffAdmittanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw), pid(0,20,1) {}
	// StiffAdmittanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw), pid(0,10,1) {}
	virtual ~StiffAdmittanceControl() {}

	double ___process(double dt);
    PID pid;
    AnalogFilter *A_over_s_ref;
    AnalogFilter *A_over_s_dref;
};

///**
//This class implements admittance control for stiff joints (the spring is not there!). It is based on an inner PD force control, using the PID class.
//*/
//class StiffImpedanceControl: public SEImpedanceControlBase
//{
//    public:
//    StiffImpedanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw), pid(0,5,0) {}
////    StiffImpedanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw), pid(0,20,0) {}
//    double ___process(double dt);
//    PID pid;
//};




/**
This class implements impedance control based on an inner PD force control, using the PID class.
*/
class BasicImpedanceControl: public SEImpedanceControlBase
{
public:
	BasicImpedanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw), pid(0,2,0, hw) {}
    // BasicImpedanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw), pid(0,2,0, hw) {}
	// BasicImpedanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw), pid(0,4,0, hw) {}
	// BasicImpedanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw), pid(0,10,0.7, hw) {}
	// BasicImpedanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw), pid(0,100,1.5, hw) {}
	// BasicImpedanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw), pid(0,50,1, hw) {}
	virtual ~BasicImpedanceControl() {}

	double ___process(double dt);
	
	// PDControlWithDampingInjection pid;
    MegaPDForceControl pid;
};


/**
This class implements impedance control based on an inner PD force control, using the PID class.
*/

class CollocatedImpedanceControl: public SEImpedanceControlBase
{
public:
	// CollocatedImpedanceControl(ISEHardware* hw) : ImpedanceControlBase(hw), pid(0,10,1) {}
	// CollocatedImpedanceControl(ISEHardware* hw) : ImpedanceControlBase(hw), pid(0,50,1) {}
	// CollocatedImpedanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw), pid(0,20,1) {}
	CollocatedImpedanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw), pid(0,20,1) {}
	virtual ~CollocatedImpedanceControl() {}
	
	double ___process(double dt);
	
	PID pid;

private:
	double k_des_m;
};


/**
This class implements impedance control based on an inner PD force control, using the PID class.
*/
class CollocatedAdmittanceControl: public SEImpedanceControlBase
{
public:
	CollocatedAdmittanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw), pid(0,100,1)
	{
		A_over_s_ref = NULL;
		A_over_s_dref = NULL;
	}
	// CollocatedAdmittanceControl(ISEHardware* hw) : ImpedanceControlBase(hw), pid(0,50,1) {}
	virtual ~CollocatedAdmittanceControl() {}

	double ___process(double dt);
    AnalogFilter *A_over_s_ref;
    AnalogFilter *A_over_s_dref;
	PID pid;
};




/**
This class implements impedance control based on an inner sliding-mode force control
*/
class SMImpedanceControl: public SEImpedanceControlBase
{
public:
	SMImpedanceControl(ISEHardware* hw); ///< Edit here set the specific SM algorithm
	virtual ~SMImpedanceControl() {}

	double ___process(double dt);
	SlidingModeForceControlBase* c;
};


/**
This class implements impedance control based on an passive force control with inner motor velocity feedback. Passivity analisis of this algorithm is reported in

H. Vallery, J. Veneman, E. H. F. van Asseldonk, R. Ekkelenkamp, M. Buss, and H. van Der Kooij, "Compliant actuation of rehabilitation robots", IEEE Robot. Autom. Mag., vol. 15, no. 3, pp. 60-69, Sep. 2008.
*/
class VelocitySourcedImpedanceControl: public SEImpedanceControlBase
{
public:
	VelocitySourcedImpedanceControl(ISEHardware* hw);
	virtual ~VelocitySourcedImpedanceControl() {}

	double ___process(double dt);
	PassiveValleryForceControl* c;
};

}

#endif // SEAIMPEDANCECONTROL_H
