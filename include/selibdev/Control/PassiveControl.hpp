#ifndef PASSIVECONTROL_H
#define PASSIVECONTROL_H

#include "ControlBase.hpp"
#include "ModelReference.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

namespace selibdev
{

/**
This class implements force control by using a #PID with integral roll off
*/
class PassivePIDForceControl: public SEControl
{
public:
    PassivePIDForceControl(ISEHardware* hw);
    virtual ~PassivePIDForceControl() { delete iFilter; }

    double ki, kp, kd, ff;

protected:
    double err, derr, ierr;
	DigitalFilter* iFilter;
    //virtual double __process(double tau, double dtau, double dt) override;
    virtual double ___process(double dt) override;
};

/**
This class implements passive force control by using a #PID with zero integrator and damping injection
*/
class PDForceControlWithDampingInjection: public SEControl
{
public:
    PDForceControlWithDampingInjection(ISEHardware* hw);
    virtual ~PDForceControlWithDampingInjection() {}

    double kp, kd, ff, kinj;

protected:
    double err, derr;

    virtual double ___process(double dt) override;
};

/**
This class implements a force control algorithm based on positive load accelleration feedback. It was first proposed in

[1] G. A. Pratt and M. M. Williamson, Series Elastic Actuators, in International Conference on Intelligent Robots and Systems, 1995, vol. 1, pp. 399-406.

*/
class PassivePrattForceControl: public SEControl
{
public:
    PassivePrattForceControl(double l1, double l2, ISEHardware* hw);
    virtual ~PassivePrattForceControl() {}

    double kb;
protected:
	double kp, kd, out, err, derr;
    virtual double ___process(double dt) override;
};


class MRPassivePrattForceControl: public PassivePrattForceControl, public ModelReference2
{
public:
	MRPassivePrattForceControl(double l1, double l2, ISEHardware* hw);
    virtual ~MRPassivePrattForceControl() {} //do do mi piacerebbe chiamare direttamente i distruttori

protected:
    virtual double ___process(double dt) override;
};

/**
This class implements the force control algorithm proposed in

[1] H. Vallery, R. Ekkelenkamp, H. van der Kooij, and M. Buss, Passive and accurate torque control of series elastic actuators,2007 IEEE/RSJ Int. Conf. Intell. Robot. Syst., pp. 3534-3538, Oct. 2007.

It uses an inner motor velocity feedback
*/
class PassiveValleryForceControl: public PassivePIDForceControl
{
public:
    PassiveValleryForceControl(ISEHardware* hw);
    virtual ~PassiveValleryForceControl() { delete v; }

    double kiv, kpv, kif, kpf, kdf;

protected:
	double derr, err, ierr;
    PID *v;

    virtual double ___process(double dt) override;
};

}

#endif // PASSIVECONTROL_H
