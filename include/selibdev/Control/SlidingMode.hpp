#ifndef SLIDINGMODE_H
#define SLIDINGMODE_H

#include <sstream>
#include <string>


#include "../defines.h"
#include "ControlBase.hpp"
#include "DigitalFilter.hpp"
#include "AnalogFilter.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

namespace selibdev
{

/**
 * \brief This class implements the base to extend to implement a Sliding Mode control algorithm.
 */
class SlidingModeForceControlBase : public SEControl
{
public:
    /**
     * \param lambda A control parameters that depends by the bandwidth frequency
     * \param hw A pointer to an ISEHardware object
     */
    SlidingModeForceControlBase(double lambda, ISEHardware* hw);
    virtual ~SlidingModeForceControlBase() {delete ddFilter;}

    /** This method returns the distance of the system position in the phase plan from the sliding surface
     * \param err Error of the current control value from the reference value
     * \param derr Derivate of the error
     */
    virtual double slidingSurface(double err,double derr) =0;

    /** An abstract method to implement the control law of a sliding mode controller.
     */
    virtual double controlLaw() =0;

    double lambda1;
    double ni; ///< discontunuous gain
    double phi; ///< sliding boundary
    double ka; ///< acceleration feedback gain
    double kp, kd;

protected:

    double err, derr, s;
	double us;
	DigitalFilter* res;
	DigitalFilter* ddFilter;

    double deb1, deb2, deb3;

    virtual double ___process(double dt) override;
};


/**
\brief This class implements the sliding-mode control law proposed in

A. Calanca, L. Capisani, and P. Fiorini, Robust Force Control of Series Elastic Actuators, Actuators, Spec. Issue Soft Actuators, vol. 3, no. 3, pp. 182 204, 2014.

to control a SEA force/torque
*/
class SlidingModeForceControl : public SlidingModeForceControlBase
{
public:
	SlidingModeForceControl(double lambda, ISEHardware* hw);
    virtual ~SlidingModeForceControl() {}
    virtual double slidingSurface(double err, double derr) override;
    virtual double controlLaw() override;
protected:
};

/**
This class implements the integral sliding-mode control law proposed in

A. Calanca, L. Capisani, and P. Fiorini, Robust Force Control of Series Elastic Actuators, Actuators, Spec. Issue Soft Actuators, vol. 3, no. 3, pp. 182 204, 2014.

to control a SEA force/torque
*/
class IntegralSlidingModeForceControl : public SlidingModeForceControlBase
{
public:
    /**
     * \param lambda A control parameters that depends by the bandwidth frequency
     * \param hw A pointer to an ISEHardware object
     */
	IntegralSlidingModeForceControl(double l1, double l2, ISEHardware* hw);
    virtual ~IntegralSlidingModeForceControl() { delete res; }

    /** This method returns the distance of the system position in the phase plan from the sliding surface
     * \param err Error of the current control value from the reference value
     * \param derr Derivate of the error
     */
    virtual double slidingSurface(double err,double derr) override;

    /** Control law implementation of the Integral Sliding Mode Control.
     */
    virtual double controlLaw() override;

    double lambda2;
	double damp,freq;
    
protected:
	double ierr;
	DigitalFilter* res;
};

/**
This class implements the super twisting sliding-mode control law used in
A. Calanca, L. Capisani, and P. Fiorini, Robust Force Control of Series Elastic Actuators, Actuators, Spec. Issue Soft Actuators, vol. 3, no. 3, pp. 182 204, 2014.
to control a SEA force/torque

\brief This class implements the super twisting sliding-mode control
*/
class SuperTwistingForceControl : public SEControl
{
public:
    /**
     * \param lambda A control parameters that depends by the bandwidth frequency
     * \param hw A pointer to an ISEHardware object
     */
    SuperTwistingForceControl(double lambda, ISEHardware* hw);
    virtual ~SuperTwistingForceControl() {}

	//parameters
    double ki,ni,rho, a;

protected:

	//errors
    double err,derr,ierr,s,lambda;
	//auxiliary variable
	double ua, dua, utw;

	double prev_dtheta_e, ddtheta_e, ddtheta_ef;
	DigitalFilter* ddFilter;
    
    virtual double ___process(double dt) override;
};

}

#endif // SLIDINGMODE_H
