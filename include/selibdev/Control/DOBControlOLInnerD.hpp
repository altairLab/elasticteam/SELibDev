#ifndef DAMPED_DOB_CONTROL_OL_H
#define DAMPED_DOB_CONTROL_OL_H

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Enrico Sartori, Andrea Calanca
 * @date Feb 2019
 ****************************************************************************/

#include "ControlBase.hpp"
#include "AnalogFilter.hpp"

#include "DOBControlOL.hpp"

namespace selibdev
{

/**
 * \brief This class implements an Open-Loop DOB control where the plant is forced to a simpler dominant dynamics by a high inner derivative gain
 */
class DOBControlOLInnerD: public DOBControlBase {
    
    public:

        /**
         * \param Kp A proportional gain
         * \param Kd A derivative gain
         * \param QcutoffHz A DOB filter frequency
         * \param alpha A [0:1] constant that rules the influence of DOB in the forward control path
         * \param hw A pointer to an ISEHardware object
         */
        DOBControlOLInnerD(double Kp, double Kd, double QcutoffHz, double alpha, selibdev::ISEHardware *hw);
        virtual ~DOBControlOLInnerD() {}
    
        double ___process(double deltaTime);

    protected:
        
        double alpha;
        
        double U_dob, U_dob_prev, err;


        virtual std::string cb_getLoggerEntityNameString() override;
        virtual std::string cb_getLoggerPropertiesString() override;
};

}

#endif // DAMPED_DOB_CONTROL_OL_H
