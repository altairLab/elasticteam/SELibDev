#ifndef MODEL_REFERENCE_HPP
#define MODEL_REFERENCE_HPP

namespace selibdev
{

/**
This class implements a 2nd order reference model:  ddx + l1*dx +l2*x = l2*r
where r is the input reference and x is the model output
*/
class ModelReference2
{
public:
    ModelReference2(double l1, double l2);
	virtual ~ModelReference2() {}

    //to be set at the beginning
    double l1, l2;

protected:
    void MRUpdate(double ref, double dt);

    //model reference variables
    double f1, f2, xr1, xr2, prev_xr1, prev_xr2;

};

}

#endif // MODEL_REFERENCE_HPP