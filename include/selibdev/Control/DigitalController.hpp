#ifndef DIGITALCONTROLLER_H
#define DIGITALCONTROLLER_H

#include "DigitalFilter.hpp"
#include "ControlBase.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

namespace selibdev
{


/**
 * \brief A digital controller class, nothing else than a digital filter
*/
class DigitalControl: public DigitalFilter, public ControlBase
{
public:
    DigitalControl(int order, double* a, double* b) : DigitalFilter::DigitalFilter(order, a, b) {}
    virtual ~DigitalControl();
    
    double __process(double y, double unused1, double unused2);
};

}

#endif // DIGITALCONTROLLER_H
