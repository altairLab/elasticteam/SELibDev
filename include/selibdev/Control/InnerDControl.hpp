#ifndef INNER_D_CONTROL_HPP
#define INNER_D_CONTROL_HPP

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Enrico Sartori, Andrea Calanca
 * @date Feb 2019
 ****************************************************************************/

#include "ControlBase.hpp"
#include "SlidingMode.hpp"

namespace selibdev
{

/****************** PinnerD ************************/   

/**
 * \brief This class implements a controller characterized by a proportional gain on the forward path and a derivative gain on the feedback.
 */
class PinnerD : public selibdev::SEControl
{
public:

    /**
     * \param hw A pointer to an ISEHardware object
     */
    PinnerD(selibdev::ISEHardware *hw);

    /**
     * \param Kp A proportional gain
     * \param Kd An inner derivative gain
     * \param hw A pointer to an ISEHardware object
     */
    PinnerD(double kP, double kd, selibdev::ISEHardware *hw);
    virtual ~PinnerD() {}

    double kP;
    double kd;
    double ff;
    
protected:

    virtual double ___process(double dt) override;

    virtual std::string cb_getLoggerEntityNameString() override;
    virtual std::string cb_getLoggerPropertiesString() override;

    selibdev::Filter *pz_comp;

private:
    double _err;
};

/****************** SMCinnerD ***********************/

/**
 * \brief This class implements a Sliding Mode Controller with an inner derivative gain
 */
class SMCinnerD: public selibdev::SlidingModeForceControlBase
{
public:
    /**
     * \param k_damp An inner derivative gain
     * \param lambda A lambda value, it depends to the bandwidth frequency
     * \param hw A pointer to an ISEHardware object
     */
    SMCinnerD(double k_damp,  double lambda, selibdev::ISEHardware* hw);
    virtual ~SMCinnerD() {}
    virtual double slidingSurface(double err, double derr);
    virtual double controlLaw();

protected:
    double ierr, alpha, err_prev;
    
    virtual std::string cb_getLoggerEntityNameString() override;
    virtual std::string cb_getLoggerPropertiesString() override;

};

/****************** PinnerPD ************************/

/**
 * \brief This class implements a PinnerD controller augmented with a further proportional gain on the feedback path. 
 * It aims to enhance the control bandwith.
 */
class PinnerPD : public selibdev::SEControl
{
public:
    /**
     * \param hw A pointer to an ISEHardware object
     */
    PinnerPD(selibdev::ISEHardware *hw);
    /**
     * \param kP An outer proportional gain
     * \param kp An inner proportional gain
     * \param kd An inner derivative gain
     * \param hw A pointer to an ISEHardware object
     */
    PinnerPD(double kP, double kp, double kd, selibdev::ISEHardware *hw);
    virtual ~PinnerPD() {}

    double kP; ///< An outer proportional gain
    double kp; ///< An inner proportional gain
    double kd; ///< An inner derivative gain
    double ff; ///< The feed forward gain
    
protected:
    virtual double ___process(double dt) override;

    virtual std::string cb_getLoggerEntityNameString() override;
    virtual std::string cb_getLoggerPropertiesString() override;

private:
    double _err;
};

}

#endif // INNER_D_CONTROL_HPP