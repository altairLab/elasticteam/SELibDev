#ifndef DOBControlCL_H
#define DOBControlCL_H


#include "ControlBase.hpp"
#include "AnalogFilter.hpp"
#include "DOBControlOL.hpp"

#include <sstream>
#include <string>

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Davide Ciocca, Andrea Calanca
 * @date Oct 2016
 ****************************************************************************/

namespace selibdev
{

/**
\brief DOB force controller for Series Elastic Actuators. It implemants the closed loop configuration proposed in [1] considering a second order nominal model which account for the motor dynamics only

[1]
*/
class DOBControlCL : public DOBControlBase {
    public:
        DOBControlCL(double KP, double KD, double QcutoffHz, selibdev::ISEHardware *hw);
        virtual ~DOBControlCL() {}

        double ___process(double deltaTime);

    private:
        double err, derr, rd_prev;

        virtual std::string cb_getLoggerEntityNameString() override;
};


/**
\brief DOB force controller for Series Elastic Actuators. It implemants the closed loop configuration proposed in [1] considering a third order nominal model which account for the motor dynamics and the link inertia

[1]
*/
class DOBControlCL3order : public DOBControlBase
{
    public:
        DOBControlCL3order(double KP, double KD, double QcutoffHz, selibdev::ISEHardware *hw);
        virtual ~DOBControlCL3order() {}

        double ___process(double deltaTime);

    private:
        double rd_prev;
        double U, d_hat, err, derr;
        
        virtual std::string cb_getLoggerEntityNameString() override;
};
}

#endif // DOBControlCL_H
