#ifndef _CONTROL_BASE_HPP
#define _CONTROL_BASE_HPP

#include "../defines.h"
#include "DigitalFilter.hpp"
#include "../Hardware/ISEHardware.hpp"
#include "../Utility/Loggable.hpp"
#include "../Utility/MemoryLogger.hpp"

#include <cmath>
#include <iostream>
#include <sstream>
#include <cstring>
#include <fstream>
// #include <Eigen/Dense>


/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2016
 ****************************************************************************/

namespace selibdev
{

/** TorqueSensing enumerator
 * This enumerator allows to select the torque sensor to use
 */
enum TorqueSensing
{
   fromMotorSensor,
   fromSpring,
   fromLinkSensor
};

/**
 * \brief This virtual class describes a generic Controller. The control law should be implemented by childs in the virtual _process function. The class also provides an automatic logging functionality by overriding the member log.
*/
class ControlBase : public selibdev::Loggable
{
public:

    ControlBase();
    virtual ~ControlBase() {}

    virtual void init() {}
    double process(double theta, double dtheta, double dt);
    virtual void dispose();

    void refSaturation( double lowThreshold, double upThreshold);

    static double saturation(double in, double threshold);
    static double saturation(double in,  double lowThreshold, double upThreshold);

    double          ref, dref, ddref;

protected:
    /// Implements template method pattern for process function.
    virtual double _process(double theta, double dtheta, double dt) =0;

    double          time;   ///< the (real-) time variable automatically updated every dt
    double          out;    ///< just for child convenience

private:
    double          _ret;
};

/**
 * \brief This virtual class describes a controller for rigid joint i.e. a motro joint. The control law should be implemented by childs in the virtual __process function. The class provides motor position, velocity and torque members and an additional torque observer in the case the the torque sensor is missing. Also it provides friction compensation utilities.
*/
class MotorControlBase : public selibdev::ControlBase
{
public:
	MotorControlBase(IMotorHardware *hw); ///< constructor. By default  accellerations are filtered (50Hz at Ts=0.333ms) while position and velocityes are not processed
    virtual ~MotorControlBase();

    double frictionCurrentCompensationColoumb(double velocity) { return frictionTorqueCompensationColoumb(velocity) / Kt; }

    double frictionTorqueCompensationColoumb(double velocity)
    {
        return std::signbit(velocity) ? -hw->coulombFrictionN : hw->coulombFrictionP;
    }

    double frictionCurrentCompensation(double velocity) { return frictionTorqueCompensation(velocity) / Kt; }

    double frictionTorqueCompensation(double velocity)
    {
        return std::signbit(velocity) ? -hw->coulombFrictionN - hw->viscousFrictionN*fabs(velocity) : hw->coulombFrictionP + hw->viscousFrictionP*fabs(velocity);
    }

    double frictionCurrentCompensation() { return frictionCurrentCompensation(dtheta_m); }

    double frictionTorqueCompensation() { return frictionTorqueCompensation(dtheta_m); }

    IMotorHardware  *hw; ///< where to retrieve the sensor data

    double          Kt; ///< motor torque constant (just for ease of use !!)
    double          Jm; ///< reflected motor inertia, including the gear ratio. (just for ease of use !!)

    double          theta_m, dtheta_m, ddtheta_m, jerk_m; ///< for ease of use
    double          tau, dtau, ddtau; ///< for ease of use
    double          tau_motor, tau_link, tau_ext, current; ///< just for logging

    double          smooth_dtau;

    TorqueSensing torqueSensing;

protected:

    double          torqueObserver, diffTorqueObserver;

    DigitalFilter   *jerkMDifferentiatorFilter;///<< differentiator filter with low pass stage for the motor jerk. The bandwidth is 10Hz by default
    DigitalFilter   *torqueObserverFilter; ///< preprocessing filter for the torque observer. The bandwidth is 5Hz by default


    /// Reads the sensor data from IMotorHardware and sets their pre-processing, then invokes __process. Modify this method for changing the pre-processing.
    /// The frist two parameters (theta and dtheta) are not used because the sensors are directly read from IMotorHardware.
    virtual double _process(double theta, double dtheta, double dt) override;

    /// Virtual method that implements the specific MOTOR control law.
    /// Implements template method pattern for _process function.
    virtual double __process(double theta, double dtheta, double dt) =0;

private:
    double          torqueObserverCutOff, prev_torqueObserver, g;
};

/**
\brief This is the abstract base class for control algorithms for series elastic joints, namely SE (series elastic joint/actuators). It contains the basic SEA states including the spring torque (tau), the motor position (theta_m) and the environment position (theta_e) with first and second order derivatives. All these variables can be optionally filtered. This class retirieve the signal from the hw so it needs a pointer to an object which implements the ISEHardware interface.Child classes must override the member ___process
*/
class SEControl : public selibdev::MotorControlBase
{
public:
    SEControl(ISEHardware *hw); ///< constructor. Accellerations and velocities are filtered (50Hz) by default. positions are not processed
    virtual ~SEControl();

    ISEHardware     *sehw; ///< a pointer to hw casted using the ISEHardware interface

    double          A; ///< A = Jm / Kspring
    double          Kspring; ///< SEA spring stiffness constant (for ease of use)
    double          tau_spring;

    double          theta_e, dtheta_e, ddtheta_e; ///< for ease of use

protected:
     /// Reads the sensor data from ISEHardware and sets their pre-processing, then invokes ___process. Modify this method for changing the pre-processing.
     /// The frist two parameters (theta and dtheta) are not used because the sensor data is directly read from ISEHardware.
    virtual double __process(double theta, double dtheta, double dt) override;

    /// Virtual method that implements the specific SEA control law.
    /// Implements template method pattern for __process function
    virtual double ___process(double dt) =0;
};

//todo put something protected?


/**
\brief A PID controller with a velocity filter an a output stage filter (before the control output)
*/
class PID : public selibdev::ControlBase
{
public:
    PID(double ki, double kp, double kd);
    virtual ~PID();

    double          KP, KD, KI, integralSAT;

    DigitalFilter   *outFilter; ///< output filter. Set to null (default) to skip it
    DigitalFilter   *velFilter; ///< velocity filter. Set to null (default) to skip it

protected:
    virtual double _process(double theta, double dtheta, double deltaTime) override;

private:
    double err, derr, ierr;

};

/**
 * \brief A PID algorithm for motor control. It uses the class PID by composition
*/
class MotorPID : public selibdev::MotorControlBase
{
public:
    MotorPID(double ki, double kp, double kd, ISEHardware* hw);
    virtual ~MotorPID();

    PID     *pid;

protected:
    virtual double __process(double pos, double vel, double dt) override;
};

/**
 * \brief This class implements a PID force controller with damping injection and discontinuous robustification based on sign(err)
 */
class MegaPDForceControl : public selibdev::SEControl
{
public:
    /**
     * \param hw A pointer to an ISEHardware object
     */
    MegaPDForceControl(ISEHardware *hw);
    /** 
     * \param ki An integral gain
     * \param kp A proportional gain
     * \param kd A derivative gain
     * \param hw A pointer to an ISEHardware object
     */
    MegaPDForceControl(double ki, double kp, double kd, ISEHardware *hw);
    virtual ~MegaPDForceControl() {}

    double kp, kd, ff, kinj;
    double ni, phi;
protected:
    virtual double ___process(double dt) override;

    virtual std::string cb_getLoggerEntityNameString() override;
    virtual std::string cb_getLoggerPropertiesString() override;

private:
    double _err, _derr, _us;
};

/**
 * \brief This class implements a PID controller with loggable theta_e
 */
class SEVelocityControl : public selibdev::SEControl
{
public:
    SEVelocityControl(double ki, double kp, double kd, ISEHardware *hw);
    virtual ~SEVelocityControl() {}

    PID *pid;

protected:
    virtual double ___process(double dt);
};

}

#endif // _CONTROL_BASE_HPP
