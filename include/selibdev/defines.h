#ifndef DEFINES_H_INCLUDED
#define DEFINES_H_INCLUDED

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/


// ----------- GLOBAL VARIABLES ------------
#include "config/config.h"
//#include "config/default.h"

//------------- RT constants--------------
#define NSEC_PER_SEC 1000000000.0
//#define RT_ETHERCAT_DELAY     70000 // 0.07 ms

//--------------- electronics constants ----------------------
#define ENCODER_STEPS 20000.0
#define SET_CURRENT_CONVERSION 1.4286 // 10 volt / 7 ampere
#define READ_CURRENT_CONVERSION  1.7500 //  7 ampere / 4 volt
#define READ_TORQUE_CONVERSION 1.0  // 25 Nm / 10 volt

//--------------- physical constants ----------------------

//faulaber motor parameters

#define KT 1.43 // measured from torque sensor (works good)
//#define KT 2.28650 // from datasheet (works bad - not consistent with toque measurements e.g. dynamomenter + torque sensor)

#define COULOMB_FRICTION_POS 0.09060 * KT
#define COULOMB_FRICTION_NEG 0.10880 * KT
#define VISCOUS_FRICTION_POS 0.01270 * KT
#define VISCOUS_FRICTION_NEG 0.01060 * KT

#define JM 0.02078
#define CUR_SAT 6

//JPL motor parameters
//#define COULOMB_FRICTION_POS 0.14411
//#define COULOMB_FRICTION_NEG 0.16078
//#define VISCOUS_FRICTION_POS 0.00211
//#define VISCOUS_FRICTION_NEG 0.00217

//#define KT 0.42375
//#define JM 0.00041
//#define CUR_SAT 3

//#define TORQUE_OFFSET .0392

// spring-load parameters
//#define K_SPRING 1.03900 //metal soft spring
//#define K_SPRING 2.4882 //metal hard spring
//#define K_SPRING 3.30890 //red spring
// #define K_SPRING 3580.99 //load cell
//#define K_SPRING 78 //alignment joint
//#define K_SPRING 98.0 //oldham joint
// #define K_SPRING 240.00 //link 1
#define K_SPRING 12.34 // Ks Forecast

//#define JL 0.000125 //verylow
//#define JL 0.00068 //low (short wood)
//#define JL 0.00734 //low1 (long wood)
#define JL 0.00210 //mid
//#define JL 0.0307 //high
//#define JL 0.0099 //mid1
//#define JL 0.0868 //high1
// environment spring = 0.1 Nm/rad

// ------------- GLOBAL FUNCTIONS -------------

#include <cstdlib>
#include <cmath>

namespace selibdev
{

#define __sign(x) (std::signbit(x)?-1.0:1.0)

double inline drand() { return (double)std::rand() / (double)(RAND_MAX); } //returns from 0 to 1

}

#include "Utility/Debug.hpp"

#endif // DEFINES_H_INCLUDED
