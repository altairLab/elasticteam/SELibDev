#ifndef ISEAHARDWARE_H
#define ISEAHARDWARE_H

#include <iostream>
#include <memory>
#include "../Control/DigitalFilter.hpp"
#include "../Control/AnalogFilter.hpp"
#include "../defines.h"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/


namespace selibdev
{

/**
This class describes the interface with the HW electronics. You must extend this class and fill all the abstract methods to make the library working with your HW. See the child class SEHardware as example.
\brief This class describes the interface with the HW electronics.
*/
class IMotorHardware
{
public:

    virtual ~IMotorHardware() = default;

    double Jm; ///< reflected motor inertia (it includes the gear ratio ^2)
    double Kt; ///< motor torque constant
    double currentSaturation; ///< the maximum current allowed by the motor
    double coulombFrictionP; ///< motor coulomb friction for positive velocities
    double coulombFrictionN; ///< motor coulomb friction for negative velocities
    double viscousFrictionP; ///< motor viscous friction for positive velocities
    double viscousFrictionN; ///< motor viscous friction for negative velocities

    virtual void refresh(double dt) = 0; ///< this method is called in the main LoopTask cycle and is supposed to update the class fields with hardware data

    virtual void setCurrent(double value) = 0; ///< sets the current to the motor
    virtual void setTauM(double value) = 0; ///< sets the torque to the motor (will be then converted in a current)

    virtual double getCurrent() = 0; ///< reads the active motor current
    virtual double getTauM() = 0; ///< reads the motor torque (relative to the the active motor current)

    virtual double getThetaM() = 0; ///< reads the motor angle or position
    virtual double getDThetaM() = 0; ///< reads the motor velocity
    virtual double getDDThetaM() = 0; ///< reads the motor acceleration
    
    virtual double getTorque() = 0; ///< reads the force or torque sensor
    virtual double getDTorque() = 0; ///< compute the force or torque derivative
    virtual double getDDTorque() = 0; ///< compute the force or torque derivative
    virtual void setTorqueOffset(double value) = 0; ///< sets the offset for the torque sensor

    virtual double getLinkTorque() = 0; ///< reads the force or torque sensor ON THE LINK if present
    virtual double getLinkDTorque() = 0; ///< compute the force or torque derivative
    virtual double getLinkDDTorque() = 0; ///< compute the force or torque derivative

    virtual double getExternalTorque() = 0; ///< reads the force or torque sensor ON THE Environment if present
    virtual double getExternalDTorque() = 0; ///< compute the force or torque derivative
    virtual double getExternalDDTorque() = 0; ///< compute the force or torque derivative
    virtual void setExternalTorqueOffset(double value) = 0; ///< sets the offset for the EXTERNAL torque sensor

    virtual bool getIndexM() ///< returns the index signal of the motor encoder
    { 
        debug_info("The encoder doesn't have the index signal"); 
        return false;
    } 

    virtual void resetM() = 0; ///< resets the motor encoder

    virtual bool isStandStill(double timeSec) = 0; ///< returns if the SEA (motor and load) is stand still during a certain time intervall in seconds
    virtual bool isSaturated() = 0; ///< returns if the motor reached the saturation within the previous loop

    void setFilterThetaM(Filter * filter) { thetaM_Filter.reset(filter); }
    void setFilterDThetaM(Filter * filter) { dthetaM_Filter.reset(filter); }
    void setFilterDDThetaM(Filter * filter) { ddthetaM_Filter.reset(filter); }

    void setDerivativeFilterDTorqueM(Filter * filter) { dtorqueM_DerivativeFilter.reset(filter); }
    void setDerivativeFilterDDTorqueM(Filter * filter) { ddtorqueM_DerivativeFilter.reset(filter); }
    void setFilterTorqueM(Filter * filter) { torqueM_Filter.reset(filter); }
    void setFilterDTorqueM(Filter * filter) { dtorqueM_Filter.reset(filter); }
    void setFilterDDTorqueM(Filter * filter) { ddtorqueM_Filter.reset(filter); }

    void setDerivativeFilterDTorqueL(Filter * filter) { dtorqueL_DerivativeFilterr.reset(filter); }
    void setDerivativeFilterDDTorqueL(Filter * filter) { ddtorqueL_DerivativeFilter.reset(filter); }
    void setFilterTorqueL(Filter * filter) { torqueL_Filter.reset(filter); }
    void setFilterDTorqueL(Filter * filter) { dtorqueL_Filter.reset(filter); }
    void setFilterDDTorqueL(Filter * filter) { ddtorqueL_Filter.reset(filter); }

    void setDerivativeFilterDTorqueE(Filter * filter) { dtorqueE_DerivativeFilter.reset(filter); }
    void setDerivativeFilterDDTorqueE(Filter * filter) { ddtorqueE_DerivativeFilter.reset(filter); }
    void setFilterTorqueE(Filter * filter) { torqueE_Filter.reset(filter); }
    void setFilterDTorqueE(Filter * filter) { dtorqueE_Filter.reset(filter); }
    void setFilterDDTorqueE(Filter * filter) { ddtorqueE_Filter.reset(filter); }

protected:

    std::unique_ptr<Filter> thetaM_Filter; ///< low-pass filter for thetaM, disabled (set to null) by default (one pole)
    std::unique_ptr<Filter> dthetaM_Filter; ///< low-pass filter for dthetaM, disabled (set to null) by default (one pole)
    std::unique_ptr<Filter> ddthetaM_Filter; ///< low-pass filter for ddthetaM, set to 50Hz bandwidth by default (one pole)
    
    std::unique_ptr<Filter> dtorqueM_DerivativeFilter;
    std::unique_ptr<Filter> ddtorqueM_DerivativeFilter; ///< differentiator filter with low pass stage for the numerical derivative of dtorque. Default bandwidth is 10Hz
    std::unique_ptr<Filter> torqueM_Filter; ///< low-pass filter for the torque read from the torque sensor, disabled (set to null) by default (one pole)
    std::unique_ptr<Filter> dtorqueM_Filter; ///< low-pass filter for the numerical derivative of torque from torque sensor, disabled (set to null) by default (one pole)ddiff_torque
    std::unique_ptr<Filter> ddtorqueM_Filter;

    std::unique_ptr<Filter> dtorqueL_DerivativeFilterr;
    std::unique_ptr<Filter> ddtorqueL_DerivativeFilter;
    std::unique_ptr<Filter> torqueL_Filter;
    std::unique_ptr<Filter> dtorqueL_Filter;
    std::unique_ptr<Filter> ddtorqueL_Filter;
    
    std::unique_ptr<Filter> dtorqueE_DerivativeFilter;
    std::unique_ptr<Filter> ddtorqueE_DerivativeFilter;
    std::unique_ptr<Filter> torqueE_Filter;
    std::unique_ptr<Filter> dtorqueE_Filter;
    std::unique_ptr<Filter> ddtorqueE_Filter;

};


/**
This class describes the interface with the HW electronics. You must extend this class and fill all the abstract methods to make the library working with your HW. See the child class SEHardware as example.
\brief This class describes the interface with the HW electronics.
*/
class ISEHardware : public IMotorHardware
{
public:

    virtual ~ISEHardware() = default;

    virtual void refresh(double dt) = 0; ///< this method is called in the main LoopTask cycle and is supposed to update the class fields with hardware data

    virtual bool isStandStill(double time) = 0; ///< returns if the SEA (motor and load) has been stand still in the last time interval in seconds
    virtual bool isSaturated() = 0; ///< returns if the motor reached the saturation within the previous loop

    virtual double getStrainGaugeValue() = 0;
    virtual void setStrainGaugeOffset(double offset) = 0;

    virtual double getThetaE() = 0; ///< reads the load/environment angle or position
    virtual double getDThetaE() = 0; ///< reads the load/environment velocity
    virtual double getDDThetaE() = 0; ///< computes the load/environment accelleration based on velocity numerical differences

    virtual bool isConfigured() = 0;
    virtual void update() = 0;

    virtual bool getIndexE() ///< returns the index signal of the load encoder
    { 
        debug_info("The encoder doesn't have the index signal"); 
        return false;
    } 

    virtual void resetE() = 0; ///< resets the load encoder

    void setFilterThetaE(Filter * filter) { thetaE_Filter.reset(filter); }
    void setFilterDThetaE(Filter * filter) { dthetaE_Filter.reset(filter); }
    void setFilterDDThetaE(Filter * filter) { ddthetaE_Filter.reset(filter); }

    double Kspring; ///< SEA spring stiffness constant

    double strain_gauge_gain; ///< SEL current to strain proportional constant

protected:

    double strain_gauge_value;

    std::unique_ptr<Filter> thetaE_Filter; ///< low-pass filter for thetaE, disabled (set to null) by default (one pole)
    std::unique_ptr<Filter> dthetaE_Filter; ///< low-pass filter for dthetaE, disabled (set to null) by default (one pole)
    std::unique_ptr<Filter> ddthetaE_Filter; ///< low-pass filter for ddthetaE, set to 50Hz bandwidth by default (one pole)

};

}

#endif // ISEAHARDWARE_H
