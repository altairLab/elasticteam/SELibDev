#ifndef EL1202_HPP
#define EL1202_HPP
#include "elXXXX.hpp"

/****************************************************************************
 * Copyright (C) 2015 Lorenzo Bertelli, Andrea Calanca
 * @author Lorenzo Bertelli, Andrea Calanca
 * @date june 2015
 ****************************************************************************/

class EL1202 : public ELXXXX
{
    public:
        EL1202(int slaveID, ec_slavet * ec_slave);
        virtual ~EL1202(){};

        bool getInput1();
        bool getInput2();
};

#endif
