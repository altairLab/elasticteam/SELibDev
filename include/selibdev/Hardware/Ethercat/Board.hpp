#ifndef BOARD_H
#define BOARD_H


/*
 * BIG TODO understand the function of groups in soem and a Board 
 *          should represent a group!!!
 */

#include <map>
#include <memory>
#include "elXXXX.hpp"

namespace ethcat {
    class Board {
        // Input/Output EtherCATmapping
        static char IOmap[];

        std::map<std::string, std::unique_ptr<ELXXXX>> slaves;

        static uint16_t workerCount;
        static bool started;
    public:
        Board() = default;
        ~Board() = default;

        /**
         * @brief get a slave reference
         * 
         * @param name string that identify the slave
         * 
         * @return A reference to the requested slave casted into T
         * 
         */
        template <typename T>
        inline T& getSlave(std::string name) {
            if (slaves.find(name) == slaves.end())
                throw std::logic_error("cannot find slave");
            
            auto ptr = dynamic_cast<T*>(slaves[name].get());

            if (ptr == nullptr)
                throw std::logic_error(
                    "The specified slave cannot be "
                    "cast into the type requested"
                );

            return *ptr;
            
        }

        template <typename T, typename ...A>
        T &addSlave(std::string name, A&&... args){
            T* s = new T(std::forward<A>(args)...);
            
            slaves.emplace(
                name, 
                std::unique_ptr<ELXXXX>(s)
            );

            return *s;
        }

        bool configureHook();

        static void startHook(const std::string &ifname);

        static void sendAndReceive();

        static bool checkHook();

        static void stopHook();

        static void cleanupHook();

        static bool isStarted() {return started;}
    }; // Board
    
} // ethcat

#endif // BOARD_H
