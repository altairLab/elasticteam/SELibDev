#ifndef EL3008_HPP
#define EL3008_HPP

#include "elXXXX.hpp"
#include "selibdev/defines.h"

/****************************************************************************
 * Copyright (C) 2015 Lorenzo Bertelli, Andrea Calanca
 * @author Lorenzo Bertelli, Andrea Calanca
 * @date june 2015
 ****************************************************************************/

class EL3008 : public ELXXXX
{
    public:
        EL3008(int slaveID, ec_slavet * ec_slave);

        int setup(uint16_t slave);
        short getRaw(int channel);
        uint8 getRaw1(int channel);
        uint8 getRaw2(int channel);
        double getAnalog(int channel);


    private:
        double scale(short value);

};

#endif
