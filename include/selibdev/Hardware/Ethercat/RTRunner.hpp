#ifndef RT_RUNNER_HPP
#define RT_RUNNER_HPP

#include <atomic>

#include "selibdev/Hardware/Ethercat/SEHardware.hpp"
#include "selibdev/Tasks/LoopTask.hpp"

namespace selibdev
{

class RTRunner {

public:
    ~RTRunner();

    static RTRunner &getInstance();

    bool start(selibdev::ISEHardware *hw, std::string const &ifname = "eth0");
    void stop();

    inline bool isRunning() const
    {
        return _running;
    }

    void setTaskFactory(selibdev::LoopTaskFactory *tf);

    RTRunner(RTRunner const &) = delete;
    void operator=(RTRunner const &) = delete;

protected:
    RTRunner();
    virtual bool _configure();

    static void *_loop_helper(void *ptr); // main loop helper
    void *_dummy(void);                   // dummy loop
    void *_loop(void);                    // main loop

    bool _check_rt(); // Check if (soft-)realtime constraints are met
    void _safety();   // Euristic safety checks

    // EtherCAT error checker loop helper
    static void *_ecatcheck_helper(void *ptr);  
    // EtherCAT error checker loop
    void *_ecatcheck(void);                     

    std::string _ifname;

    selibdev::LoopTaskFactory *_taskFactory;
    std::unique_ptr<selibdev::ISEHardware> _hw;
    
    pthread_t _main_loop_thread;
    pthread_t _ecatcheck_thread;

    std::atomic<bool> _running;
    bool _exitLoop;

    struct timespec _t;
    double _time;
    double _prev_time;
    double _start_time;
};
}

#endif // RT_RUNNER_HPP