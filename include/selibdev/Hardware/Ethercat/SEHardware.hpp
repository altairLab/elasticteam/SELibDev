
#ifndef TEST_ETHERCAT_SEHARDWARE_HPP
#define TEST_ETHERCAT_SEHARDWARE_HPP

//
// Created by noemurr on 01/04/19.
//

#include "selibdev/Hardware/ISEHardware.hpp"
#include "selibdev/Hardware/Ethercat/Board.hpp"

namespace selibdev
{

class SEHardware : public selibdev::ISEHardware {
/**
This class implements the ISEHardware interface and comunicates with the Board.
Physical system parameters are defined in defines.h
*/
    public:        
        SEHardware(double Jm, double Kspring, double Kt);
        virtual ~SEHardware() override = default;

        ethcat::Board boardInterface;

        void refresh(double dt) override;

        void setCurrent(double value) override;

        inline void setTauM(double value) override {setCurrent(value/Kt);}

        inline double getCurrent() override {return active_current;}

        inline double getTauM() override {return active_tau_m;}

        inline double getThetaM() override {return thetaM;}
        inline double getDThetaM() override {return dthetaM;}
        inline double getDDThetaM() override {return ddthetaM;}

        inline double getThetaE() override {return thetaE;}
        inline double getDThetaE() override {return dthetaE;}
        inline double getDDThetaE() override {return ddthetaE;}

        bool getIndexM() override;

        bool getIndexE() override;

        inline double getTorque() override {return torqueM;}

        inline double getDTorque() override {return dtorqueM;}

        inline double getDDTorque() override {return ddtorqueM;}

        inline void setTorqueOffset(double value) override {
            this->torqueM_offset = value;
        }

        inline double getLinkTorque() override {return torqueL;}

        inline double getLinkDTorque() override {return dtorqueL;}

        inline double getLinkDDTorque() override {return ddtorqueL;}

        // inline void setLinkTorqueOffset(double value) override {
        //     this->torqueL_offset = value;
        // }

        double getStrainGaugeValue() override;

        void setStrainGaugeOffset(double offset) override;

        inline double getExternalTorque() override {return torqueE;}

        inline double getExternalDTorque() override {return dtorqueE;}

        inline double getExternalDDTorque() override {return ddtorqueE;}

        inline void setExternalTorqueOffset(double value) override {
            this->torqueE_offset = value;
        }

        void resetM() override;

        void resetE() override;

        bool isStandStill(double timeSec) override;

        inline bool isSaturated() override {return saturation_flag;}

        inline bool isConfigured() override {return configured;}

        bool saturation_flag;

        inline void update() override {ethcat::Board::sendAndReceive();}

    protected:

        virtual void updateMotor(double dt);

        virtual void updateLink(double dt);

        void updateExternal(double dt);

        const char * eth_name;
        
        const double ENCODER_RESOLUTION = (2 * M_PI / ENCODER_STEPS);

        bool configured;
        int i;
        double rand_amp, randM, randE;

        double thetaM, prev_thetaM;
        double thetaE, prev_thetaE;
        double thetaMzero;
        double thetaEzero;

        int prev_encoderMOTOR, prev_encoderLOAD, zeroMOTOR, zeroLOAD;

        double dthetaM, ddthetaM, diffthetaM, ddiffthetaM, periodM, signM;
        double dthetaE, ddthetaE, diffthetaE, ddiffthetaE, periodE, signE;

        double  prev_diffthetaM, period_dthetaM, prev_dthetaM, inc_dthetaM;
        double  prev_diffthetaE, period_dthetaE, prev_dthetaE, inc_dthetaE;

        std::unique_ptr<selibdev::DigitalFilter> envelopeFilterM;
        std::unique_ptr<selibdev::DigitalFilter> envelopeFilterE;

        double standStillTimeM, standStillTimeE;

        double toRadians;

        double active_tau_m, active_current, current_command;
        double torqueM, prev_torqueM, dtorqueM, ddtorqueM;
        double torqueL, prev_torqueL, dtorqueL, ddtorqueL;
        double torqueE, prev_torqueE, dtorqueE, ddtorqueE;

        double torqueM_offset, torqueE_offset;
        double torqueE_gain;

        double strain_gauge_offset;

        double curSaturation(double in);
};
}

#endif //TEST_ETHERCAT_SEHARDWARE_H
