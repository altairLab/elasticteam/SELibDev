#ifndef ELXXXX_HPP
#define ELXXXX_HPP

/****************************************************************************
 * Copyright (C) 2015 Lorenzo Bertelli, Andrea Calanca
 * @author Lorenzo Bertelli, Andrea Calanca
 * @date june 2015
 ****************************************************************************/

#include <vector>
#include <algorithm>
#include <iostream>
#include <string>

// SOEM includes
extern "C" {
#include <ethercattype.h>
#include <nicdrv.h>
#include <ethercatbase.h>
#include <ethercatmain.h>
#include <ethercatcoe.h>
#include <ethercatfoe.h>
#include <ethercatconfig.h>
#include <ethercatprint.h>
}

class ELXXXX
{
    public:
        ELXXXX(int slaveID, ec_slavet * ec_slave);
        virtual ~ELXXXX() = default;

        std::string getInputsAsString();
        std::string getOutputsAsString();

        int getSlaveID() const;

    protected:
        int slaveID;
        ec_slavet * ec_slave;

    private:
        std::string UIntToHexStr(unsigned int tmp);
};

#endif
