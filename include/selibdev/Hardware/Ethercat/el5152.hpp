#ifndef EL5152_HPP
#define EL5152_HPP

/****************************************************************************
 * Copyright (C) 2015 Lorenzo Bertelli, Andrea Calanca
 * @author Lorenzo Bertelli, Andrea Calanca
 * @date june 2015
 ****************************************************************************/

#include <vector>
#include <algorithm>
#include <iostream>
#include <string>

#include "elXXXX.hpp"

class EL5152 : public ELXXXX
{
    public:
        EL5152(int slaveID, ec_slavet * ec_slave);
        int getPosition1();
        int getPosition2();
        unsigned int getPeriod1();
        unsigned int getPeriod2();
        bool resetEncoders();
};

#endif
