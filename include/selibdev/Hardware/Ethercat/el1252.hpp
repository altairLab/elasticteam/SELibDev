#ifndef EL1252_HPP
#define EL1252_HPP
#include "elXXXX.hpp"

class EL1252 : public ELXXXX
{
    public:
        EL1252(int slaveID, ec_slavet * ec_slave);

        void getInput(int channel);
};

#endif
