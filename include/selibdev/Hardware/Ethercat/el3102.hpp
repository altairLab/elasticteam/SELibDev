#ifndef EL3102_HPP
#define EL3102_HPP

/****************************************************************************
 * Copyright (C) 2015 Lorenzo Bertelli, Andrea Calanca
 * @author Lorenzo Bertelli, Andrea Calanca
 * @date june 2015
 ****************************************************************************/

#include "elXXXX.hpp"

class EL3102 : public ELXXXX
{
    public:
        EL3102(int slaveID, ec_slavet * ec_slave);

        short getRaw1();
        short getRaw2();
        double getAnalog1();
        double getAnalog2();

    private:
        double scale(short value);

};

#endif
