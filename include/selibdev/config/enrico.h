#ifndef ENRICO_H
#define ENRICO_H

// ----------- GLOBAL VARIABLES ------------

#define LOG_DIRECTORY "/home/enrico/Log/"

//------------- RT constants--------------
//#define RT_INTERVAL     10000000 // 10 ms -> 100 Hz
//#define RT_INTERVAL     4000000 // 4 ms -> 250 Hz
//#define RT_INTERVAL     2000000 // 2 ms -> 500 Hz
//#define RT_INTERVAL     1000000 // 1 ms -> 1 kHz
//#define RT_INTERVAL     500000 // 0.5 ms -> 2 kHz
//#define RT_INTERVAL     333333 // 0.333333 ms -> 3 kHz
//#define RT_INTERVAL     250000 // 0.25 ms -> 4 kHz
#define RT_INTERVAL     200000 // 0.2 ms -> 5 kHz
//#define RT_INTERVAL     125000 // 0.125 ms -> 8 kHz

//#define TS 0.010
//#define TS 0.004
//#define TS 0.002
//#define TS 0.001
//#define TS 0.0005
//#define TS 0.000333333
//#define TS 0.00025
#define TS 0.0002
//#define RT 0.000125

// -- Uncomment to use fixed sensor calibration
//#define TORQUE_SENSORS_USE_STATIC_OFFSETS

#endif //ENRICO_H
