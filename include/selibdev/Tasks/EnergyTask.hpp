#ifndef ENERGYTASK_H
#define ENERGYTASK_H

#include "../defines.h"
#include "LoopTask.hpp"
#include "../Control/ControlBase.hpp"
#include "../Control/EnergyControl.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

namespace selibdev
{

/**
This is an example class to test the implemented energy control algorithms. This is used to inject a certain quantity of energy in the system or to swinging up a series elastic pendolum.
*/
class EnergyTask: public LoopTask
{
public:
	~EnergyTask(){delete ctr;};
	EnergyTask(ISEHardware* hw);
	int _loop();

	protected:
	EnergyControl* ctr;

};

}

#endif // ENERGYTASK_H
