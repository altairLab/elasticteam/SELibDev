#ifndef CURRENTTASK_H
#define CURRENTTASK_H

#include <math.h>
#include <sstream>
#include <string>

#include "LoopTask.hpp"
#include "../defines.h"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

namespace selibdev
{

/**
This class implements a task that sets a constant motor current
*/
class CurrentTask: public LoopTask
{
public:
	CurrentTask(ISEHardware* hw): LoopTask(hw){}
    int _loop();

	double current_ref;
};

/**
This class implements a task that sets a sinusoidal motor current
*/
class SinCurrentTask: public LoopTask
{
public:
	SinCurrentTask(ISEHardware* hw): LoopTask(hw){}
    int _loop();

    double amp;
    double freq;
};

}

#endif // CURRENTTASK_H
