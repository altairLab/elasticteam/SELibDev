#ifndef POSITIONTASK_H
#define POSITIONTASK_H

#include <math.h>
#include <sstream>
#include <string>

#include "LoopTask.hpp"
#include "../defines.h"
#include <selibdev/Utility/MemoryLogger.hpp>

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

namespace selibdev
{

/**
This class implements a task to test position control algorithms
*/
class PositionTask: public LoopTask
{
public:
    PositionTask(ISEHardware* hw);
    int _loop();
    
    selibdev::MemoryLogger *ctr_logger;

	double position_ref;
	MotorPID* ctr;
};


/**
This class implements a task to test velocity control algorithms
*/
class VelocityTask: public LoopTask
{
public:
    VelocityTask(ISEHardware* hw);
    int _loop();

    selibdev::MemoryLogger *ctr_logger;
    
	SEVelocityControl* ctr;

    double velocity_ref;
};

/**
This class implements a task to test position control with sinusoidal reference
*/
class SinPositionTask: public PositionTask
{
public:
	SinPositionTask(ISEHardware* hw): PositionTask(hw){};
    int _loop();
    
    double amp;
    double freq;

protected:
	double fadeIn;
};

/**
This class implements a task to test position control with sinusoidal reference
*/
class SinVelocityTask: public VelocityTask
{
public:
	SinVelocityTask(ISEHardware* hw): VelocityTask(hw){};
    int _loop();
    
    double amp;
    double freq;

protected:
    double fadeIn;
};

/**
This class implements a homing task
*/
class HomingTask: public PositionTask
{
public:
	HomingTask(ISEHardware* hw): PositionTask(hw){}
    int _loop();
};

}

#endif // POSITIONTASK_H
