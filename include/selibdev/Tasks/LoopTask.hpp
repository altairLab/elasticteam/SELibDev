#ifndef _LOOPTASK_H
#define _LOOPTASK_H

#include "../Hardware/ISEHardware.hpp"
#include "../Control/ControlBase.hpp"
#include "../Control/DigitalFilter.hpp"
#include "../Control/SlidingMode.hpp"

#include <iostream>
#include <fstream>

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

namespace selibdev
{

/**
This virtual class describes a task which is a constititive element of a task machine. The task is characterized by a loop operation, a next task (member LoopTask* next) and an after task (member LoopTask* after). The pointer next is initialized to "this" so the task loops by default. Within a loop the next task can be dynamically set. The task is also characterized by a maxDuration. When the duration expires the after task becomes the current task. If after or next are null the execution is stopped.

The correct usage of the task machine is:

@code
static LoopTask* currentTask; ///< The current task

int mainLoop() ... to be executed each dt seconds
{
    task = LoopTask::getCurrentTask();
    repeat = task->loop(dt);
}
@endcode

Note: A task can be considered similar a state of a finite state machine, however the transition policy is decided by the task itself and it is not centralized by a scheduler/dispatcher as in typical state machine design pattern. This mechanism is more generic because the transition can depend on specific informations which may not be included in the parent LoopTask class. A task is conceived as an high level operation which may require several information to do some reasoning and decide a transition. A FSM can be used within a single task to describe simpler operations.

Implementation details & suggestion:
1. The currentTask is a private static member of LoopTask. Childs cannot directly access it. They can just set the next and after tasks and invoke the reansitions goToNext or goToAfter
2. If the task are dynamically creates and not retrieved from a pool there may be a memory leak: all the previous states may remain in memory. Suggested solution: use a collection for the pool of task.
3. If the next state is changed you may want to dispose the previous task.

\brief This virtual class describes a task which is a constititive element of a task machine.
*/

class LoopTask
{
public:
    LoopTask(ISEHardware* hw);
    virtual ~LoopTask(){ delete hw; } //do not delete next otherwise the next state will be erased!!

    int loop(double dt);
    virtual int _loop(){ debug_info("void task! \n"); return 0;}

    virtual bool isFinished() { return time>maxDuration; } ///< the task termination condition, when it is satisfied the after task runs

    // TODO: To override mandatory
    virtual void dispose() {}

    void timeReset() { time = 0; } ///< it resets the time

	LoopTask* next;
	LoopTask* after;

	double maxDuration; ///< defines the maximum duration of a task. If it is negative the task can be neverending. The duration is cumulative in case of loop-transitions unless the timeReset() is invoked

	void goToNext();
	void goToAfter();

	bool endLine; ///< if true a cout << endl is invoked every loop

	static LoopTask* getCurrentTask();
	static void setCurrentTask(LoopTask* t); //todo singletonize

protected:
    static LoopTask* currentTask; ///< The current task
    ISEHardware* hw;

    double dt, time;
    double current;  ///< the motor current [A]. Meant to be used in children. This is a motor control library.
    double tau_m;  ///< the motor torque [Nm]. Meant to be used in children. This is a motor control library.

private:
	bool toRet;
};

/**
 * \brief Base class to exted from every TaskFactory that has to create a Task that inherit from LoopTask
 */
struct LoopTaskFactory
{
    LoopTaskFactory() {}
    virtual ~LoopTaskFactory() { delete task; }

    LoopTask * createTask(ISEHardware *hw);

protected:

    virtual LoopTask * _createTask(ISEHardware *hw) = 0;

private: 

    LoopTask *task;
};

/**
 * \brief This class implements a simple task for initialization purposes
 */
class InitTask: public LoopTask
{
public:
    InitTask(ISEHardware* hw, LoopTask *afterTask) :
        LoopTask(hw),
        counter(0),
        strain_gauge_offset(0.0),
        motor_torque_offset(0.0),
        external_torque_offset(0.0)
    {
        counter = 0;
        strain_gauge_offset = 0;
        maxDuration = 5.0;
        after = afterTask;
        endLine = false;
    }
    int _loop();

private:
    int counter;
    double strain_gauge_offset;
    double motor_torque_offset;
    double external_torque_offset;

};

}

#endif // _LOOPTASK_H
