#ifndef SELCONTROLTASK_H
#define SELCONTROLTASK_H

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Enrico Sartori, Andrea Calanca
 * @date Feb 2019
 ****************************************************************************/

#include <math.h>
#include <sstream>
#include <string>

#include "selibdev/Tasks/LoopTask.hpp"
#include "selibdev/Utility/MemoryLogger.hpp"
#include "selibdev/Control/ImpedanceControl.hpp"
#include "selibdev/Control/PassiveControl.hpp"

#include "../Control/DOBControlOL.hpp"
#include "../Control/DOBControlCL.hpp"
#include "../Control/DOBControlOLInnerD.hpp"
// #include "../Control/SELControl.hpp"
#include "../Control/InnerDControl.hpp"

namespace selibdev
{

/**
 * \brief This class implements the task used to make the tests of the article [1]
 * 
 * [1] 
 */
class SELControlTask : public selibdev::LoopTask
{
public:
    SELControlTask(selibdev::ISEHardware* hw);
    virtual ~SELControlTask();
 
    int _loop();
    double torque_ref;

protected:
    //controllers
    selibdev::SEControl* ctr;

    selibdev::MegaPDForceControl* megaPD;
    
    selibdev::DOBControlOL* OL_dob;
    selibdev::DOBControlOL3order* OL_dob_3_order;
    selibdev::DOBControlOLInnerD* dDOB;
    selibdev::DOBControlCL* CL_dob;
    // selibtest::DOBControlCL3order* CL_dob_3_order;

    // selibtest::SELSlidingModeForceControl* smc;
	// selibtest::IntegralSlidingModeForceControl* ism;
    selibdev::SMCinnerD *smcind;

    selibdev::PinnerD *pind;
    selibdev::PinnerPD *pinpd;

    selibdev::MemoryLogger *ctr_logger;

	double amp, freq;
    double k_des, d_des;

    void init(double f);
};

/**
 * \brief Task Factory for the SELControlTask
 */
struct SELControlTaskFactory : public selibdev::LoopTaskFactory
{

    SELControlTaskFactory() : LoopTaskFactory() {}
    virtual ~SELControlTaskFactory() {}

    virtual selibdev::LoopTask *_createTask(selibdev::ISEHardware *hw) override
    {
        fprintf(stderr, "Creating SELControlTaskFactory!\n");
        SELControlTask *t = new SELControlTask(hw);
        t->maxDuration = 60.0;
        return t;
    }

};

}

#endif // SELCONTROLTASK_H
