#ifndef IDENTIFICATIONTASK_H
#define IDENTIFICATIONTASK_H

#include <string>
#include "LoopTask.hpp"
#include "PositionTask.hpp"
#include "CurrentTask.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

namespace selibdev
{

/**
This class implements a finite state machine of series tasks that can be used to serialize identification experiments
*/
class IdentificationTask: public LoopTask
{
public:
    IdentificationTask(double* amps, double* freqs, int n, ISEHardware* hw);
    int _loop();

protected:
	int idx;
    int size;
    double* amp;
    double* freq;

private:
    SinPositionTask* pt;
};

/**
This class implements a finite state machine of series tasks to for identifing the friction curve in a torque/velocity plot
*/
class FrictionIdentificationTask: public IdentificationTask
{
public:
	FrictionIdentificationTask(double* amps, double* freqs, int n, ISEHardware* hw): IdentificationTask(amps,freqs,n,hw) {}
	int _loop();

private:
	CurrentTask* ct;
	VelocityTask* vt;

};

/**
This class implements a finite state machine of series tasks to for identifing the inertia
*/
class InertiaIdentificationTask: public IdentificationTask
{
public:
	InertiaIdentificationTask(double* amps, double* freqs, int n, ISEHardware* hw): IdentificationTask(amps,freqs,n,hw) {}
	int _loop();

private:
	SinVelocityTask* vt;
};

/**
This class implements a finite state machine of series tasks to for identifing the stiffness of the spring
*/
class SpringIdentificationTask: public IdentificationTask
{
public:
	SpringIdentificationTask(double* amps, double* freqs, int n, ISEHardware* hw): IdentificationTask(amps,freqs,n,hw) {
        ctr = new MegaPDForceControl(0,2,0.04,hw);

        ctr->torqueSensing = TorqueSensing::fromMotorSensor;

        ctr_logger = new MemoryLogger((int)1000);
        ctr_logger->loggerFilePath = "/home/altair/SELib/Log"; 
        //decide which keys of the entries to be flushed on file
        ctr_logger->loggerKeys = std::vector<std::string>({
            "time",
            "ref",
            "theta_m",
            "dtheta_m",
            "theta_e",
            "dtheta_e",
            "current",      
            "tau"
        }); 
        ctr_logger->loggerName = "Spring_identification_task_log";
        //this set to take one value every X specified below here
        ctr_logger->decimationFactor = 0;
        //decide to do the average of the decimationFactor or not
        ctr_logger->averagingEnabled = true;
        ctr_logger->loggerEnabled = true;

        ctr->setLogger(ctr_logger);

        amp = amps[0];
        freq = freqs[0];
        ctr->ref = 0;
    }
	int _loop();

private:
	MegaPDForceControl* ctr;
    MemoryLogger* ctr_logger;

    double amp, freq;
};

/**
This class implements a finite state machine of series tasks to for identifing the stiffness of the environment
*/
class EnvironmentIdentificationTask: public IdentificationTask
{
public:
	EnvironmentIdentificationTask(double* amps, double* freqs, int n, ISEHardware* hw): IdentificationTask(amps,freqs,n,hw) {}
	int _loop();

private:
	SinVelocityTask* vt;
};

/**
This class implements a finite state machine of series tasks to set different sinusoidal motor currents
*/
class openLoopIdentificationTask: public IdentificationTask
{
public:
	openLoopIdentificationTask(double* amps, double* freqs, int n, ISEHardware* hw): IdentificationTask(amps,freqs,n,hw) {}
	int _loop();

private:
	SinCurrentTask* t;

};

struct InertiaIdentificationTaskFactory : public LoopTaskFactory
{

    InertiaIdentificationTaskFactory(double* amps, double* freqs, int n) : LoopTaskFactory() {
        this->amp = amps;
        this->freq = freqs;
        this->size = n;
        this->duration = 4.0;
    }
    virtual ~InertiaIdentificationTaskFactory() {}


protected:
    int size;
    double* amp;
    double* freq;
    double duration;

public:
    virtual LoopTask *_createTask(ISEHardware *hw) override
    {
        IdentificationTask *id = new InertiaIdentificationTask(amp, freq, size, hw);

        debug_info("Creating InertiaIdentificationTasks");
        
        id->maxDuration = duration;
        id->endLine = false;

        return id;
    }

};

struct FrictionIdentificationTaskFactory : public LoopTaskFactory
{

    FrictionIdentificationTaskFactory(double* velocity_refs, int n) : LoopTaskFactory() {
        this->velocity_refs = velocity_refs;
        this->size = n;
        this->duration = 4.0;
    }
    virtual ~FrictionIdentificationTaskFactory() {}


protected:
    int size;
    double* velocity_refs;
    double duration;

public:
    virtual LoopTask *_createTask(ISEHardware *hw) override
    {
        IdentificationTask *id = new FrictionIdentificationTask(velocity_refs, velocity_refs, size, hw);
        
        debug_info("Creating FrictionIdentificationTasks");
        
        id->maxDuration = duration;
        id->endLine = false;

        return id;
    }

};

struct SpringIdentificationTaskFactory : public LoopTaskFactory
{

    SpringIdentificationTaskFactory(double amp, double freq) : LoopTaskFactory() {
        this->amp = amp;
        this->freq = freq;
        this->duration = 20.0;
    }
    virtual ~SpringIdentificationTaskFactory() {}


protected:
    double amp;
    double freq;
    double duration;

public:
    virtual LoopTask *_createTask(ISEHardware *hw) override
    {
        double amps[1];
        amps[0] = amp;
        double freqs[1];
        freqs[0] = freq;

        IdentificationTask *id = new SpringIdentificationTask(amps, freqs, 1, hw);
        
        debug_info("Creating SpringIdentificationTasks");
        
        id->maxDuration = duration;
        id->endLine = false;

        return id;
    }

};

}

#endif // IDENTIFICATIONTASK_H
