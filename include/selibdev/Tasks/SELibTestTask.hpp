#ifndef _SELIBTESTTASK_H
#define _SELIBTESTTASK_H

#include <math.h>
#include <sstream>
#include <string>

#include "LoopTask.hpp"
#include "../Control/SlidingMode.hpp"
#include "../Control/ImpedanceControl.hpp"
#include "../Control/PassiveControl.hpp"
#include "../Utility/ConfigurationManager.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Davide Costanzi
 * @date october 2018
 ****************************************************************************/

namespace selibdev
{

/**
This class is a set of test to verify library's force control algorithms.
Control algortithms are initialized in the init method and a force reference is given in the _loop method.

\brief This class is a set of test to verify library's force control algorithms.
*/

class SELibTestTask : public selibdev::LoopTask
{
public:
    enum struct ControllerType
    {
        PASSIVE_PID,
        PD_INJ,
        MEGA_PD
    };

    enum struct ReferenceType
    {
        GRAVITY_COMP,
        SINUSOID
    };
    
    SELibTestTask(ISEHardware *hw);
    virtual ~SELibTestTask();

    virtual int _loop() override;

    virtual void dispose() override;
    
    void setControllerType(ControllerType ctrType);
    void setReferenceType(ReferenceType refType);

    double torque_ref;

    double init_duration;

    // Frequency reference
    double amp, freq, offset;

    // Gravity compensation reference
    double mgb, posOffset;

protected:
    //controllers
    selibdev::SEControl* ctr;

    //SlidingModeForceControl* sm;
	//IntegralSlidingModeForceControl* ism;
 
    PassivePIDForceControl* pd;
    PDForceControlWithDampingInjection *pdinj;
    selibdev::MegaPDForceControl *megapd;

    //PID *pid;

    double f_band;

    // Logger of the selected controller
    selibdev::MemoryLogger ctr_logger;

private:
    ControllerType _ctrType;
    bool _gravityCompReference;

    void _init();
    void _setActiveControllerHelper();
};

/**
 * \brief Task Factory of the SELibTestTask
 */
struct SELibTestTaskFactory : public selibdev::LoopTaskFactory
{

    SELibTestTaskFactory() : selibdev::LoopTaskFactory() {}
    virtual ~SELibTestTaskFactory() {}

    double duration;

    SELibTestTask::ControllerType ctrType;

    SELibTestTask::ReferenceType refType;

    virtual selibdev::LoopTask *_createTask(ISEHardware *hw) override
    {
        fprintf(stderr, "Creating SELibTestTask!\n");
        selibdev::SELibTestTask *tc = new selibdev::SELibTestTask(hw);

        tc->maxDuration = duration;

        tc->setControllerType(ctrType);
        tc->setReferenceType(refType);
        return tc;
    }

};

}

#endif // _SELIBTESTTASK_H
