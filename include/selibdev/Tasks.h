#ifndef _TASKS_H
#define _TASKS_H

#include "Tasks/CurrentTask.hpp"
#include "Tasks/EnergyTask.hpp"
#include "Tasks/IdentificationTask.hpp"
#include "Tasks/LoopTask.hpp"
#include "Tasks/PositionTask.hpp"

#endif // _TASKS_H

