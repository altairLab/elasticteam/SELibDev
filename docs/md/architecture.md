# Architecture {#architecture}

## Architecture scheme

![Architecutre_scheme](SelibDev_main_scheme.png)

The library is divided in 4 main components:
1. **Control**: Contains the implementations of all the controllers.
2. **Filters**: Contains the implementations of the digital and analog filters.
3. **Task**: Contains the task implemented by the library
4. **Hardware**: Contains the interface to the Hardware componets.

## Control 

This module contains the implementations of all the control algorithms 
spported by the library.
All the controllers inherits from the `ControlBase` class. 

## Filters 

In this module there are the declaration of `Filter` that is the interface of
a generic Filter, and two classes that inherits from it: `AnalogFilter` and 
`DigitalFilter`. This last two classes are used by the controllers
(situated in the Control directory).

## Tasks

A task represents the application of one or more control algorithms on a 
specific Hardware. 
It also is the logic link between the hardware and the control components.
It is possible to concatenate more tasks and to switch between concatenated 
tasks with the methods `LoopTask::goNext()` and  `LoopTask::goAfter()`.

## Hardware

This module should contains all the classes that communicate directly with a 
specific hardware.
All the classes that representing an hardware must inherits from the 
`ISEHardware` interface because a pointer to this interface is used by the
tasks to the control algorithms. 

## Usage of the library

To correctly use the library, first, it is necessary to define an 
implementation of `ISEHardware` that can communicate with the harware setup 
that is been used. 
Later it is possible to use one or more, predefined or custom, tasks to apply 
one or more control algoritms (objects that inherits from `ControlBase`) to 
the hardware.
