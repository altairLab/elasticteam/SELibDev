Controllers {#controllers}
===========

Proposed controllers:

- PID Force Control

- Passive Linear Force Control (PD + %PID with integral roll off)

- Passive Acceleration based Force Control [1][2]

- Passive Velocity sourced Force Control [3]

- Sliding-mode Force Control [4]

- Integral Sliding-mode Control [4]

- Human Adaptive Force Control [5][10]

- Indirect Human Adaptive Force Control [6]

- Multi-model Human Adaptive Force Control [9] 

- Acceleration-Based Force Control [11][12][13]

- Force Control Based on Disturbance Observers (DOBs) [14][15][16]

- Impedance contol based on all the above force implementations [7][8][9]

- InnerD controllers (PinnerD, SMCinnerD, DOBinnerD) [17]


Notation and physical meaning of variables is according with figures 4 and 5 in [10] , see [here](https://www.researchgate.net/publication/265124731_On_The_Role_of_Compliance_In_Force_Control)

References:

[1] G. A. Pratt and M. M. Williamson, Series Elastic Actuators, in International Conference on Intelligent Robots and Systems, 1995, vol. 1, pp. 399-406.

[2] M. M. Williamson, Series Elastic Actuators, Master Thesis, 1995.

[3] H. Vallery, R. Ekkelenkamp, H. van der Kooij, and M. Buss, Passive and accurate torque control of series elastic actuators, 2007 IEEE/RSJ Int. Conf. Intell. Robot. Syst., pp. 3534-3538, Oct. 2007.

[4] A. Calanca, L. Capisani, and P. Fiorini, Robust Force Control of Series Elastic Actuators, Actuators, Spec. Issue Soft Actuators, vol. 3, no. 3, pp. 182-204, 2014.

[5] A. Calanca and P. Fiorini, Human-Adaptive Control of Series Elastic Actuators, Robotica, vol. 2, no. 08, pp. 1301-1316, 2014.

[6] A. Calanca, R. Muradore, and P. Fiorini, Passivity Human-Adaptive Control of Elastic Actuators, Submitted 

[7] H. Vallery, J. Veneman, E. H. F. van Asseldonk, R. Ekkelenkamp, M. Buss, and H. van Der Kooij, Compliant actuation of rehabilitation robots, IEEE Robot. Autom. Mag., vol. 15, no. 3, pp. 60-69, Sep. 2008.

[8] N. L. Tagliamonte and D. Accoto, Passivity Constraints for the Impedance Control of Series Elastic Actuators, J. Syst. Control Eng., vol. 228, no. 3, pp. 138-153, 2013.

[9] A. Calanca, R. Muradore, and P. Fiorini, Impedance Control of Series Elastic Actuators: Passivity and Acceleration-Based Control, Submitted.

[10] A. Calanca and P. Fiorini, On The Role of Compliance In Force Control, in International Conference on intelligent Autonomous Systems, 2014.

[11] A. Calanca, R. Muradore, and P. Fiorini, Impedance Control of Series Elastic Actuators Using Acceleration Feedback, in Wearable Robotics: Challenges and Trends: Proceedings of the 2nd International Symposium on Wearable Robotics, WeRob2016, October 18-21, 2016, Segovia, Spain, J. Gonzalez-Vargas, J. Ibanez, J. L. Contreras-Vidal, H. van der Kooij, and J. L. Pons, Eds. Cham: Springer International Publishing, 2017, pp. 33-37.

[12] A. Calanca and P. Fiorini, A Rationale for Acceleration Feedback in Force Control of Series Elastic Actuators, Submitted.

[13] T. Boaventura, M. Focchi, M. Frigerio, J. Buchli, C. Semini, G. a. Medrano-Cerda, and D. G. Caldwell, On the role of load motion compensation in high-performance force control, in 2012 IEEE/RSJ International Conference on Intelligent Robots and Systems, 2012, pp. 4066-4071.

[14] N. Paine, S. Oh, and L. Sentis, Design and control considerations for high-performance series elastic actuators, IEEE/ASME Trans. Mechatronics, vol. 19, no. 3, 2014.

[15] H. Yu, S. Huang, G. Chen, Y. Pan, and Z. Guo, Human-Robot Interaction Control of Rehabilitation Robots with Series Elastic Actuators, IEEE Trans. Robot., vol. 31, no. 5, pp. 1089-1100, 2015.

[16] S. Oh and K. Kong, High Precision Robust Force Control of a Series Elastic Actuator, IEEE/ASME Trans. Mechatronics, vol. 4435, preprint, 2016.

[17] ...