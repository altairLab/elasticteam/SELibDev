Installation {#installation}
===========

The library is built with CMake, to generate the Makefile we use the following commands:
```
mkdir build
cd build
cmake ..
```
Some parameters in the CMakeCache need to be configured, by running the command
```
ccmake .
```
All the following parameters are shown:
```
CMAKE_BUILD_TYPE: 		{1}
CMAKE_INSTALL_PREFIX: 		{2}
SELIB_CONFIGURATION_NAME: 	{3}
SELIB_HW_ETHERCAT: 		{4}
SELIB_INCLUDE_TESTS: 		{5}
SELIB_TARGET: 			{6}
SELIB_USE_CATKIN: 		{7}
```
{1}: Can be Debug or Release.
{2}: The path where the library will be installed. 
{3}: Name of the configuration file containing the pragma instructions for the configuration of the library at the build time.
{4}: Include the implementation of a sample hardware (AltairLab testbench).
{5}: ON or OFF.
{6}: An arbitrary number of target can be defined in order to optimize the build of the library for every specific case. Two are already implemented SELIB_TARGET_LINUX, SELIB_TARGET_EMBEDDED.
{7}: ON if you want integrate the library in a Catkin project, for example to use in ROS.

