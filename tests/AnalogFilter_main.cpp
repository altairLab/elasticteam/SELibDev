#include <iostream>
#include <vector>
#include <cmath>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>

#include "defines.h"
#include "Control/AnalogFilter.hpp"

using namespace std;
using namespace selibdev;

int main(int argc, char **argv) {
    double t;
    double T = 10.0;
    double h = 1e-4;

    double reference;
    double output;

    std::ofstream filter;

    int order = 2;  // Filter Order

    double Fc_zeros = 15;   // Hz
    double Fc_poles = 10;   // Hz

    double w_z = 2 * M_PI * Fc_zeros;   // Omega Zeros
    double w_q = 2 * M_PI * Fc_poles;   // Omega Poles

    double xi_z = 1;    // Xi Zeros
    double xi_q = 1;    // Xi Poles

    double a[] = {1 / pow(w_q, 2), 2 * xi_q / w_q, 1.0};
    double b[] = {1 / pow(w_z, 2), 2 * xi_z / w_z, 1.0};

    AnalogFilter *analogFilter = new AnalogFilter(order, a, b);


    filter.open("filter.csv");

    for (t = 0.0; t <= T; t += h) {
        usleep(h * 1e6);

        reference = sin(400 * t);
        output = analogFilter->process(reference, h);
        filter << output << endl;
    }

    filter.close();

    debug_info("Task completed.");
    std::cin.get();

    return 0;
}
