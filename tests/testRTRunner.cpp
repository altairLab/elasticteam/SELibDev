#include <iostream>
#include <signal.h>
#include "Hardware/Ethercat/RTRunner.hpp"
#include "Tasks/SELibTestTask.hpp"
#include "Utility/ConfigurationManager.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Davide Costanzi
 * @date october 2018
 ****************************************************************************/

using namespace std;
using namespace selibdev;

//bool forceExit;

//void sighandler(int sig) { forceExit = true; }
void sighandler(int sig) { RTRunner::getInstance().stop(); }

int main(int argc, char **argv)
{
    ConfigurationManager * conf = ConfigurationManager::getInstance();
    conf->setConfig(ConfigurationManager::LibConfigs::LOG_PATH, "/home/enrico/Log/");
    conf->setConfig(ConfigurationManager::LibConfigs::LOG_MAX_ROW, (double)500000);
    
    srand(time(NULL));

    signal(SIGABRT, &sighandler);
    signal(SIGTERM, &sighandler);
    signal(SIGINT, &sighandler);

    selibdev::LoopTaskFactory *tf;

    //SEForceTaskFactory *seftf = new SEForceTaskFactory();
    //seftf->logFileName = "SEAForceTask_impedance";
    //seftf->duration = 20.0;
    //seftf->torque_ref = 0.1;
    //tf = dynamic_cast<RTRunner::LoopTaskFactory*>(seftf);

    selibdev::SELibTestTaskFactory *ttf = new selibdev::SELibTestTaskFactory();
    ttf->duration = 10.0;
    ttf->ctrType = selibdev::SELibTestTask::ControllerType::MEGA_PD;
    ttf->refType = selibdev::SELibTestTask::ReferenceType::SINUSOID;
    tf = dynamic_cast<selibdev::LoopTaskFactory*>(ttf);

    //forceExit = false;

    //RTRunner runner;
    RTRunner &runner = RTRunner::getInstance();

    runner.setTaskFactory(tf);

    if (!runner.start())
    {
        std::exit(1);

        return 1;
    }

    debug_error("Waiting high priority thread...");

    while (!runner.isRunning());

    debug_error("STARTED!!");

    // Main Loop is running
    //while (!forceExit && runner.isRunning());
    while (runner.isRunning());

    //runner.stop();

    debug_error("STOPPED!!");

    delete tf;

    std::exit(0);

    return 0;
}
