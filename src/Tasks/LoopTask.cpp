#include "Tasks/LoopTask.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

using namespace selibdev;

LoopTask::LoopTask(ISEHardware* hw)
{
    this->hw = hw;
    next = this;
    maxDuration = 60.0; ///< task max duration in seconds, 1 min by default
    endLine = true;
}

int LoopTask::loop(double dt)
{
    next = this; //this is a loop task!!

    this->time += dt;
    this->dt = dt;
    hw->refresh(dt);

	//do action
    toRet = this->_loop();

	//next transition
	goToNext();

	//after transition
	if (isFinished())
    {
    	goToAfter();
        debug_info("Task Finished: time " <<time);
        //hw->setCurrent(0.0);

    }

    //the loop cout - however it is not true that all task should have a cout
    // if(endLine) debug_info("");

    return toRet;
}

LoopTask *LoopTask::currentTask = NULL;

LoopTask *LoopTask::getCurrentTask() { return currentTask;}

void LoopTask::setCurrentTask(LoopTask* t) { currentTask = t;}

void LoopTask::goToNext()
{
	if(next == NULL) toRet = 0;
	else
	{
		if(currentTask!=next) { currentTask->dispose(); next->timeReset(); }
		currentTask = next;
	}
}

void LoopTask::goToAfter()
{
	if(after == NULL) toRet = 0;
	else
	{
		currentTask->dispose();
		after->timeReset();
		currentTask = after;
	}
}

/* **************************************** */

LoopTask * LoopTaskFactory::createTask(ISEHardware *hw)
{
    task = _createTask(hw);
    return task;
}

/* **************************************** */

int InitTask::_loop()
{
    if(time >= 0.1) // the same thing if(counter >= maxDuration/(double)dt - 10)
    {
        //offset measurement
        if(time < maxDuration - 0.51)
        {
            strain_gauge_offset += hw->getStrainGaugeValue();
            motor_torque_offset += hw->getTorque();
            external_torque_offset += hw->getExternalTorque();
            counter++;
        }
        //offset setting
        if(time >= maxDuration - 0.51) // the same thing if(counter >= maxDuration/(double)dt - 10)
        {
            hw->setStrainGaugeOffset( strain_gauge_offset/ (double)counter );
            hw->setTorqueOffset( motor_torque_offset/ (double)counter );
            hw->setExternalTorqueOffset( external_torque_offset/ (double)counter );
            debug_info("link torque offset: " << strain_gauge_offset/ (double)counter);
        }

    }

    return 1;
}
