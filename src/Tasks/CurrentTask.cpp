#include "Tasks/CurrentTask.hpp"
#include "Control/ControlBase.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
****************************************************************************/

using namespace selibdev;

LoopTask* ____cu__create_task(ISEHardware* hw)
{
    CurrentTask* c = new CurrentTask(hw);
    c->current_ref = -0.2;

  	SinCurrentTask* sc = new SinCurrentTask(hw);
    sc->amp = 0.5;
    sc->freq = 0.5;
    sc->maxDuration = 20.0;
    
	return c;
}

int CurrentTask::_loop()
{
    current = current_ref;
	//current += Control::frictionCompensation(hw->getDThetaM());
    hw->setCurrent(current);

    return 1;
}

int SinCurrentTask::_loop()
{
    current =  amp * sin(2 * M_PI * freq * time);
	//current += Control::frictionCompensation(thetaMfilter->process(hw->getDThetaM()));
    hw->setCurrent(current);
    return 1;
}
