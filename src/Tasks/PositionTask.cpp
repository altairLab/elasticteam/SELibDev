#include "Tasks/PositionTask.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

using namespace selibdev;

// LoopTask* _create_task(ISEHardware* hw)
// {
//     SinPositionTask* spt = new SinPositionTask(hw);
//     spt->amp = 3.14/4;
//     spt->freq = 5;
//     spt->maxDuration = 20.0;

// 	PositionTask* pt = new PositionTask(hw);
// 	pt->maxDuration = 600;

// 	VelocityTask* vt = new VelocityTask(hw);
// 	vt->velocity_ref = 5;
//     vt->maxDuration = 12;

//     HomingTask* ht = new HomingTask(hw);

//     return vt;
// }

//JPL
//#define KP 5.0
//#define KD 0.2
//#define KI 0.0

//faulhaber
#define KP 2.0
#define KD 0.04
#define KI 0.0

//#define KP 0.05
//#define KD 0.001
//#define KI 0.0

////
//#define KP 40.0
//#define KD 2
//#define KI 1



PositionTask::PositionTask(ISEHardware* hw): LoopTask(hw)
{
    ctr = new MotorPID(KI,KP,KD,hw);

    ctr_logger = new MemoryLogger((int)1000);
    ctr_logger->loggerFilePath = "/home/altair/SELib/Log"; 
    //decide which keys of the entries to be flushed on file
    ctr_logger->loggerKeys = std::vector<std::string>({
        "time",
        "theta_m",
        "dtheta_m",
        "current",      
        "tau"
    }); 
    ctr_logger->loggerName = "Position_task_test_log";
    //this set to take one value every X specified below here
    ctr_logger->decimationFactor = 0;
    //decide to do the average of the decimationFactor or not
    ctr_logger->averagingEnabled = true;
    ctr_logger->loggerEnabled = true;

    ctr->setLogger(ctr_logger);
}

int PositionTask::_loop()
{
    ctr->ref = position_ref;
    current = ctr->process(hw->getThetaM(),hw->getDThetaM(),dt) / KT;
//    current += ctr->frictionCurrentCompensation(hw->getDThetaM());
    // debug_info(hw->getDThetaM());
    hw->setCurrent(current);
    return 1;
}

VelocityTask::VelocityTask(ISEHardware* hw): LoopTask(hw)
{
    ctr = new SEVelocityControl(8,0.1,0.0,hw);
    
    ctr->torqueSensing = TorqueSensing::fromMotorSensor;

    ctr_logger = new MemoryLogger((int)1000);
    ctr_logger->loggerFilePath = "/home/altair/SELib/Log"; 
    //decide which keys of the entries to be flushed on file
    ctr_logger->loggerKeys = std::vector<std::string>({
        "time",
        "ref",
        "theta_m",
        "dtheta_m",
        "ddtheta_m",
        "theta_e",
        "dtheta_e",
        "ddtheta_e",
        "current",    
        "tau_spring",  
        "tau"
    }); 
    ctr_logger->loggerName = "Velocity_task_test_log";
    //this set to take one value every X specified below here
    ctr_logger->decimationFactor = 0;
    //decide to do the average of the decimationFactor or not
    ctr_logger->averagingEnabled = true;
    ctr_logger->loggerEnabled = true;

    ctr->setLogger(ctr_logger);
}

int VelocityTask::_loop()
{
	ctr->ref = velocity_ref;
    current = ctr->process(hw->getThetaM(),hw->getDThetaM(),dt) / KT;
    hw->setCurrent(current);
    return 1;
}

int SinPositionTask::_loop()
{
    fadeIn = fadeIn < 1.0 ? time : 1.0;
    ctr->ref =  fadeIn * amp * sin(2 * M_PI * freq * time);
    ctr->dref = fadeIn * amp * freq * cos(2 * M_PI * freq * time);
    ctr->ddref = fadeIn * (-amp) * freq * freq * sin(2 * M_PI * freq * time);
    current = ctr->process(hw->getThetaM(),hw->getDThetaM(),dt) / KT;
    // current = ctr->process(hw->getThetaE(),hw->getDThetaE(),dt) / KT;
	hw->setCurrent(current);

    return 1;
}

int SinVelocityTask::_loop()
{
    fadeIn = (1-exp(-2*time));
    ctr->ref =  fadeIn * amp * sin(2 * M_PI * freq * time);
    ctr->dref = fadeIn * amp * freq * cos(2 * M_PI * freq * time);
    ctr->ddref = fadeIn * (-amp) * freq * freq * sin(2 * M_PI * freq * time);
	current = ctr->process(hw->getDThetaM(),hw->getDDThetaM(),dt) / KT;
    
    hw->setCurrent(current);
    return 1;
}

int HomingTask::_loop()
{
    if (hw->getIndexM())
    {
        hw->resetM();
        debug_info("home!");
        return 0;
    }
    else
    {
        ctr->ref = time*M_PI*0.2;
        current = ctr->process(hw->getThetaM(),hw->getDThetaM(),dt) / KT;
        hw->setCurrent(current);
        return 1;
    }
}
