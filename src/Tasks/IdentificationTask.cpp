#include "Tasks/IdentificationTask.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

using namespace selibdev;

IdentificationTask::IdentificationTask(double* amps, double* freqs, int n, ISEHardware* hw): LoopTask(hw)
{
    amp = amps;
    freq = freqs;
    size = n;
}

int IdentificationTask::_loop()
{
    hw->setCurrent(0);
	if (idx >= size) return 0; //all experiments has been done*/

    if (hw->isStandStill(2))
    {
        hw->resetM();
        hw->resetE();

        debug_info("Experiment n." << idx);
        pt = new SinPositionTask(hw);
        pt->amp = amp[idx];
        pt->freq = freq[idx];
        pt->maxDuration = 10.0;

//        pt->ctr->pid->KP = 1.0; // to keep the control lighter

        //state concatenation
        pt->after = this;
        next = pt;
        //so after pt I come here again

        idx++;
    }
    return 1;
}

int FrictionIdentificationTask::_loop()
{
    hw->setCurrent(0);
	if (idx >= size) return 0; //all experiments has been done*/
    
    if (hw->isStandStill(3))
    {
        hw->resetM();
        hw->resetE();

        debug_info("Experiment n." << idx);

//        ct = new CurrentTask(hw);
//		  ct->current_ref= amp[idx]*0.4;
//        debug_info("Current Ref = " << ct->current_ref);
//        ct->logEnabled = true;
//        ct->maxDuration = 25;
//        ct->filename = this->filename;
//        //state concatenation
//        ct->after = this;
//        next = ct;
//        //so after pt I come here again


        vt = new VelocityTask(hw);
        vt->ctr_logger->loggerName = "Friction_identification_task_log";
        vt->velocity_ref = amp[idx];
        vt->maxDuration = 20;
        //state concatenation
        vt->after = this;
        next = vt;
        //so after pt I come here again



        idx++;

    }
    return 1;
}

int InertiaIdentificationTask::_loop()
{
    hw->setCurrent(0);
	if (idx >= size) return 0; //all experiments has been done*/
    
    if (hw->isStandStill(3))
    {
        hw->resetM();
        hw->resetE();

        debug_info("Experiment n." << idx);

        vt = new SinVelocityTask(hw);
        vt->ctr_logger->loggerName = "Inertia_identification_task_log";
        vt->amp = amp[idx];
        vt->freq = freq[idx];
        vt->maxDuration = 10;
        //state concatenation
        vt->after = this;
        next = vt;
        //so after pt I come here again

        idx++;

    }
    return 1;
}

int SpringIdentificationTask::_loop()
{
    ctr->ref = (amp + amp * sin(2*M_PI*time*freq)) * (1-exp(-2*time));
    // if(ctr->ref < amp) {
    //     ctr->ref += vel * dt;
    //     if(ctr->ref > amp) ctr->ref = amp;
    // }
    
    current = ctr->process(0,0,dt) / KT;
    hw->setCurrent(current);
    return 1;
}

int EnvironmentIdentificationTask::_loop()
{
    hw->setCurrent(0);
	if (idx >= size) return 0; //all experiments has been done*/
    
    if (hw->isStandStill(3))
    {
        hw->resetM();
        hw->resetE();

        debug_info("Experiment n." << idx);

        vt = new SinVelocityTask(hw);
        vt->ctr_logger->loggerName = "Environment_identification_task_log";
        vt->amp = amp[idx];
        vt->freq = freq[idx];
        vt->maxDuration = 20;
        //state concatenation
        vt->after = this;
        next = vt;
        //so after pt I come here again

        idx++;

    }
    return 1;
}

int openLoopIdentificationTask::_loop()
{
    hw->setCurrent(0);
    if (hw->isStandStill(3333))
    {
        hw->resetM();
        hw->resetE();

        t = new SinCurrentTask(hw);
		t->amp = amp[idx];
        t->freq = freq[idx];
        t->maxDuration = 7;

        //state concatenation
        t->after = this;
        next = t;
        //so after pt I come here again

        debug_info("Experiment n." << idx);

        idx++;

    }
    if (idx > size) return 0; //all experiments has been done*/
    return 1;
}