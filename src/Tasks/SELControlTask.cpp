#include "Tasks/SELControlTask.hpp"

/****************************************************************************
 * Copyright (C) 2018 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2018
 ****************************************************************************/

using namespace selibdev;

selibdev::SELControlTask::~SELControlTask()
{
    // delete ism;
    delete megaPD;
    // delete smc;
    delete smcind;
    delete pind;
    delete pinpd;
    delete OL_dob;
    delete OL_dob_3_order;
    delete dDOB;
    delete CL_dob;
}

void selibdev::SELControlTask::init(double f)
{
    // Hardware configuration
    
    hw->setDerivativeFilterDTorqueM(AnalogFilter::getDifferentiatorHz(5.0));
    hw->setDerivativeFilterDDTorqueM(AnalogFilter::getDifferentiatorHz(5.0));
    
    // hw->setDerivativeFilterDTorqueL(AnalogFilter::getDifferentiatorHz(5.0));
    // hw->setDerivativeFilterDDTorqueL(AnalogFilter::getDifferentiatorHz(5.0));
    // hw->setDerivativeFilterDTorqueL(AnalogFilter::getDifferentiatorHz(35.0));
    // hw->setDerivativeFilterDDTorqueL(AnalogFilter::getDifferentiatorHz(35.0));
    hw->setDerivativeFilterDTorqueL(AnalogFilter::getDifferentiatorHz(40.0));
    hw->setDerivativeFilterDDTorqueL(AnalogFilter::getDifferentiatorHz(40.0));
    
    hw->setDerivativeFilterDTorqueE(AnalogFilter::getDifferentiatorHz(5.0));
    hw->setDerivativeFilterDDTorqueE(AnalogFilter::getDifferentiatorHz(5.0));
    
    // Controller configuration

    double lambda1, lambda2, lambda;

    lambda = 2*M_PI*f;

    lambda2 = powf(2*M_PI*f,2);
    lambda1 = 2.0*sqrt(lambda2); //diminuire questo toglie il chattering

	// ism = new selibtest::IntegralSlidingModeForceControl(lambda1,lambda2,hw);
    // ism->ni = 1.0;
    // ism->phi = 150.0;
    // ism->ka = 0.0;

    megaPD = new MegaPDForceControl(hw);
    megaPD->kinj = 0.0;
    megaPD->phi = 0.0;
    megaPD->ni = 0.0;
    megaPD->ff = 1.0;
    megaPD->kp = 4.027; 
    megaPD->kd = 0.2;
    // Critically damped (bandwidth 40Hz)
    // megaPD->kp = 4.5; 
    // megaPD->kd = 0.05;
    // Critically damped (bandwidth 35Hz)
    // megaPD->kp = 3.2; 
    // megaPD->kd = 0.04;

    double kP = 4.027;
    double kd = 0.2;
    pind = new selibdev::PinnerD(kP, kd, hw);
    pind->ff = 1.0;
    // pind->kP = 4.027;
    // pind->kd = 0.2;

    pinpd = new selibdev::PinnerPD(hw);
    pinpd->kP = 4.027;
    pinpd->kp = 2.0;
    pinpd->kd = 0.2;
    pinpd->ff = 1.0 + pinpd->kp; // inv of system  gain

    double k_d = 0.2;
    // double w_alpha_des = 2.0*M_PI*1.0;
    // smc = new selibtest::SELSlidingModeForceControl(k_d,w_alpha_des,lambda,hw);
    // smc->ni = 0.5; //1.0;
    // smc->phi = 1.5;
    // smc->kdref = 1.0;

    smcind = new selibdev::SMCinnerD(k_d, lambda, hw);
    smcind->ni = 0.9;
    smcind->phi = 1.5;

    double ol_dob_kp = 4.5; // 4.027;
    double ol_dob_kd = 0.05; // 0.2;
    double ol_dob_qcutoffhz = 10.0; // 0.5;
    OL_dob = new selibdev::DOBControlOL(ol_dob_kp, ol_dob_kd, ol_dob_qcutoffhz, hw);
    // OL_dob->dynamicFFEnabled = true;

    double cl_dob_kp = 4.5; // 4.027;
    double cl_dob_kd = 0.05; // 0.2;
    double cl_dob_qcutoffhz = 10.0; // 5.0;
    CL_dob = new selibdev::DOBControlCL(cl_dob_kp, cl_dob_kd, cl_dob_qcutoffhz, hw);
    
    double ol_dob3_kp = 4.027;
    double ol_dob3_kd = 0.2;
    double ol_dob3_qcutoffhz = 0.5;
    OL_dob_3_order = new selibdev::DOBControlOL3order(ol_dob3_kp, ol_dob3_kd, ol_dob3_qcutoffhz, hw);
    // OL_dob_3_order->dynamicFFEnabled = true;

    // double cl_dob3_kp = 0.0;
    // double cl_dob3_kd = 0.0;
    // double cl_dob3_qcutoffhz = 0.0;
    // CL_dob_3_order = new DOBControlCL3order(cl_dob3_kp, cl_dob3_kd, cl_dob3_qcutoffhz, hw);

    double d_dob_kp         = 4.027; 
    double d_dob_kd         = 0.2; 
    double d_dob_qcutoffhz  = 1.8; 
    double d_dob_alpha      = 1.0;
    dDOB = new selibdev::DOBControlOLInnerD(d_dob_kp, d_dob_kd, d_dob_qcutoffhz, d_dob_alpha, hw);
    
}

selibdev::SELControlTask::SELControlTask(ISEHardware* hw): LoopTask(hw)
{
	//----- tuning ---------------
    // performance parameters: define the bandwidth for the closed loop system
    init(4.0);//bandwidth in Hz
    // init(35.0);

    ConfigurationManager * conf = ConfigurationManager::getInstance();
    std::string logPath = conf->getConfig<std::string>(ConfigurationManager::LibConfigs::LOG_PATH);
    double log_max_row = conf->getConfig<double>(ConfigurationManager::LibConfigs::LOG_MAX_ROW);
   
    ctr_logger = new MemoryLogger((int)log_max_row);
    ctr_logger->loggerFilePath = logPath; 
    ctr_logger->loggerKeys = std::vector<std::string>({
        "time", "theta_e", "dtheta_e",  
        "tau", "dtau", 
        "ref", "dref",
        "tau_link", "tau_motor", 
        "d_hat" // "err" // "s"
        }); 
    ctr_logger->loggerName = "stiff";
    ctr_logger->decimationFactor = 10;
    ctr_logger->averagingEnabled = false;
    ctr_logger->loggerEnabled = true;

    // Controllers for InnerD paper

    // ctr = megaPD;
    // ctr = pind;
    // ctr = pinpd;
    
    // ctr = ism;
    // ctr = smcind;
    // ctr = smc; 

    // ctr = dDOB; 
    // ctr = OL_dob;
    // ctr = OL_dob_3_order;
    ctr = CL_dob;

    ctr->setLogger(ctr_logger);

    ctr->torqueSensing = fromLinkSensor; // fromSpring, fromMotorSensor
}

int selibdev::SELControlTask::_loop()
{

    amp = -0.05; // -0.4
    freq = 0.5;  // 2
    
    
    // ctr->ref = amp*1.2 + amp*sin(2 * M_PI * freq * time);
    // ctr->dref =  amp * 2 * M_PI * freq * cos(2 * M_PI * freq * time);
    // ctr->ddref =  - amp * powf(2 * M_PI * freq,2) * sin(2 * M_PI * freq * time);

    ctr->ref = 0;
    ctr->dref = 0;
    ctr->ddref = 0;

//////    // STEP
    
    // ctr->ref = 3.0*amp + amp * __sign(sin(2 * M_PI * freq * time));

//////    // SIN
   freq = time / 12.0;
   ctr->ref = - 0.7 * (0.4 + 0.25 * (sin(2 * M_PI * freq *time))); //* (1-exp(-2*time));
   ctr->dref = - 0.7 * (0.25 * 2 * M_PI * freq* (cos(2 * M_PI * freq *time)));
   ctr->ddref = 0.7 * (0.25 * 2 * M_PI * freq *2 * M_PI * freq * (sin(2 * M_PI * freq *time)));

    // ctr->ref = 1.5*amp + amp * sign(sin(2 * M_PI * freq * time));

    // // SEL Paper
    // // tracking 4 e 8 Hz:
    // freq = 0.5;

    // ctr->ref = - 0.8*(1.25 + 0.25 * (sin(2 * M_PI * freq *time)))*(1-exp(-2*time));
    // ctr->ref = - 1.5*(1 + 0.2 * (sin(2 * M_PI * freq *time)))*(1-exp(-2*time));
    //     ctr->ref = 1.0*(0.4 + 0.3 * (sin(2 * M_PI * 10 *time)))*(1-exp(-2*time));
    // ctr->ref = 0;
    // ctr->ref = 0.6;


    // ctr->ref = - 0.8*(1.25 + 0.25 * (sin(2 * M_PI * freq *time))) * (1-exp(-2*time));
    // ctr->dref = - 0.8*(0.25 * 2 * M_PI * freq* (cos(2 * M_PI * freq *time)));
    // ctr->ddref = 0.8*(0.25 * 2 * M_PI * freq *2 * M_PI * freq * (sin(2 * M_PI * freq *time)));

    tau_m = ctr->process(0.0,0.0,dt); // SEA controllers already know tau & dtau
    
    // tau_m += ctr->frictionTorqueCompensationColoumb(hw->getDThetaM());
    tau_m += ctr->frictionTorqueCompensation();
    hw->setTauM(tau_m);

    return 1;
}
