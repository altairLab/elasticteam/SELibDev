#include "Tasks/EnergyTask.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

using namespace selibdev;

LoopTask* e_create_task(ISEHardware* hw)
{
    EnergyTask* e = new EnergyTask(hw);
    e->maxDuration = 15;
    return e;
}

EnergyTask::EnergyTask(ISEHardware* hw): LoopTask(hw)
{
    ctr = new EnergyControl(hw);
    ctr->energy_ref = 0.1;
	ctr->k_e = 0.1;

}

int EnergyTask::_loop()
{
    current = ctr->process(0.0,0.0,dt) / KT;
    hw->setCurrent(current);
    return 1;
}


