#include "Tasks/SELibTestTask.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Davide Costanzi
 * @date october 2018
 ****************************************************************************/

// selibdev::LoopTask* selibdev::create_task(ISEHardware* hw)
// {
//     selibdev::SELibTestTask* tc = new selibdev::SELibTestTask(hw);
//     tc->maxDuration = 10.0;
//     tc->setControllerType(selibdev::SELibTestTask::ControllerType::MEGA_PD);
//     tc->setReferenceType(selibdev::SELibTestTask::ReferenceType::SINUSOID);
//     return tc;
// }

selibdev::SELibTestTask::~SELibTestTask()
{
    //delete sm;
	//delete ism;
    delete pd;
    delete pdinj;
    delete megapd;
    //delete pid;
}

selibdev::SELibTestTask::SELibTestTask(ISEHardware *hw) :
    selibdev::LoopTask(hw),
    ctr_logger(500000),
    _ctrType(ControllerType::MEGA_PD),
    _gravityCompReference(false)
{
    endLine = false;

	//----- tuning ---------------
    f_band = 20;

	_init();

    //------- set the active controller ------------
    _setActiveControllerHelper();

    // - Force Control
    //ctr = sm;
    //ctr = ism;

    //ctr = pd;
    //ctr = pdinj;
    //ctr = megapd;

    //pid = new PID(0,70.0,0.0);

    //------- set reference settings ---------------
    init_duration = 2;
    // Gravity compensation
    mgb = 1.72;
    posOffset = 1.51;
    // Sinusoid
    amp = 0.4;
    freq = 0.1;
    offset = -1.0;
}

void selibdev::SELibTestTask::setControllerType(ControllerType ctrType)
{
    _ctrType = ctrType;
    _setActiveControllerHelper();
}

void selibdev::SELibTestTask::setReferenceType(ReferenceType refType)
{
    switch (refType)
    {
        case ReferenceType::GRAVITY_COMP:
            _gravityCompReference = true;
            break;
        case ReferenceType::SINUSOID:
            _gravityCompReference = false;
            break;
    }
}

void selibdev::SELibTestTask::_init()
{
    // performance parameters: define the (second order) reference model for the closed loop system
    double lambda1, lambda2;
    lambda2 = powf(2 * M_PI * f_band, 2);
    lambda1 = 2.0 * sqrt(lambda2); //diminuire questo toglie il chattering

    //-----sliding-modes ---------------

    //sm = new SlidingModeForceControl(lambda1,hw);
    //sm->ni = 0.5;

    //ism = new IntegralSlidingModeForceControl(lambda1,lambda2,hw);
    //ism->ni = 0.5;
    //ism->phi = 20;

    //---------Passive-----------------------------------
    pd = new PassivePIDForceControl(hw);
    pd->ff = 1.0;
    //pd->kp = 4.0;
    //pd->kd = 12.0;
    pd->kp = 1.0;
    pd->kd = 0.0;
    pd->ki = 0.0;

    pdinj = new PDForceControlWithDampingInjection(hw);
    pdinj->ff = 1.0;
    //pdinj->kp = 4.0;
    //pdinj->kd = 12.0;
    pdinj->kp = 1.0;
    pdinj->kd = 0.0;
    pdinj->kinj = 0.0;

    megapd = new selibdev::MegaPDForceControl(hw);
    megapd->ff = 1.0;
    megapd->kp = 4.0;
    megapd->kd = 0.02;
    megapd->kinj = 0.0;
    megapd->ni = 0.8;
    megapd->phi = 2;

    ConfigurationManager * conf = ConfigurationManager::getInstance();
    std::string logPath = conf->getConfig<std::string>(ConfigurationManager::LibConfigs::LOG_PATH);
    
    ctr_logger.loggerFilePath = logPath;
    ctr_logger.loggerEntityName = "megapd";
    ctr_logger.loggerKeys = std::vector<std::string>({"time", "ref", "tau"});
    ctr_logger.loggerEnabled = true;
    
    megapd->setLogger(&ctr_logger);                 // Custom logger
    // megapd->getLogger()->setLogEnable(true);     // Default logger
}

void selibdev::SELibTestTask::_setActiveControllerHelper()
{
    // - Force Control
    switch (_ctrType)
    {
        case ControllerType::PASSIVE_PID:
            // ctr = pd;
            break;
        case ControllerType::PD_INJ:
            // ctr = pdinj;
            break;
        case ControllerType::MEGA_PD:
            ctr = megapd;
            break;
    }

    // - Force Sensing
    //ctr->torqueSensing = fromLinkSensor;
    //ctr->torqueSensing = fromSpring;
    ctr->torqueSensing = fromMotorSensor;
}

int selibdev::SELibTestTask::_loop()
{
    double theta_e = hw->getThetaE() + posOffset;

    if (_gravityCompReference)
    {
        // Gravity compensation
        if (time < init_duration)
        {
            ctr->ref = - (1-exp(time/init_duration))/(1-M_E) * mgb * cos(theta_e);
            //ctr->dref = - exp(time/init_duration)/(1-M_E) * mgb/init_duration * sin(theta_e) * hw->getDThetaE();
        }
        else
        {
            ctr->ref = - mgb * cos(theta_e);
            //ctr->dref = mgb * sin(theta_e) * hw->getDThetaE();
        }
    }
    else
    {
        // Sinusoid with offset
        if (time < init_duration)
        {
            ctr->ref = (1-exp(time/init_duration))/(1-M_E) * offset;
            //ctr->dref = - exp(time/init_duration)/(1-M_E) * offset / init_duration;
            //ctr->ddref = - exp(time/init_duration)/(1-M_E) * offset / (init_duration * init_duration);
        }
        else
        {
            ctr->ref = offset - amp * sin(2 * M_PI * freq * (time - init_duration));
            //ctr->dref = - amp * (2 * M_PI * freq * cos(2 * M_PI * freq * time));
            //ctr->ddref = - amp * (4 * M_PI * M_PI * freq * freq * sin(2 * M_PI * freq * time));
        }
    }

    tau_m = ctr->process(0.0, 0.0, dt); // SEA controllers already know tau & dtau
    //tau_m = pid->process(hw->getThetaE(),hw->getDThetaE(),dt);
    tau_m += ctr->frictionTorqueCompensationColoumb(hw->getDThetaM());
    //tau_m += ctr->frictionTorqueCompensation();

    hw->setTauM(tau_m);

    return 1;
}

void selibdev::SELibTestTask::dispose()
{
    ctr->dispose();
}