#include "Utility/Logger.hpp"

using namespace selibdev;

Logger::Logger()
    :   loggerEnabled(false),
        decimationFactor(0),
        averagingEnabled(false),
        logCounter(0),
        loggerClosed(false),
        privateLogCounter(0)
{
    // ntd
}

void Logger::saveLoggableRefs(std::map<std::string, const double *> * loggable_map) ///< save the reference of the variables to log
{
    if (!refs.empty()) 
        refs.clear();

    // Population of the vector containing the variable references 
    for (std::string key : loggerKeys)
    {
        const double * ref = (loggable_map->find(key) != loggable_map->end()) ? (*loggable_map)[key] : &default_value;
        refs.push_back( ref );
    }

    // Initialization of vectors
    record.resize(loggerKeys.size());
    decimationRecord.resize(loggerKeys.size());
}

void Logger::log() 
{
    recordIsReady = false;
    if (decimationFactor != 0)
    {
        if (privateLogCounter % decimationFactor == 0)
        {
            recordIsReady = true;
            for(unsigned int i = 0; i < decimationRecord.size(); i++ )
            {
                record[i] = (averagingEnabled) ? decimationRecord[i] : *(refs[i]);
                decimationRecord[i] = 0.0;
                logCounter++;
            }
        }
        for(unsigned int i = 0; i < decimationRecord.size(); i++ )
            decimationRecord[i] += *(refs[i]) / decimationFactor;
    } 
    else
    {
        recordIsReady = true;
        for(unsigned int i = 0; i < record.size(); i++ )
        {
            record[i] = *(refs[i]);
            logCounter++;
        }
    } 
    privateLogCounter++;
}

void Logger::logClose() 
{ 
    // Mandatory check
    if ( loggerClosed ) return;
    loggerClosed = true; 
}