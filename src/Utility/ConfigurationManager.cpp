#include <Utility/ConfigurationManager.hpp>

using namespace selibdev;


template<> std::map<ConfigurationManager::LibConfigs, std::string> * ConfigurationManager::getConfMap() 
{ 
    return &stringLibConfigMap; 
}

template<> std::map<ConfigurationManager::LibConfigs, double> * ConfigurationManager::getConfMap() 
{ 
    return &doubleLibConfigMap; 
}

template<> std::map<std::string, std::string> * ConfigurationManager::getConfMap() 
{ 
    return &stringUserConfigMap; 
}

template<> std::map<std::string, double> * ConfigurationManager::getConfMap() 
{ 
    return &doubleUserConfigMap;
}


void ConfigurationManager::setConfig(ConfigurationManager::LibConfigs key, std::string value)
{ 
    ( * getConfMap<ConfigurationManager::LibConfigs, std::string>() )[key] = value; 
}

void ConfigurationManager::setConfig(ConfigurationManager::LibConfigs key, double value)
{ 
    ( * getConfMap<ConfigurationManager::LibConfigs, double>() )[key] = value; 
}

void ConfigurationManager::setConfig(std::string key, std::string value)
{ 
    ( * getConfMap<std::string, std::string>() )[key] = value; 
}

void ConfigurationManager::setConfig(std::string key, double value)
{ 
    ( * getConfMap<std::string, double>() )[key] = value; 
}