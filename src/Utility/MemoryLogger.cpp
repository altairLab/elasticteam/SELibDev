#include <Utility/MemoryLogger.hpp>
#include <ctime>
#include <iostream>

void selibdev::MemoryLogger::log()
{   
    if (!loggerEnabled) 
        return;
  
    if (!isMemLogOpened)
        logOpen();

    if (logCounter < mem_COL_MAX * mem_ROW_MAX)
    {
        // std::vector<double> values = getLogRecord();
        // for (unsigned int i = 0; i < values.size(); i++)
        // {
        //     memLogData.push_back(values[i]);
        // }
        
        Logger::log();

        if (recordIsReady)
        {
            for (double d : record)
                memLogData.push_back(d);
        }

        // if (logCounter >= mem_COL_MAX * mem_ROW_MAX && writeOnOutOfMemory)
        if (logCounter >= mem_COL_MAX * mem_ROW_MAX || writeOnOutOfMemory) {
            memoryFlush();
        }
    }
}

void selibdev::MemoryLogger::logOpen()
{
    mem_COL_MAX = loggerKeys.size();
    // Allocate immediatelly the required memory 
    //      (avoid to intermediate allocation during control)
    memLogData.reserve(mem_COL_MAX * mem_ROW_MAX);
    isMemLogOpened = true;
}

void selibdev::MemoryLogger::logClose()
{
    memoryFlush();

    isMemLogOpened = false;
    FileLogger::logClose();
}

void selibdev::MemoryLogger::memoryFlush()
{
    if ( loggerClosed ) return;

    if (logCounter > 0)
    {
        if (!logFile.is_open())
            FileLogger::logOpen();

        for (unsigned int i = 0; i < logCounter; i++)
        {
            logFile << memLogData[i] << " ";
            if ( (i+1) % mem_COL_MAX == 0 ) logFile << std::endl;
        }

        logCounter = 0;
    }

    memLogData.clear();
}
