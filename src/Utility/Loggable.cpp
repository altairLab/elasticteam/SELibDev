#include "Utility/Loggable.hpp"

using namespace selibdev;

Loggable::Loggable() : isLoggerSet(false)
{
    // Set the default logger
    double log_max_row = ConfigurationManager::getInstance()->getConfig<double>(ConfigurationManager::LibConfigs::LOG_MAX_ROW);
    logger = new MemoryLogger(log_max_row);
    logger->loggerEntityName = "default";
}

Loggable::~Loggable()
{
    logger->logClose();
    delete logger;
}

const double *Loggable::getEntryDouble(std::string key)
{
    return (const double *)loggable_map[key];
}

void Loggable::setLogger(Logger *logger)
{
    if (logger)
    {
        isLoggerSet = true;
        this->logger = logger;
        this->logger->saveLoggableRefs(&loggable_map);
        // these methods act as callbacks that must be implemented in the loggable child class
        this->logger->loggerEntityName = getLoggerEntityNameString();
        this->logger->loggerProperties = getLoggerPropertiesString();
    }
}

Logger *Loggable::getLogger()
{
    // If no logger is set, the default one is initialized
    if (!isLoggerSet)
        setLogger();
    return logger;
}

std::string Loggable::cb_getLoggerEntityNameString()
{
    return "";
}

std::string Loggable::cb_getLoggerPropertiesString()
{
    return "";
}

void Loggable::addEntryDouble(std::string key, const double *ref) 
{
    loggable_map[key] = (const double *)ref;
}

void Loggable::setLogger()
{
    // The default logger catch all the variables available in the loggable map
    std::vector<std::string> keys;
    for (const auto &pair : loggable_map)
        keys.push_back(pair.first);
    logger->loggerKeys = keys;
    logger->saveLoggableRefs(&loggable_map);
    isLoggerSet = true;
}

std::string Loggable::getLoggerEntityNameString()
{
    if (logger->loggerEntityName == "")
        return cb_getLoggerEntityNameString();
    return logger->loggerEntityName;
}

std::string Loggable::getLoggerPropertiesString()
{
    if (logger->loggerProperties == "")
        return cb_getLoggerPropertiesString();
    return logger->loggerProperties;
}