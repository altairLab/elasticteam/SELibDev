#include <Utility/FileLogger.hpp>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <limits>

void selibdev::FileLogger::log()
{    
    if (!loggerEnabled) 
        return;

    if (!logFile.is_open())
        logOpen();

    Logger::log();

    if (recordIsReady)
    {
        for (double d : record)
            logFile << d << " ";
        logFile << std::endl;
    }
}

void selibdev::FileLogger::logOpen()
{
    if (!logFile.is_open())
    {
        // FILE PATH

        std::time_t t = std::time(0);
        std::tm* now = std::localtime(&t);
        char buffer[30];
        std::strftime(buffer, sizeof(buffer), "%y%m%d%H%M%S", now);
        fileName = "";
        if ( loggerName != "" ) fileName += loggerName + "_";
        if ( loggerEntityName != "" ) fileName += loggerEntityName + "_";
        if ( loggerProperties != "" ) fileName += loggerProperties + "_";
        fileName += std::string(buffer) + ".csv";
        if ( loggerFilePath[loggerFilePath.length()-1] != '/' ) loggerFilePath += "/";
        fullPath = loggerFilePath + fileName;

        logFile.open(fullPath.c_str());

        // Set double print precision
        logFile << std::setprecision(std::numeric_limits<double>::max_digits10);

        // Write of header row of log file
        for (std::string k : loggerKeys)
        {
            logFile << k << " ";
        }
        logFile << std::endl;

        debug_error("FileLogger file name: " << fileName);
    } 
    else 
    {
        debug_error("FileLogger already open.");
    }

}

void selibdev::FileLogger::logClose()
{
    if (loggerClosed) return;
    
    if (logFile.is_open())
    {
        logFile.close();
    }

    selibdev::Logger::logClose();
}
