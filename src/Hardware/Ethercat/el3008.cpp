#include <vector>
#include <algorithm>
#include <math.h>
#include <iostream>

#include "selibdev/Hardware/Ethercat/el3008.hpp"

#define MIN_VOLTAGE  0.0
#define MAX_VOLTAGE  10.0
#define MAX_DISCRETE 0x7FFF


EL3008::EL3008(int slaveID, ec_slavet * ec_slave) : ELXXXX(slaveID, ec_slave)
{
}

int EL3008::setup(uint16_t slave)
{
  int retval;
  uint16_t u16val;

  // map velocity
  uint16_t map_1c12[1] = {0x1603};
  uint16_t map_1c13[1] = {0x1a04};

  retval = 0;

  // Set PDO mapping using Extended Mode

  retval += ec_SDOwrite(slave, 0x1c12, 0x01, TRUE, sizeof(map_1c12), &map_1c12, EC_TIMEOUTSAFE);
  retval += ec_SDOwrite(slave, 0x1c13, 0x01, TRUE, sizeof(map_1c13), &map_1c13, EC_TIMEOUTSAFE);

  // set other nescessary parameters as needed
  // .....

  // while(EcatError) printf("%s", ec_elist2string());

  if (!(retval > 0))
  {
    return 0;
  }
  debug_info("EL3008 (slave # " << slave << ") setup completed");
  return 1;
}


double EL3008::scale(short value)
{
    return ((double)value * MAX_VOLTAGE / (double)MAX_DISCRETE);
}

double EL3008::getAnalog(int channel)
{
    return scale(getRaw(channel));
}

short EL3008::getRaw(int channel)
{
    return (short)((unsigned short)(ec_slave[slaveID].inputs[4*channel-2]) + ((unsigned short)(ec_slave[slaveID].inputs[4*channel-1]) << 8));
}

uint8 EL3008::getRaw1(int channel)
{
    return (ec_slave[slaveID].inputs[4*channel-2]);
}

uint8 EL3008::getRaw2(int channel)
{
    return (ec_slave[slaveID].inputs[4*channel-1]);
}

