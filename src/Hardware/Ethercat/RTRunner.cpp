#include "selibdev/Hardware/Ethercat/RTRunner.hpp"

#include <string>
#include <unistd.h>


#ifdef DEBUG
    #define DEBUG_INFO(x) std::cout << x
#else
    #define DEBUG_INFO(x) /*x*/
#endif
// #define LOOP_SCHED_PRIORITY 49
#define LOOP_SCHED_PRIORITY 80 // Suggested for PREEMPT_RT PATCHED KERNEL

// EL5101_0000
#define VENDOR_ID 0x00000002
#define PRODUCT_ID 0x13ed3052

#define RT_EPSILON 0.1 * RT_INTERVAL // ns

#define CHECK_INTERVAL 10000 // us

namespace // anonymous
{
    static inline void tsnorm(struct timespec *ts)
    {
        while (ts->tv_nsec >= NSEC_PER_SEC)
        {
            ts->tv_nsec -= NSEC_PER_SEC;
            ts->tv_sec++;
        }
    }

    template <typename T>
    int sgn(T val)
    {
        return (T(0) < val) - (val < T(0));
    }

    int stick_this_thread_to_core(int core_id)
    {
        int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
        if (core_id < 0 || core_id >= num_cores)
            return EINVAL;

        cpu_set_t cpuset;
        CPU_ZERO(&cpuset);
        CPU_SET(core_id, &cpuset);

        pthread_t current_thread = pthread_self();
        return pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
    }
} // anonymous


selibdev::RTRunner::RTRunner() :
    _ifname("eth0"),
    _taskFactory(nullptr),
    _running(false),
    _exitLoop(false)
{
    // ntd
}

selibdev::RTRunner::~RTRunner()
{
    // ntd
    //delete _hw;
}

selibdev::RTRunner &selibdev::RTRunner::getInstance()
{
    static RTRunner instance;

    return instance;
}

bool selibdev::RTRunner::start(selibdev::ISEHardware *hw, std::string const &ifname)
{
    _ifname = ifname;

    pthread_attr_t attr;
    pthread_attr_t *attrp = &attr;

    pthread_attr_init(attrp);
    pthread_attr_setinheritsched(attrp, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(attrp, SCHED_FIFO);

    sched_param param;
    sched_param *paramp = &param;

    param.sched_priority = LOOP_SCHED_PRIORITY;

    pthread_attr_setschedparam(attrp, paramp);

    //int iret1 = pthread_create(&(_ecatcheck_thread), nullptr, &RTRunner::_ecatcheck_helper, this);

    //if (iret1)
    //{
    //    printf("Failed to create error-handling thread\n");
    //    _running = false;
    //    return false;
    //}

    // moved this here from _configure()
    _hw.reset(hw);

    _exitLoop = false;
    int iret2 = pthread_create(&(_main_loop_thread), attrp, &RTRunner::_loop_helper, this);
    //int iret2 = 1; // failed to create
    if (iret2)
    {
        debug_error("Failed to create loop thread");
        _running = false;
        return false;
    }

    return true;
}

void selibdev::RTRunner::stop()
{
    if (_running)
    {
        _exitLoop = true; // exits from the main loop before joining
        //pthread_join(_error_handling_thread, nullptr);
        pthread_join(_main_loop_thread, nullptr);
    }
    ethcat::Board::cleanupHook();
    printf("RTRunner stopped!\n");
}

void selibdev::RTRunner::setTaskFactory(selibdev::LoopTaskFactory *tf)
{
    _taskFactory = tf;
}

bool selibdev::RTRunner::_configure()
{
    selibdev::ConfigurationManager *conf =
        selibdev::ConfigurationManager::getInstance();

    auto Jm = conf->getConfig<double>(
        selibdev::ConfigurationManager::LibConfigs::MOTOR_INERTIA
    );
    auto Kt = conf->getConfig<double>(
        selibdev::ConfigurationManager::LibConfigs::MOTOR_TORQUE_CONST
    );
    auto Ks = conf->getConfig<double>(
        selibdev::ConfigurationManager::LibConfigs::KSPRING
    );

    // TODO: use ConfigurationManager
    // conf->setConfig(ConfigurationManager::LibConfigs::COULOMB_FRICTION_P,COULOMB_FRICTION_POS)
    // conf->setConfig(ConfigurationManager::LibConfigs::COULOMB_FRICTION_P,COULOMB_FRICTION_POS)
    // _hw->coulombFrictionP = COULOMB_FRICTION_POS;
    // _hw->coulombFrictionN = COULOMB_FRICTION_NEG;
    // _hw->viscousFrictionP = VISCOUS_FRICTION_POS;
    // _hw->viscousFrictionN = VISCOUS_FRICTION_NEG;
    // _hw->currentSaturation = CUR_SAT;

    // Hardware init

    // Physical constants are set by the constructor
    // It also configures the communication with the etercat hardware
    // _hw.reset(new selibdev::SEHardware(Jm, Ks, Kt)); TODO was done here this thing
    //_hw = new SEHardware(Jm, Ks, Kt);

    if (!_hw->isConfigured())
        return false;

    _hw->setCurrent(0.0);

    return true;
}

void *selibdev::RTRunner::_loop_helper(void *ptr)
{
    stick_this_thread_to_core(0);
    return reinterpret_cast<RTRunner*>(ptr)->_loop();
    // dummy thread
    //return reinterpret_cast<RTRunner *>(ptr)->_dummy();
}

void *selibdev::RTRunner::_dummy(void)
{
    int real_time_flag = 0;

    printf("Started main loop thread, ");

    _running = true;

    _time = 0;

    //retrieve the real time
    clock_gettime(CLOCK_MONOTONIC, &_t);
    _start_time = (_t.tv_sec + _t.tv_nsec / NSEC_PER_SEC);
    _t.tv_nsec += RT_INTERVAL;
    tsnorm(&_t);
    clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &_t, nullptr);

    while (!_exitLoop)
    {
        if (!_check_rt())
        {
            fprintf(stderr, "HELP! OUT OF REALTIME! - %d\n", real_time_flag);
            ++real_time_flag;
            // Try to recover... maybe
            clock_gettime(CLOCK_MONOTONIC, &_t);
        }
        //time [s] variable update
        _time = (_t.tv_sec + _t.tv_nsec / NSEC_PER_SEC) - _start_time;

        //THE TASK LOOP!!!
        //_task = LoopTask::getCurrentTask();
        //repeat = _task->loop(_time - _prev_time) > 0;
        _prev_time = _time;

        // REALTIME: Calculate next shot
        _t.tv_nsec += RT_INTERVAL;
        tsnorm(&_t);

        // REALTIME: Wait until next shot
        clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &_t, nullptr);
    }

    printf("Main loop thread stopped\n");
    _running = false;
    pthread_exit(0);
    return nullptr;
}

void *selibdev::RTRunner::_loop()
{
    int real_time_flag = 0;

    printf("Started main loop thread, ");

    if (!_configure())
    {
        fprintf(stderr, "Configuration failed!\n");
        _running = false;
        return nullptr;
    }

    _running = true;

    selibdev::LoopTask *tc = _taskFactory->createTask(_hw.get());
    //selibdev::LoopTask *tc = _taskFactory->createTask(_hw);

// TODO: these should be moved to selibtest
#ifdef TORQUE_SENSORS_USE_DEFINES // TODO too much custom: initTask and TORQUE_CONSTANTS should be defined in selibtest
    _hw->setTorqueOffset(GLOBAL_TORQUE_OFFSET);
    _hw->setLinkTorqueOffset(GLOBAL_LINK_TORQUE_OFFSET);
    _hw->setExternalTorqueOffset(GLOBAL_EXTERNAL_TORQUE_OFFSET);
    selibdev::LoopTask::setCurrentTask(tc);
#else
    selibdev::InitTask *init = new selibdev::InitTask(_hw.get(), tc);
    //selibdev::InitTask *init = new selibdev::InitTask(_hw, tc);
    selibdev::LoopTask::setCurrentTask(init);
#endif

    //this method retrieves/updates the data from/to the hardware (force checks)
    if(not ethcat::Board::checkHook()){
        std::cerr << "checkhook failed!!" << std::endl;
    }
    _hw->update();
    // NOTE: removed sw reset to maintain encoders positions between execution
    //_hw->resetM();
    //_hw->resetE();
    _hw->refresh(TS);

    _time = 0;

    //retrieve the real time
    clock_gettime(CLOCK_MONOTONIC, &_t);
    _start_time = (_t.tv_sec + _t.tv_nsec / NSEC_PER_SEC);
    _t.tv_nsec += RT_INTERVAL;
    tsnorm(&_t);
    clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &_t, nullptr);

    bool repeat = true;

    while (!_exitLoop && repeat)
    {
        if (!_check_rt())
        {
            fprintf(stderr, "HELP! OUT OF REALTIME! - %d\n", real_time_flag);
            ++real_time_flag;
            // Try to recover... maybe
            clock_gettime(CLOCK_MONOTONIC, &_t);
        }
        //time [s] variable update
        _time = (_t.tv_sec + _t.tv_nsec / NSEC_PER_SEC) - _start_time;

        //THE TASK LOOP!!!
        repeat = selibdev::LoopTask::getCurrentTask()->loop(_time - _prev_time) > 0;
        _prev_time = _time;
        _safety();

        // Perform the update, to send/receive ethercat slaves data
        // With the last SOEM version, one update is enough
        _hw->update();

        // REALTIME: Calculate next shot
        //t.tv_nsec += (RT_INTERVAL - RT_ETHERCAT_DELAY);
        _t.tv_nsec += RT_INTERVAL;
        tsnorm(&_t);

        // REALTIME: Wait until next shot
        clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &_t, nullptr);
    }

    selibdev::LoopTask::getCurrentTask()->dispose();

    _running = false;

    printf("Main loop thread stopped\n");
    return nullptr;
}

bool selibdev::RTRunner::_check_rt()
{
    //retrieve the time
    struct timespec t_real;
    clock_gettime(CLOCK_MONOTONIC, &t_real);
    //if (std::abs(t_real.tv_sec * NSEC_PER_SEC + t_real.tv_nsec - (_t.tv_sec * NSEC_PER_SEC + _t.tv_nsec)) > RT_EPSILON)
    if (t_real.tv_sec * NSEC_PER_SEC + t_real.tv_nsec - (_t.tv_sec * NSEC_PER_SEC + _t.tv_nsec) > RT_EPSILON)
        return false;
    return true;
}

void selibdev::RTRunner::_safety()
{
    static int unsafety;

    if (abs(_hw->getDThetaE()) > 200)
        unsafety++;

    if (unsafety > 1000)
    {
        _exitLoop = true;
        _hw->setCurrent(0.0);
        fprintf(stderr, "Safety Limits Excedeed!!!\n");
    }
}

void *selibdev::RTRunner::_ecatcheck_helper(void *ptr)
{
    return reinterpret_cast<RTRunner *>(ptr)->_ecatcheck();
}

void *selibdev::RTRunner::_ecatcheck()
{
    printf("Started EtherCAT error-checking thread\n");
    while (_running)
    {
        if (!ethcat::Board::checkHook())
        {
            fprintf(stderr, "EtherCat check failed!! Stopping...\n");
            _exitLoop = true;
        }
        usleep(CHECK_INTERVAL);
        // osal_usleep(CHECK_INTERVAL);
    }
    printf("Stopping error-checking thread\n");
    return nullptr;
}
