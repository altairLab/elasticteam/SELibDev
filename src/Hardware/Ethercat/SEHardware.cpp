//
// Created by noemurr on 01/04/19.
//

#include "selibdev/Hardware/Ethercat/SEHardware.hpp"
#include "selibdev/Hardware/Ethercat/el3102.hpp"
#include "selibdev/Hardware/Ethercat/el2202.hpp"
#include "selibdev/Hardware/Ethercat/el4004.hpp"
#include "selibdev/Hardware/Ethercat/el5152.hpp"
#include "selibdev/Hardware/Ethercat/el1202.hpp"
#include "selibdev/Utility/ConfigurationManager.hpp"

using namespace selibdev;

selibdev::SEHardware::SEHardware(double Jm, double Kspring, double Kt)
    : envelopeFilterE(selibdev::DigitalFilter::getLowPassFilterHz(1)),
      envelopeFilterM(selibdev::DigitalFilter::getLowPassFilterHz(1)) {
    this->Jm = Jm;
    this->Kspring = Kspring;
    this->Kt = Kt;
    // TODO: use ConfigurationManager

    selibdev::ConfigurationManager *conf =
        selibdev::ConfigurationManager::getInstance();

    this->strain_gauge_gain = conf->getConfig<double>(
        selibdev::ConfigurationManager::LibConfigs::STRAIN_GAUGE_GAIN
    );
    this->torqueE_gain = conf->getConfig<double>(
        selibdev::ConfigurationManager::LibConfigs::EXTERNAL_TORQUE_GAIN
    );

    this->coulombFrictionP = conf->getConfig<double>(
        selibdev::ConfigurationManager::LibConfigs::COULOMB_FRICTION_P
    );
    this->coulombFrictionN = conf->getConfig<double>(
        selibdev::ConfigurationManager::LibConfigs::COULOMB_FRICTION_N
    );
    this->viscousFrictionP = conf->getConfig<double>(
        selibdev::ConfigurationManager::LibConfigs::VISCOUS_FRICTION_P
    );
    this->viscousFrictionN = conf->getConfig<double>(
        selibdev::ConfigurationManager::LibConfigs::VISCOUS_FRICTION_N
    );
    this->currentSaturation = conf->getConfig<double>(
        selibdev::ConfigurationManager::LibConfigs::CUR_SATURATION
    );

    srand(time(nullptr));
    toRadians = 1.0 / (double) ENCODER_STEPS * 2 * M_PI;

    /*// DEFAULT initialization of signal differentiators
    torqueM_DerivativeFilter = AnalogFilter::getDifferentiatorHz(10.0);
    dtorqueM_DerivativeFilter = AnalogFilter::getDifferentiatorHz(10.0);

    torqueL_DerivativeFilter = AnalogFilter::getDifferentiatorHz(10.0);
    dtorqueL_DerivativeFilter = AnalogFilter::getDifferentiatorHz(10.0);

    torqueE_DerivativeFilter = AnalogFilter::getDifferentiatorHz(10.0);
    dtorqueE_DerivativeFilter = AnalogFilter::getDifferentiatorHz(10.0);

    envelopeFilterM = DigitalFilter::getLowPassFilterHz(1);
    envelopeFilterE = DigitalFilter::getLowPassFilterHz(1);*/
    setDerivativeFilterDTorqueM(
        selibdev::AnalogFilter::getDifferentiatorHz(10.0)
    );
    setDerivativeFilterDDTorqueM(
        selibdev::AnalogFilter::getDifferentiatorHz(10.0)
    );

    setDerivativeFilterDTorqueL(
        selibdev::AnalogFilter::getDifferentiatorHz(10.0)
    );
    setDerivativeFilterDDTorqueL(
        selibdev::AnalogFilter::getDifferentiatorHz(10.0)
    );
    setDerivativeFilterDTorqueE(
        selibdev::AnalogFilter::getDifferentiatorHz(10.0)
    );
    setDerivativeFilterDDTorqueE(
        selibdev::AnalogFilter::getDifferentiatorHz(10.0)
    );


    eth_name = conf->getConfig<std::string>(
        selibdev::ConfigurationManager::LibConfigs::ETHERCAT_CONN_NAME
    ).c_str();

    debug_info(eth_name);
    //boardInterface.reset(new Board(eth_name));
    //this method is supposed to configure the communication with the etercat 
    //hardware

    //configured = boardInterface->configureHook();
    // init of Board (should be init of group)
    if (not ethcat::Board::isStarted()) {
        ethcat::Board::startHook(eth_name);
    }

    if (not ethcat::Board::checkHook()) {
        throw std::runtime_error("ERROR hook not correct well configured");
    }
    // creating sheepboard
    boardInterface.addSlave<EL1202>("digitalInputs", 2, ec_slave);
    // the first channel of digitalInputs must 
    // be set to true to enable the motor.
    boardInterface.addSlave<EL2202>(
        "digitalOutputs", 3, ec_slave
    ).setOutput(1, true);
    boardInterface.addSlave<EL3102>("analogInputsB", 4, ec_slave);
    boardInterface.addSlave<EL5152>("encoders", 5, ec_slave);
    boardInterface.addSlave<EL3102>("analogInputs", 6, ec_slave);
    boardInterface.addSlave<EL4004>("analogOutputs", 7, ec_slave);

    setCurrent(0.0);

    this->zeroMOTOR = 0;
    this->zeroLOAD = 0;

    configured = true;
}

void selibdev::SEHardware::setCurrent(double value){
    current_command = curSaturation(value) * SET_CURRENT_CONVERSION;
    auto &ai = boardInterface.getSlave<EL4004>("analogOutputs");
    ai.setAnalog_1_2(current_command);
}

double selibdev::SEHardware::curSaturation(double in) {
    if (fabs(in) > currentSaturation) {
        debug_info("SATURATION!!");
        saturation_flag = true;
        return currentSaturation * __sign(in);
    } else {
        saturation_flag = false;
        return in;
    }
}

bool selibdev::SEHardware::getIndexM() {
    return boardInterface.getSlave<EL1202>("digitalInputs").getInput2();
}

bool selibdev::SEHardware::getIndexE() {
    return boardInterface.getSlave<EL1202>("digitalInputs").getInput1();
}

double selibdev::SEHardware::getStrainGaugeValue() {
    return strain_gauge_value;
}

void selibdev::SEHardware::setStrainGaugeOffset(double offset) {
    strain_gauge_offset = offset;
}

void selibdev::SEHardware::resetM() {
    zeroMOTOR =
        boardInterface.getSlave<EL5152>("encoders").getPosition1();
    prev_encoderMOTOR = zeroMOTOR;
    prev_thetaM = thetaM;
    envelopeFilterM->clean();
    prev_diffthetaM = diffthetaM;
}

void selibdev::SEHardware::resetE() {
    zeroLOAD =
        boardInterface.getSlave<EL5152>("encoders").getPosition2();
    prev_encoderLOAD = zeroLOAD;
    prev_thetaE = thetaE;
    envelopeFilterM->clean();
    prev_diffthetaM = diffthetaM;
}

void selibdev::SEHardware::refresh(double dt) {
    auto &analogInputs = boardInterface.getSlave<EL3102>("analogInputs");
    auto &analogInputsB = boardInterface.getSlave<EL3102>("analogInputsB");
    auto &encoders = boardInterface.getSlave<EL5152>("encoders");

    //torque/current
    active_current  =   (double) analogInputsB.getAnalog1() * 
                        READ_CURRENT_CONVERSION; 

    active_tau_m    =   active_current * Kt;

    updateMotor(dt);
    updateLink(dt);
    updateExternal(dt);

    //encoder position in rad
    thetaM          =   -(double) (encoders.getPosition2() - zeroMOTOR) * 
                        toRadians; //encoders
    thetaE          =   -(double) (encoders.getPosition1() - zeroLOAD) * 
                        toRadians; //encoders

    //encoder velocity in rad
    //1. diff velocity
    //it is better to make a int difference!!
    diffthetaM      =   (double) (-encoders.getPosition2() + prev_encoderMOTOR) 
                        * (toRadians / dt); 
    diffthetaE      =   (double) (-encoders.getPosition1() + prev_encoderLOAD) 
                        * (toRadians / dt); 

    //2.period velocity

    //if the position is about the same the period is incremented 
    // -> about means within 1e-10 (can be tuned)
    //otherwise the new period value is read and converted in seconds

    if (fabs(thetaM - prev_thetaM) < ENCODER_RESOLUTION) periodM += dt;
    else
        //encoder period in second: 4 step per pulse * 1e-7 time conversion
        periodM = 0.25e-7 * encoders.getPeriod2(); 

    if (fabs(thetaE - prev_thetaE) < ENCODER_RESOLUTION) periodE += dt;
    else
        //encoder period in second: 4 step per pulse * 1e-7 time conversion
        periodE = 0.25e-7 * encoders.getPeriod1(); 

    //sign
    if (diffthetaM != 0) signM = diffthetaM > 0 ? 1.0 : -1.0;
    if (diffthetaE != 0) signE = diffthetaE > 0 ? 1.0 : -1.0;
    period_dthetaM = signM * (2 * M_PI / ENCODER_STEPS) / periodM;
    period_dthetaE = signE * (2 * M_PI / ENCODER_STEPS) / periodE;

//  //this results in chattering because the sign of 0 is positive!
//  period_dthetaM = sign(diffthetaM) * (2*M_PI / ENCODER_STEPS) / periodM;
//	period_dthetaE = sign(diffthetaE) * (2*M_PI / ENCODER_STEPS) / periodE;

    //sometimes I read spikes in the velocity
    //outlier removal 1
    //sudden increments > 10 [rad/s] are skipped and I get 
    //slowly closer to the period value
    //se ho un incremento improvviso > 10 lo cazzio subito. 
    // e mi avvicino lentamente al nuovo valore di period

    dthetaM = period_dthetaM;
    dthetaE = period_dthetaE;

    inc_dthetaM = fabs(dthetaM - prev_dthetaM);
    inc_dthetaE = fabs(dthetaE - prev_dthetaE);

    if (inc_dthetaM > 10) {
        dthetaM = prev_dthetaM + __sign(period_dthetaM - prev_dthetaM);
        debug_info("velocity_outlier_M");
    }
    if (inc_dthetaE > 10) {
        dthetaE = prev_dthetaE + __sign(period_dthetaE - prev_dthetaE);
        debug_info("velocity_outlier_E");
    }

//  outlier removal 2 (alternative outlier removal based on envelope - 
//  works worse)
//	dthetaM_envelope = envelopeFilterM->process(fabs(prev_dthetaM));
//  dthetaE_envelope = envelopeFilterE->process(fabs(prev_dthetaE));
//	if( inc_dthetaM > 0.5*dthetaM_envelope) 
//        dthetaM = prev_dthetaM + sign(period_dthetaM-prev_dthetaM);
//	if( inc_dthetaE > 0.5*dthetaE_envelope) 
//        dthetaE = prev_dthetaE + sign(period_dthetaE-prev_dthetaE);

//	//artificial noise (just for test)
//	i++;
//	rand_amp = 0.0;
//	randM = drand()*rand_amp - rand_amp/2;
//	randE = drand()*rand_amp - rand_amp/2;
//	dthetaM += randM;
//	dthetaE += randE;

    //check if we are at stand still
    if (fabs(thetaM - prev_thetaM) < 2*ENCODER_RESOLUTION) standStillTimeM += dt;
    else standStillTimeM = 0;

    if (fabs(thetaE - prev_thetaE) < 2*ENCODER_RESOLUTION) standStillTimeE += dt;
    else standStillTimeE = 0;

    //acceleration
    ddthetaM            = (dthetaM - prev_dthetaM) / dt;
    ddthetaE            = (dthetaE - prev_dthetaE) / dt;

    thetaM              = (thetaM_Filter == nullptr)    
                            ? thetaM   : thetaM_Filter->process(thetaM, dt);

    dthetaM             = (dthetaM_Filter == nullptr)   
                            ? dthetaM  : dthetaM_Filter->process(dthetaM, dt);

    ddthetaM            = (ddthetaM_Filter == nullptr)  
                            ? ddthetaM : ddthetaM_Filter->process(ddthetaM, dt);

    thetaE              = (thetaE_Filter == nullptr)    
                            ? thetaE   : thetaE_Filter->process(thetaE, dt);

    dthetaE             = (dthetaE_Filter == nullptr)   
                            ? dthetaE  : dthetaE_Filter->process(dthetaE, dt);

    ddthetaE            = (ddthetaE_Filter == nullptr)  
                            ? ddthetaE : ddthetaE_Filter->process(ddthetaE, dt);

    //updates
    prev_encoderMOTOR   = encoders.getPosition2();
    prev_encoderLOAD    = encoders.getPosition1();

    prev_thetaM         = thetaM;
    prev_thetaE         = thetaE;

    prev_dthetaM        = dthetaM;
    prev_dthetaE        = dthetaE;

    prev_torqueM        = torqueM;
    prev_torqueL        = torqueL;
    prev_torqueE        = torqueE;

}

void selibdev::SEHardware::updateMotor(double dt)
{
    auto &analogInputs = boardInterface.getSlave<EL3102>("analogInputs");
    torqueM = (double) analogInputs.getAnalog1() * 
                READ_TORQUE_CONVERSION - torqueM_offset;
                
    torqueM = (torqueM_Filter == nullptr) ? torqueM : torqueM_Filter->process(torqueM, dt);

    dtorqueM = dtorqueM_DerivativeFilter->process(torqueM, dt);
    dtorqueM = (dtorqueM_Filter == nullptr) ? dtorqueM : dtorqueM_Filter->process(dtorqueM, dt);

    ddtorqueM = ddtorqueM_DerivativeFilter->process(dtorqueM, dt);
    ddtorqueM = (ddtorqueM_Filter == nullptr) ? ddtorqueM : ddtorqueM_Filter->process(ddtorqueM, dt);

    prev_torqueM = torqueM;
}

void selibdev::SEHardware::updateLink(double dt)
{
    auto &analogInputs = boardInterface.getSlave<EL3102>("analogInputs");
     
    strain_gauge_value = (double) analogInputs.getAnalog1();
    torqueL = (strain_gauge_value - strain_gauge_offset) * strain_gauge_gain;
    
    torqueL = (torqueL_Filter == nullptr) ? torqueL : torqueL_Filter->process(torqueL, dt);

    dtorqueL = dtorqueL_DerivativeFilterr->process(torqueL, dt);
    dtorqueL = (dtorqueL_Filter == nullptr) ? dtorqueL : dtorqueL_Filter->process(dtorqueL, dt);

    ddtorqueL = ddtorqueL_DerivativeFilter->process(dtorqueL, dt);
    ddtorqueL = (ddtorqueL_Filter == nullptr) ? ddtorqueL : ddtorqueL_Filter->process(ddtorqueL, dt);    

    prev_torqueL = torqueL;
}

void selibdev::SEHardware::updateExternal(double dt)
{
    //FORCE from futek sensor on environment
    auto &analogInputsB = boardInterface.getSlave<EL3102>("analogInputsB");

    torqueE = (double) analogInputsB.getAnalog1() * torqueE_gain - torqueE_offset; 
    torqueE = (torqueE_Filter == nullptr) ? torqueE : torqueE_Filter->process(torqueE, dt);
    
    dtorqueE = dtorqueE_DerivativeFilter->process(torqueE, dt);
    dtorqueE = (dtorqueE_Filter == nullptr) ? dtorqueE : dtorqueE_Filter->process(dtorqueE, dt);

    ddtorqueE = ddtorqueE_DerivativeFilter->process(dtorqueE, dt);
    ddtorqueE = (ddtorqueE_Filter == nullptr) ? ddtorqueE : ddtorqueE_Filter->process(ddtorqueE, dt);

    prev_torqueE = torqueE;
}

bool selibdev::SEHardware::isStandStill(double timeSec) {
    if (standStillTimeM > timeSec && standStillTimeE > timeSec) return true;
    else return false;
}