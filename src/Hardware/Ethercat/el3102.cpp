#include <vector>
#include <algorithm>
#include <math.h>
#include <iostream>

#include "selibdev/Hardware/Ethercat/el3102.hpp"

#define MIN_VOLTAGE  0.0
#define MAX_VOLTAGE  10.0
#define MAX_DISCRETE 0x7FFF


EL3102::EL3102(int slaveID, ec_slavet * ec_slave) : ELXXXX(slaveID, ec_slave)
{
}

double EL3102::scale(short value)
{
    return ((double)value * MAX_VOLTAGE / (double)MAX_DISCRETE);
}
        

short EL3102::getRaw1()
{
    return (short)((unsigned short)(ec_slave[slaveID].inputs[1]) + ((unsigned short)(ec_slave[slaveID].inputs[2]) << 8));
}

short EL3102::getRaw2()
{
    return (short)((unsigned short)(ec_slave[slaveID].inputs[4]) + ((unsigned short)(ec_slave[slaveID].inputs[5]) << 8));
}

double EL3102::getAnalog1()
{
    return scale(getRaw1());
}

double EL3102::getAnalog2()
{
    return scale(getRaw2());
}

