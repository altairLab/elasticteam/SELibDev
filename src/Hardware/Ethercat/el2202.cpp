#include <vector>
#include <algorithm>
#include <math.h>
#include <iostream>

#include "selibdev/Hardware/Ethercat/el2202.hpp"

EL2202::EL2202(int slaveID, ec_slavet * ec_slave) : ELXXXX(slaveID, ec_slave)
{
}

void EL2202::initOutput()
{
	ec_slave[slaveID].outputs[0] = 0x00;
}

void EL2202::setOutput(int channel, bool value)
{
	if(value)
		ec_slave[slaveID].outputs[0] = ec_slave[slaveID].outputs[0] | (0x01 << (( (channel-1)*2)));
	else
		ec_slave[slaveID].outputs[0] = ec_slave[slaveID].outputs[0] & !(0x01 << (( (channel-1)*2)));
}
