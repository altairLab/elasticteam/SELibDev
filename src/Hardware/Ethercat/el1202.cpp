#include <vector>
#include <algorithm>
#include <math.h>
#include <iostream>

#include "selibdev/Hardware/Ethercat/el1202.hpp"

EL1202::EL1202(int slaveID, ec_slavet * ec_slave) : ELXXXX(slaveID, ec_slave)
{
}

bool EL1202::getInput1()
{

	return ((ec_slave[slaveID].inputs[0] & 0x01) == 0x01);
}

bool EL1202::getInput2()
{

	return ((ec_slave[slaveID].inputs[0] & 0x02) == 0x02);
}
