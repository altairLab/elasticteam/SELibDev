#include <iomanip>
#include <sstream>
#include "selibdev/Hardware/Ethercat/elXXXX.hpp"

ELXXXX::ELXXXX(int slaveID, ec_slavet * ec_slave)
{
    this->slaveID = slaveID;
    this->ec_slave = ec_slave;
}
std::string ELXXXX::getInputsAsString()
{
    std::string out;

    for (unsigned int i = 0; i < ec_slave[slaveID].Ibytes; i++)
    {        
        out += UIntToHexStr(ec_slave[slaveID].inputs[i]);
    }

    return out;
}

std::string ELXXXX::getOutputsAsString()
{
    std::string out;

    for (unsigned int i = 0; i < ec_slave[slaveID].Obytes; i++)
    {        
        out += UIntToHexStr(ec_slave[slaveID].outputs[i]);
    }

    return out;
}

std::string ELXXXX::UIntToHexStr(unsigned int tmp)
{
        std::ostringstream out;
        out << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << tmp;
        return out.str();
}

int ELXXXX::getSlaveID() const {
    return slaveID;
}
