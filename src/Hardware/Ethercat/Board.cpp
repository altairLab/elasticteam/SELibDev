#include "selibdev/Hardware/Ethercat/Board.hpp"
#include <sstream>

extern "C" {
    #include <ethercat.h>
}

using namespace ethcat;

#ifdef DEBUG
    #define DEBUG_INFO(x) std::cout << x
#else
    #define DEBUG_INFO(x) /*x*/
#endif

///////////////////////////////// Configure Defines ////////////////////////////

#define IO_MAP_LENGTH 4096

/////////////////////////////// End Configure Defines //////////////////////////

char Board::IOmap[IO_MAP_LENGTH];
uint16_t Board::workerCount;
bool Board::started;

bool Board::configureHook() {
    // todo
    return true;
}

void Board::startHook(const std::string &ifname) {
    auto v = ec_init(ifname.c_str());
    if (v) {
        DEBUG_INFO("ec_init on " << ifname << " succeeded.\n");
        // find and auto-config slaves
        ec_configdc();
        //ec_dcsync0();

        if ( ec_config(false, &IOmap) > 0 ) {
            DEBUG_INFO(ec_slavecount << " slaves found and configured.\n");

            DEBUG_INFO("Slaves mapped, state to SAFE_OP.\n");
            // wait for all slaves to reach SAFE_OP state
            ec_statecheck(0, EC_STATE_SAFE_OP,  EC_TIMEOUTSTATE * 4);

            ec_slave[0].state = EC_STATE_OPERATIONAL;
            // send one valid process data to make outputs in slaves happy
            ec_send_processdata();
            workerCount = ec_receive_processdata(EC_TIMEOUTRET);
            // request OP state for all slaves
            ec_writestate(0);
            // wait for all slaves to reach OP state
            ec_statecheck(0, EC_STATE_OPERATIONAL,  EC_TIMEOUTSTATE * 4);


            if (ec_slave[0].state != EC_STATE_OPERATIONAL ) {
                std::stringstream ss;
                ss << "Operational state NOT reached for all slaves.";
                throw std::runtime_error(ss.str());
            } else {
                DEBUG_INFO("Operational state reached for all slaves.\n");
            }
        }
        started = true;
    } else {
        std::stringstream ss;
        ss << "ERROR: ec_init exits with status: " << v;
        started = false;
        throw std::runtime_error(ss.str());
    }
}

void Board::sendAndReceive() {
    // Send and receive process data
    ec_send_processdata();
    // Parse the received process data
    workerCount = ec_receive_processdata(EC_TIMEOUTRET);
}

bool Board::checkHook() {
    #define EC_TIMEOUTMON 500
    if( 
        (workerCount < (ec_group[0].outputsWKC * 2) + ec_group[0].inputsWKC) 
        || ec_group[0].docheckstate
    ) {
        // one ore more slaves are not responding
        ec_group[0].docheckstate = FALSE;
        ec_readstate();
        for (int slave = 1; slave <= ec_slavecount; slave++)
        {
            if (ec_slave[slave].state != EC_STATE_OPERATIONAL)
            {
                ec_group[0].docheckstate = TRUE;
                if (ec_slave[slave].state == (EC_STATE_SAFE_OP + EC_STATE_ERROR))
                {
                    DEBUG_INFO(
                        "ERROR : slave " << 
                        slave << " is in SAFE_OP + ERROR, attempting ack."
                    );
                    ec_slave[slave].state = (EC_STATE_SAFE_OP + EC_STATE_ACK);
                    ec_writestate(slave);
                }
                else if(ec_slave[slave].state == EC_STATE_SAFE_OP)
                {
                    DEBUG_INFO("WARNING : slave " << slave << " is in SAFE_OP, change to OPERATIONAL.");
                    ec_slave[slave].state = EC_STATE_OPERATIONAL;
                    ec_writestate(slave);
                }
                else if(ec_slave[slave].state > 0)
                {
                    if (ec_reconfig_slave(slave, EC_TIMEOUTMON))
                    {
                        ec_slave[slave].islost = FALSE;
                        DEBUG_INFO("MESSAGE : slave " << slave << " reconfigured");
                    }
                }
                else if(!ec_slave[slave].islost)
                {
                    ec_slave[slave].islost = TRUE;
                    DEBUG_INFO("ERROR : slave " << slave << " lost");
                }
            }
            if (ec_slave[slave].islost)
            {
                if(!ec_slave[slave].state)
                {
                    if (ec_recover_slave(slave, EC_TIMEOUTMON))
                    {
                        ec_slave[slave].islost = FALSE;
                        DEBUG_INFO("MESSAGE : slave " << slave << " recovered");
                    }
                }
                else
                {
                    ec_slave[slave].islost = FALSE;
                    DEBUG_INFO("MESSAGE : slave " << slave << " found");
                }
            }
        }
        if(!ec_group[0].docheckstate)
        {
            DEBUG_INFO("OK : all slaves resumed OPERATIONAL.");
        }
        return false;
    }
    return true;
}

void Board::stopHook() {
    ec_close();
    started = false;
    // todo
}

void Board::cleanupHook() {
    // todo
}


///////////////////////////////// Configure Undefines ////////////////////////////

#undef IO_MAP_LENGTH

/////////////////////////////// End Configure Undefines //////////////////////////