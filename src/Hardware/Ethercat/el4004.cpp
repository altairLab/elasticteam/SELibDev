#include <vector>
#include <algorithm>
#include <math.h>
#include <iostream>

#include "selibdev/Hardware/Ethercat/el4004.hpp"

#define MIN_VOLTAGE  0.0
#define MAX_VOLTAGE  10.0
#define MAX_DISCRETE 0x7FFF


EL4004::EL4004(int slaveID, ec_slavet * ec_slave) : ELXXXX(slaveID, ec_slave)
{
}

unsigned short EL4004::scale(double value)
{
    double inputLimited = 0;
    
    inputLimited = std::max(value, MIN_VOLTAGE);
    inputLimited = std::min(inputLimited, MAX_VOLTAGE);
    
    return (unsigned short) (inputLimited * (double)MAX_DISCRETE / MAX_VOLTAGE);
}
        

void EL4004::setAbsAnalog1(double value)
{
    unsigned short scaledValue = 0;

    scaledValue = scale(std::fabs(value));

    ec_slave[slaveID].outputs[0] = (unsigned char) scaledValue;
    ec_slave[slaveID].outputs[1] = (unsigned char) (scaledValue >> 8);
}

void EL4004::setAbsAnalog2(double value)
{
    unsigned short scaledValue = 0;

    scaledValue = scale(std::fabs(value));

    ec_slave[slaveID].outputs[2] = (unsigned char) scaledValue;
    ec_slave[slaveID].outputs[3] = (unsigned char) (scaledValue >> 8);
}

void EL4004::setAbsAnalog3(double value)
{
    unsigned short scaledValue = 0;

    scaledValue = scale(std::fabs(value));

    ec_slave[slaveID].outputs[4] = (unsigned char) scaledValue;
    ec_slave[slaveID].outputs[5] = (unsigned char) (scaledValue >> 8);
}

void EL4004::setAbsAnalog4(double value)
{
    unsigned short scaledValue = 0;

    scaledValue = scale(std::fabs(value));

    ec_slave[slaveID].outputs[6] = (unsigned char) scaledValue;
    ec_slave[slaveID].outputs[7] = (unsigned char) (scaledValue >> 8);
}

void EL4004::setAnalog_1_2(double value)
{
    if (value > 0)
    {
        setAbsAnalog1(value);
        setAbsAnalog2(0);
    }
    else
    {
        setAbsAnalog1(0);
        setAbsAnalog2(value);
    }
}

void EL4004::setAnalog_3_4(double value)
{
    if (value > 0)
    {
        setAbsAnalog3(value);
        setAbsAnalog4(0);
    }
    else
    {
        setAbsAnalog3(0);
        setAbsAnalog4(value);
    }
}
