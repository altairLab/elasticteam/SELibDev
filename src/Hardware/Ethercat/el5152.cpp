#include <iomanip>
#include <sstream>

#include "selibdev/defines.h"
#include "selibdev/Hardware/Ethercat/el5152.hpp"

#define ENC_RESET_VALUE 0x00000000
#define POS_ERROR 10


EL5152::EL5152(int slaveID, ec_slavet * ec_slave) 
    : ELXXXX(slaveID, ec_slave) {}

int EL5152::getPosition1()
{
    int encoderValue = 0;

    encoderValue = ((((int)ec_slave[slaveID].inputs[5]) << 24) +
                    (((int)ec_slave[slaveID].inputs[4]) << 16) +
                    (((int)ec_slave[slaveID].inputs[3]) << 8) +
                    ((int)ec_slave[slaveID].inputs[2]));

    return encoderValue;
}

int EL5152::getPosition2()
{
    int encoderValue = 0;

    encoderValue = ((((int)ec_slave[slaveID].inputs[15]) << 24) +
                    (((int)ec_slave[slaveID].inputs[14]) << 16) +
                    (((int)ec_slave[slaveID].inputs[13]) << 8) +
                    ((int)ec_slave[slaveID].inputs[12]));

    return encoderValue;
}

unsigned int EL5152::getPeriod1()
{
    unsigned int value = 0;

    value = ((((unsigned int)ec_slave[slaveID].inputs[8]) << 16) +
             (((unsigned int)ec_slave[slaveID].inputs[7]) << 8) +
              ((unsigned int)ec_slave[slaveID].inputs[6]));

    return value;
}

unsigned int EL5152::getPeriod2()
{
    unsigned int value = 0;

    value = ((((unsigned int)ec_slave[slaveID].inputs[18]) << 16) +
             (((unsigned int)ec_slave[slaveID].inputs[17]) << 8) +
              ((unsigned int)ec_slave[slaveID].inputs[16]));

    return value;
}

bool EL5152::resetEncoders()
{

    // Encoder 1 value
    ec_slave[slaveID].outputs[2] = ENC_RESET_VALUE;
    ec_slave[slaveID].outputs[3] = ENC_RESET_VALUE >> 8;
    ec_slave[slaveID].outputs[4] = ENC_RESET_VALUE >> 16;
    ec_slave[slaveID].outputs[5] = ENC_RESET_VALUE >> 24;

    // Encoder 2 value
    ec_slave[slaveID].outputs[8] = ENC_RESET_VALUE;
    ec_slave[slaveID].outputs[9] = ENC_RESET_VALUE >> 8;
    ec_slave[slaveID].outputs[10] = ENC_RESET_VALUE >> 16;
    ec_slave[slaveID].outputs[11] = ENC_RESET_VALUE >> 24;

    //enable the update
    ec_send_processdata();
    ec_receive_processdata(EC_TIMEOUTRET);

    ec_slave[slaveID].outputs[0] = ec_slave[slaveID].outputs[0] & 0xFB; //put the bit relative to "SetCounter" to FALSE
    ec_slave[slaveID].outputs[6] = ec_slave[slaveID].outputs[6] & 0xFB; //put the bit relative to "SetCounter" to FALSE
    ec_send_processdata();
    ec_receive_processdata(EC_TIMEOUTRET);

    ec_slave[slaveID].outputs[0] = ec_slave[slaveID].outputs[0] | 0x04; //put the bit relative to "SetCounter" to TRUE
    ec_slave[slaveID].outputs[6] = ec_slave[slaveID].outputs[6] | 0x04; //put the bit relative to "SetCounter" to TRUE
    ec_send_processdata();
    ec_receive_processdata(200000);

	//-------------------- todo remove?
	ec_slave[slaveID].outputs[0] = ec_slave[slaveID].outputs[0] | 0x04; //put the bit relative to "SetCounter" to TRUE
    ec_slave[slaveID].outputs[6] = ec_slave[slaveID].outputs[6] | 0x04; //put the bit relative to "SetCounter" to TRUE
    ec_send_processdata();
    ec_receive_processdata(200000);
    ec_slave[slaveID].outputs[0] = ec_slave[slaveID].outputs[0] | 0x04; //put the bit relative to "SetCounter" to TRUE
    ec_slave[slaveID].outputs[6] = ec_slave[slaveID].outputs[6] | 0x04; //put the bit relative to "SetCounter" to TRUE
    ec_send_processdata();
    ec_receive_processdata(200000);

    ec_slave[slaveID].outputs[0] = ec_slave[slaveID].outputs[0] | 0x04; //put the bit relative to "SetCounter" to TRUE
    ec_slave[slaveID].outputs[6] = ec_slave[slaveID].outputs[6] | 0x04; //put the bit relative to "SetCounter" to TRUE
    ec_send_processdata();
    ec_receive_processdata(200000);
	//---------------------

    ec_slave[slaveID].outputs[0] = ec_slave[slaveID].outputs[0] & 0xFB; //put the bit relative to "SetCounter" to FALSE
    ec_slave[slaveID].outputs[6] = ec_slave[slaveID].outputs[6] & 0xFB; //put the bit relative to "SetCounter" to FALSE
    ec_send_processdata();
    ec_receive_processdata(EC_TIMEOUTRET);


    if ((abs(this->getPosition1() - ENC_RESET_VALUE) > POS_ERROR)
        ||
        (abs(this->getPosition2() - ENC_RESET_VALUE) > POS_ERROR)
       )
    {
        debug_info("Encoder reset failed! ");
        debug_info("enc1: " << this->getPosition1());
        debug_info("enc2: " << this->getPosition2());

        return true;
    }

    return true;
}
