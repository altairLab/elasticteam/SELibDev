#include "Control/ImpedanceControl.hpp"
#include "defines.h"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

//ImpedanceControlBase::ImpedanceControlBase(IMotorHardware* hw) : MotorControlBase(hw)
//{
//    this->logFileName = "impedance";
//    theta_ref = 0;
//    tau_ref = 0;
//}

//void ImpedanceControlBase::LogOpen()
//{
//    filenameStream << LOG_DIRECTORY << logFileName << floor(drand()*100) <<"---k_des_" << k_des << "-d_des_" << d_des << ".csv";
//    string s(filenameStream.str());
//    logfile.open(s.c_str());
//}

//void ImpedanceControlBase::Log()
//{
//	logfile << ref << " ";
//	logfile << dref << " ";
//	logfile << ddref << " ";
//	logfile << tau << " ";
//	logfile << dtau << " ";
//	logfile << theta_m << " ";
//	logfile << dtheta_m << " ";
//	logfile << theta_e << " ";
//	logfile << dtheta_e << " ";
//	logfile << k_des << " ";
//	logfile << d_des << " ";
//    logfile << log1 << " ";
//    logfile << log2 << " ";
//    logfile << log3 << " ";
//    logfile << this->time << " ";
//    logfile << endl;
//}
//double ImpedanceControlBase::__process(double theta, double dtheta, double dt)
//{
//    return 0.0;
//}

using namespace selibdev;

SEImpedanceControlBase::SEImpedanceControlBase(ISEHardware* hw) : SEControl(hw)
{
    theta_ref = 0;
    tau_ref = 0;
    k_des = 1;
    d_des = 0;
    mgb_gravity_comp = 0;
}

double TrivialImpedanceControl::___process(double dt)
{
//	tau = Jm*ddtheta_m - k_des*theta_m - d_des*dtheta_m + (k_des/K_SPRING)*torque + (d_des/K_SPRING)*dtorque;
	out = - k_des*theta_m - d_des*dtheta_m + (k_des/Kspring)*tau + (d_des/Kspring)*dtau;
	out += frictionTorqueCompensation(dtheta_m) * KT;
	// in pratica bisogna aggiungere attrito sul motore!!
	// si possono anche mettere i set points
	return out;
}

double StiffAdmittanceControl::___process(double dt)
{
    //collocated admittance control with inner force loop
    if(d_des > 0)
    {
        if(A_over_s_ref == NULL)
        {
            //double a[2] = {Kspring * d_des, Kspring * k_des};
            //double b[2] = {0, Kspring - k_des};
            double b[2] = {0, 1}; //num
            double a[2] = {d_des, k_des}; //den

            A_over_s_ref = new AnalogFilter(1,a,b);
            A_over_s_dref = new AnalogFilter(1,a,b);
        }

        pid.ref = - A_over_s_ref->process(tau,dt);
        pid.dref = - A_over_s_dref->process(dtau,dt);
    }
    else
    {
        pid.ref = - ( (1/k_des) * (tau-tau_ref) - theta_ref );
        pid.dref = - ( (1/k_des) * (dtau) );
        pid.ref = 0;
        pid.dref = 0;

    }
    pid.ddref =  0;

    out = pid.process(theta_m,dtheta_m,dt);
    return out;
}

double BasicImpedanceControl::___process(double dt)
{
	//impedance control with inner force loop
    pid.ref =  - k_des * (theta_e - theta_ref) - d_des  * dtheta_e + mgb_gravity_comp*cos(theta_e);
    pid.dref =  - k_des * dtheta_e - d_des * ddtheta_e;
    pid.ddref =  - k_des * ddtheta_e;
    out = pid.process(tau,dtau,dt);
    return out;
}

double CollocatedAdmittanceControl::___process(double dt)
{
    //collocated admittance control with inner force loop

    if(d_des > 0)
    {
        if(A_over_s_ref == NULL)
        {
            //double a[2] = {Kspring * d_des, Kspring * k_des};
            //double b[2] = {0, Kspring - k_des};
            double a[2] = {d_des, k_des};
            double b[2] = {0, 1 - k_des/Kspring};

            A_over_s_ref = new AnalogFilter(1,a,b);
            A_over_s_dref = new AnalogFilter(1,a,b);
        }

    pid.ref = - A_over_s_ref->process(tau,TS);
    pid.dref = - A_over_s_dref->process(dtau,TS);
    }
    else
    {

        pid.ref  =  - (Kspring - k_des)/(Kspring * k_des) * tau;
        pid.dref =  - (Kspring - k_des)/(Kspring * k_des) * dtau;
    }

    pid.ddref =  0;
	out = pid.process(theta_m,dtheta_m,dt);
	return out;
}

double CollocatedImpedanceControl::___process(double dt)
{
	//collocated impedance control with inner force loop
	k_des_m = (Kspring * k_des) / (Kspring - k_des);

	pid.ref =  - k_des_m * theta_m - d_des  * dtheta_m;
	pid.dref =  - k_des_m * dtheta_m - d_des * ddtheta_m;
	pid.ddref =  0;

	out = pid.process(tau,dtau,dt);
	return out;
}

SMImpedanceControl::SMImpedanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw)
{
    double f = 10;
	double w = 2*M_PI*f;
	double lambda2 = powf(w,2);
	double lambda1 = 2.0*sqrt(lambda2);
	c = new IntegralSlidingModeForceControl(lambda1,lambda2,hw);
//	c = new SlidingModeForceControl(w,hw);
	c->ni = 0;
	c->phi = 5;
    c->torqueSensing = fromSpring;
}


double SMImpedanceControl::___process(double dt)
{
	//impedance control with inner force loop
	c->ref =  - k_des * theta_e - d_des  * dtheta_e;
	c->dref =  - k_des * dtheta_e - d_des * ddtheta_e;
	c->ddref =  - k_des * ddtheta_e;
//	c->ni = 0;
	out = c->process(tau,dtau,dt);
	return out;
}


VelocitySourcedImpedanceControl::VelocitySourcedImpedanceControl(ISEHardware* hw) : SEImpedanceControlBase(hw)
{
	c = new PassiveValleryForceControl(hw);
}

double VelocitySourcedImpedanceControl::___process(double dt)
{
	//impedance control with inner force loop
	c->ref =  - k_des * theta_e - d_des  * dtheta_e;
	c->dref =  - k_des * dtheta_e - d_des * ddtheta_e;
	c->ddref =  - k_des * ddtheta_e;
    debug_info(ref);
	out = c->process(tau,dtau,dt);
	return out;
}
