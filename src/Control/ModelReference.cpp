#include "Control/ModelReference.hpp"

using namespace selibdev;

ModelReference2::ModelReference2(double l1, double l2)
{
    this->l1 = l1;
    this->l2 = l2;
}

void ModelReference2::MRUpdate(double ref, double dt)
{
//	ref = Control::saturation(ref,1.0);

    //metodo simpletico (si invertono i calcoli di f1 e f2 e si arriva ad un semi implicito!!)
    f2 = -l1 * xr2 - l2 * xr1 + l2*ref;
    xr2 = prev_xr2 + dt * f2;

//	xr2 = max(xr2,15.0);

    f1 = xr2;
    xr1 = prev_xr1 + dt * f1;

    prev_xr1 = xr1;
    prev_xr2 = xr2;
}