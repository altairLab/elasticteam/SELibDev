#include "Control/PassiveControl.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

using namespace selibdev;

PassivePIDForceControl::PassivePIDForceControl(ISEHardware* hw) : SEControl(hw)
{
	this->kp = 10;
    this->kd = 0.1;
    this->ki = 0.45*kp;
    this->ff = 0;

	iFilter = DigitalFilter::getLowPassFilterHz(1);
}

double PassivePIDForceControl::___process(double dt)
{
    err	= tau - ref;
    derr = dtau - dref;
    ierr = iFilter->process(err);//integral with roll-off
    //ierr += err*dt; //non passive integral

    out = ff*ref - kp * err - kd * derr - ki * ierr;

    return out;
}

//void PassivePIDForceControl::Log()
//{
//	logfile << ref << " ";
//    logfile << dref << " ";
//    logfile << tau << " ";
//    logfile << dtau << " ";
//    logfile << theta_m << " ";
//    logfile << dtheta_m << " ";
//    logfile << ddtheta_m << " ";
//    logfile << theta_e << " ";
//    logfile << dtheta_e << " ";
//    logfile << ddtheta_e << " ";
//    logfile << this->time << " ";
//    logfile << endl;
//}

PDForceControlWithDampingInjection::PDForceControlWithDampingInjection(ISEHardware* hw) :
    SEControl(hw)
{
    this->kp = 1;
    this->kd = 0.0;
    this->ff = 1.0;
    this->kinj = 0.01;
}

double PDForceControlWithDampingInjection::___process(double dt)
{
    err	= tau - ref;
    derr = dtau - dref;
    out = ff*ref - kp * err - kd * derr - kinj * dtheta_m;
    return out;
}

PassivePrattForceControl::PassivePrattForceControl(double l1, double l2, ISEHardware* hw) :
    SEControl(hw)
{
	this->kp = A*l2 - 1; //IMPORTANT to be set in function of lambda2
	this->kd = A*l1; //IMPORTANT to be set in function of lambda1
}

double PassivePrattForceControl::___process(double dt)
{
    err	= tau - ref;
    derr = dtau - dref;

    out = -kp * err -kd * derr + A * ddref + ref + kb*Jm * ddtheta_e;
    debug_info("time: " << time);
    return out;
}

MRPassivePrattForceControl::MRPassivePrattForceControl(double l1, double l2, ISEHardware* hw) :
    PassivePrattForceControl(l1,l2,hw),
    ModelReference2(l1,l2)
{
	this->kp = A*l2 - 1; //IMPORTANT to be set in function of lambda2
	this->kd = A*l1; //IMPORTANT to be set in function of lambda1

    this->l1 = l1;
    this->l2 = l2;
}

double MRPassivePrattForceControl::___process(double dt)
{
	MRUpdate(this->ref, dt);
	ref = xr1;
	dref = xr2;
	ddref = f2;
	return PassivePrattForceControl::___process(dt);
}

PassiveValleryForceControl::PassiveValleryForceControl(ISEHardware* hw): PassivePIDForceControl(hw)
{
	//faulhaber tuning
	kpv = 1.2;
	kiv = 0.45*kpv; // < 0.5*kpv;
	v = new PID(kiv,kpv,0);
	kpf = 100;
	kif = 0.45*kpv; // < 0.5*kpf;
	kdf = 1; // > 4*tau_d^2*kpf = 4* (1/50Hz)^2 * kpf = 4*(1/50)^2*80 = 0.128
}

double PassiveValleryForceControl::___process(double dt)
{
    err	= tau - ref;
    derr = dtau - dref;
    ierr += err * dt;
    v->ref = - kif*ierr - kpf*err - kdf*derr;
    out = v->process(dtheta_m,0,dt);
    debug_info("time: " << time);
    return out;
}
