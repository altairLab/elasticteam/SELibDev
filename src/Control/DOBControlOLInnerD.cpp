#include "Control/DOBControlOLInnerD.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Davide Ciocca, Andrea Calanca
 * @date Oct 2016
 ****************************************************************************/

using namespace selibdev;

selibdev::DOBControlOLInnerD::DOBControlOLInnerD(double KP, double KD, double QcutoffHz, double alpha, ISEHardware *hw) 
    : DOBControlBase(KP, KD, QcutoffHz, hw)
{
    this->dynamicFFEnabled = false;

    this->KP = KP;
    this->KD = KD;
    this->FF = 1.0;
    this->alpha = alpha;

    this->w_q = 2 * M_PI * QcutoffHz;

    double *a_Q, *b_Q; //todo remove
    double *a_PninvQ, *b_PninvQ;

    // Q filter
    double temp_b_Q[2] = {0.0, w_q}; //num
    b_Q = temp_b_Q; //todo remove
    double temp_a_Q[2] = {1.0, w_q,}; //den
    a_Q = temp_a_Q;
    this->Q_filter = new AnalogFilter(1, a_Q, b_Q);

    // PninvQ filter
    double temp_b_PninvQ[2] = {KD * w_q, w_q}; // num
    b_PninvQ = temp_b_PninvQ;
    double temp_a_PninvQ[2] = {1, w_q}; //den
    a_PninvQ = temp_a_PninvQ;
    this->PninvQ_filter = new selibdev::AnalogFilter(1, a_PninvQ, b_PninvQ);
    this->PninvQ_filterFF = new selibdev::AnalogFilter(1, a_PninvQ, b_PninvQ);

    this->U_dob_prev = 0.0;
}


std::string selibdev::DOBControlOLInnerD::cb_getLoggerEntityNameString()
{ return "dDOB"; }

std::string selibdev::DOBControlOLInnerD::cb_getLoggerPropertiesString()
{ 
    std::ostringstream strs;
    strs << "kp" << KP << "_kd" << KD << "_cutoff_hz" << w_q / (2*M_PI) << "_alpha" << alpha; 
    return strs.str(); 
}

double selibdev::DOBControlOLInnerD::___process(double dt)
{
    double U;
    double Q_output, PninvQ_output, PninvQFF_output;

    err = ref - tau;
    U = KP * err + FF * ref;

//    PninvQFF_output = PninvQ_filterFF->process(ref, dt);
//    if(dynamicFFEnabled) U = Kp * err + PninvQFF_output;
//    else U = KP * err + FF * ref;

    Q_output = Q_filter->process(U_dob_prev, dt);
    PninvQ_output = PninvQ_filter->process(tau, dt);
    d_hat = PninvQ_output - Q_output;
    U_dob = U - alpha * d_hat;
    out = U_dob - KD * dtau; //innerD
 
    U_dob_prev = U_dob;

    return out;
}

