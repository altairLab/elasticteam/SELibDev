#include "Control/SlidingMode.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

using namespace selibdev;

SlidingModeForceControlBase::SlidingModeForceControlBase(double lambda, ISEHardware* hw) :
    SEControl(hw)
{
	this->lambda1 = lambda;

    //from actuators paper
	phi = 20;
	ni = 1.0;

	addEntryDouble("s", &s);
}

double SlidingModeForceControlBase::___process(double dt)
{
    err = tau - ref;
    derr = dtau - dref;
    s = slidingSurface(err,derr);
    out = controlLaw();
//	out += frictionCompensation(dtheta_m) * KT;
    return out;
}

SlidingModeForceControl::SlidingModeForceControl(double lambda, ISEHardware* hw) :
    SlidingModeForceControlBase(lambda,hw)
{
	this->kp = -1;
	this->kd = A*lambda1; //IMPORTANT to be set in function of lambda
}

double SlidingModeForceControl::slidingSurface(double err,double derr)
{
	s = derr + lambda1*err;
	return s;
}

double SlidingModeForceControl::controlLaw()
{
	//sliding strategy
	us = __sign(s);
    //continuous approx
	if(fabs(s) < phi) us =  s / phi;

    out =  - kp*err - kd*derr + A*ddref + ref + Jm * ddtheta_e - ni*us;
//	out =  err - A*lambda1*derr + A*ddref + ref + Jm * ddtheta_ef - ni*us;
//	out =      - A*lambda1*derr + A*ddref + ref + Jm * ddtheta_ef - ni*us; //modified stabler version

	return out;
}


IntegralSlidingModeForceControl::IntegralSlidingModeForceControl(double l1, double l2, ISEHardware* hw): SlidingModeForceControlBase(l1,hw)
{
	lambda2 = l2;
	this->kp = A*lambda2 - 1; //IMPORTANT to be set in function of lambda12
	this->kd = A*lambda1; //IMPORTANT to be set in function of lambda12
    this->ka = 0.0;

	//-------------- resonant model --------------------
	damp = 0.997;
	freq = 1;
	res = DigitalFilter::getResonatorHz(freq,damp);

	//------------------------------------------------------
}

double IntegralSlidingModeForceControl::slidingSurface(double err,double derr)
{

    if(fabs(s) > 50) ierr = -(derr + lambda1*err)/lambda2;//to avoid the reaching phase

	ierr += err*TS;
	s = derr + lambda1*err + lambda2 * ierr;

	return s;
}

double IntegralSlidingModeForceControl::controlLaw()
{
	//sliding strategy
	us = __sign(s);
    //continuous approx
	if(fabs(s) < phi)
	{
		us =  s / phi;
//		us =  s / phi + 0.15*res->process(s);
	}

    ddtheta_e = ddtheta_m - ddtau / sehw->Kspring;
    out = -kp*err -kd*derr + A*ddref + ref + ka*Jm * ddtheta_e - ni*us;
    return out;
}


//-------------------

// Super Twisting
SuperTwistingForceControl::SuperTwistingForceControl(double lambda, ISEHardware* hw) : SEControl(hw)
{
	ki = 5;
	ni = 0.15;
	rho = 0.5;

	a = Jm / Kspring;

	ddFilter = DigitalFilter::getLowPassFilterHz(100);

	this->ki = ki;
	this->ni = ni;
	this->rho = rho;
	this->lambda = lambda;

}

double SuperTwistingForceControl::___process(double dt){

	err = tau - ref;
	derr = dtau - dref;
	s = derr + lambda*err;

	ddtheta_e = (dtheta_e - prev_dtheta_e) / dt;
	ddtheta_ef = ddFilter->process(ddtheta_e);
	prev_dtheta_e = dtheta_e;


	// control law
	dua = -ki * __sign(s);
	ua += dua * dt;
	utw =  - ni * pow(fabs(s) , rho) * __sign(s) + ua;

	out = a * (ddref - lambda * derr) + ref + Jm * ddtheta_ef + utw;

	return out;
}
