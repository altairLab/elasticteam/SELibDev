#include "Control/ControlBase.hpp"
#include <time.h>

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date july 2015
 ****************************************************************************/

selibdev::ControlBase::ControlBase() : selibdev::Loggable()
{
    addEntryDouble("time", &time);
    addEntryDouble("out", &out);
    addEntryDouble("ref", &ref);
    addEntryDouble("dref", &dref);
    addEntryDouble("ddref", &ddref);
}

double selibdev::ControlBase::process(double theta, double dtheta, double dt)
{
    this->time += dt;

    _ret = _process(theta,dtheta,dt);
    
    getLogger()->log();

    return _ret;
}

void selibdev::ControlBase::dispose()
{
    getLogger()->logClose();
}

void selibdev::ControlBase::refSaturation(double lowThreshold, double upThreshold)
{
    if ( ref < lowThreshold )
    {
        ref = lowThreshold;
        dref = 0;
        ddref = 0;
    }
    else if ( ref > upThreshold )
    {
        ref = upThreshold;
        dref = 0;
        ddref = 0;
    }
}

inline double selibdev::ControlBase::saturation(double in, double threshold)
{
    double sat = std::max(in, -threshold);
    return std::min(sat, threshold);
}

inline double selibdev::ControlBase::saturation(double in, double lowThreshold, double upThreshold)
{
    double sat = std::max(in, lowThreshold);
    return std::min( sat, upThreshold);
}

/***********************************************************/

selibdev::MotorControlBase::MotorControlBase(IMotorHardware *hw) :
    selibdev::ControlBase(),
    hw(hw),
    Kt(hw->Kt), // just for ease of use !!
    Jm(hw->Jm)  // just for ease of use !!
{
    jerkMDifferentiatorFilter = DigitalFilter::getDifferentiatorHz(50);
    torqueObserverFilter = DigitalFilter::getLowPassFilterHz(5);

    g = 31.4;

    addEntryDouble("tau", &tau);
    addEntryDouble("dtau", &dtau);
    addEntryDouble("ddtau", &ddtau);

    addEntryDouble("smooth_dtau", &smooth_dtau);

    addEntryDouble("theta_m", &theta_m);
    addEntryDouble("dtheta_m", &dtheta_m);
    addEntryDouble("ddtheta_m", &ddtheta_m);

    addEntryDouble("tau_motor", &tau_motor);
    addEntryDouble("tau_link", &tau_link);
    addEntryDouble("tau_ext", &tau_ext);
    addEntryDouble("current", &current);
}

selibdev::MotorControlBase::~MotorControlBase()
{
    delete torqueObserverFilter;
    delete jerkMDifferentiatorFilter;
}

double selibdev::MotorControlBase::_process(double theta, double dtheta, double dt)
{	
	// motor position
	theta_m = hw->getThetaM();
	dtheta_m = hw->getDThetaM();
	ddtheta_m = hw->getDDThetaM();

    jerk_m = jerkMDifferentiatorFilter->process(hw->getDDThetaM());

    // motor torque (measured from sensor)
    switch(torqueSensing)
    {
        case fromSpring: // MotorControlBase does not account for a spring. This case is not managed here but is managed in the child SEControl
        case fromMotorSensor:
            tau = hw->getTorque();
            dtau = hw->getDTorque();
            ddtau = hw->getDDTorque();
            break;
        case fromLinkSensor:
            tau = hw->getLinkTorque();
            dtau = hw->getLinkDTorque();
            ddtau = hw->getLinkDDTorque();
            break;
       }

	// torque observer (virtual torque sensor)
	torqueObserver = torqueObserverFilter->process(Kt*hw->getCurrent() + g*Jm*dtheta_m - frictionTorqueCompensation(dtheta_m)) - g*Jm*dtheta_m;
	diffTorqueObserver = (torqueObserver - prev_torqueObserver) / dt;
	prev_torqueObserver = torqueObserver;

    tau_motor = hw->getTorque();
    tau_link = hw->getLinkTorque();
    tau_ext = hw->getExternalTorque();
    current = hw->getCurrent();
    
	return __process(theta, dtheta, dt);
}

/***********************************************************/

selibdev::SEControl::SEControl(ISEHardware *hw) :
    selibdev::MotorControlBase(dynamic_cast<IMotorHardware*>(hw)),
    sehw(hw),
    A(Jm / hw->Kspring),            // for ease of use !!
    Kspring(hw->Kspring)       // for ease of use !!
{
    this->torqueSensing = fromSpring;
    
    addEntryDouble("theta_e", &theta_e);
    addEntryDouble("dtheta_e", &dtheta_e);
    addEntryDouble("ddtheta_e", &ddtheta_e);
    addEntryDouble("tau_spring", &tau_spring);
}

selibdev::SEControl::~SEControl()
{
    // ntd
}

double selibdev::SEControl::__process(double theta, double dtheta, double dt)
{
	theta_e = sehw->getThetaE();
	dtheta_e = sehw->getDThetaE();
	ddtheta_e = sehw->getDDThetaE();

    tau_spring = Kspring * (theta_m - theta_e);

    // we may want to overwrite the torque measured from sensor with the torque measured from dispacement
    if(torqueSensing == fromSpring)
    {
        tau = Kspring * (theta_m - theta_e);
        dtau = Kspring * ( dtheta_m - dtheta_e );
        ddtau = Kspring * (ddtheta_m - ddtheta_e);
    }

	return ___process(dt);

}

/***********************************************************/

selibdev::PID::PID(double ki, double kp, double kd) :
    selibdev::ControlBase()
{
	KP = kp;
	KD = kd;
	KI = ki;
    integralSAT = 10;
	outFilter = NULL;//DigitalFilter::get50HzFilter();
	velFilter = NULL;
}

selibdev::PID::~PID()
{
    delete outFilter;
    delete velFilter;
}

double selibdev::PID::_process(double pos, double vel, double deltaTime)
{
    err = ref - pos;
	derr = (dref - vel);
    if ( velFilter != NULL ) derr = velFilter->process(derr);
	ierr += err * deltaTime;
	ierr = saturation(ierr,integralSAT);
	out = KP*err + KD*derr + KI*ierr;
    if ( outFilter != NULL ) out = outFilter->process(out);
	return out;
}

/***********************************************************/

selibdev::MotorPID::MotorPID(double ki, double kp, double kd, ISEHardware* hw) :
    selibdev::MotorControlBase(hw)
{
	pid = new selibdev::PID(ki,kp,kd);
}

selibdev::MotorPID::~MotorPID()
{
    delete pid;
}

double selibdev::MotorPID::__process(double y, double dy, double dt)
{
    pid->ref = ref;
    pid->dref = dref;
    pid->ddref = ddref;
	return pid->process(y,dy,dt);
}

/***********************************************************/

selibdev::MegaPDForceControl::MegaPDForceControl(ISEHardware *hw) :
    selibdev::SEControl(hw),
    kp(1.0),
    kd(0.0),
    ff(1.0),
    kinj(0.0),
    ni(0.0),
    phi(0.0)
{
    // ntd
}

selibdev::MegaPDForceControl::MegaPDForceControl(double ki, double kp, double kd, ISEHardware *hw) :
    selibdev::SEControl(hw),
    kp(kp),
    kd(kd),
    ff(1.0),
    kinj(0.0),
    ni(0.0),
    phi(0.0),
    _err(0.0),
    _derr(0.0),
    _us(0.0)
{
    // ntd
}

double selibdev::MegaPDForceControl::___process(double dt)
{
    _err = tau - ref;
    _derr = dtau - dref;

    _us = __sign(_err);
    if (std::fabs(_us) < phi) _us = _err/phi; // continuous approx

    out = ff*ref - kp * _err - kd * _derr - kinj * dtheta_m - ni * _us;
    return out;
}

std::string selibdev::MegaPDForceControl::cb_getLoggerEntityNameString()
{ return "megapd"; }

std::string selibdev::MegaPDForceControl::cb_getLoggerPropertiesString()
{ 
    std::ostringstream strs;
    strs << "kp" << kp << "_kd" << kd << "_ff" << ff << "_kinj" << kinj << "_ni" << ni << "_phi" << phi; 
    return strs.str(); 
}


/***********************************************************/

selibdev::SEVelocityControl::SEVelocityControl(double ki, double kp, double kd, ISEHardware *hw) :
    selibdev::SEControl(hw)
{
    pid = new selibdev::PID(ki,kp,kd);
}

double selibdev::SEVelocityControl::___process(double dt)
{
    pid->ref = ref;
    pid->dref = dref;
    pid->ddref = ddref;
	return pid->process(dtheta_m,0.0,dt);
}