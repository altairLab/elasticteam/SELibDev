#include "Control/EnergyControl.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Andrea Calanca
 * @date june 2015
 ****************************************************************************/

using namespace selibdev;

EnergyControl::EnergyControl(ISEHardware* hw) : SEControl(hw)
{
    filter = DigitalFilter::getLowPassFilterHz(20);
	mu = 1 + JL/Jm;
	w = sqrt(Kspring*(1/Jm + 1/JL));
//	energy_ref = 0.1;
//	k_e = 0.1;

}

double EnergyControl::___process(double dt)
{

	energy = 0.5 * (Jm*pow(dtheta_m,2) + JL*pow(dtheta_e,2) + Kspring*pow(theta_m-theta_e,2));
	e = energy_ref - energy;
    kv = k_e*e;
	out = kv*(dtheta_m);
//	out = kv*sin(w*time);

	//friction compensation
	fcomp = frictionTorqueCompensationColoumb(dtheta_m);
//	fcomp = frictionTorqueCompensation(dtheta_m);

    fcomp += __sign(dtheta_e)*0.012 * Kt;
//	fcomp += dtheta_e*0.0002 * Kt;

    debug_info(energy);
	return out + fcomp;
}
