#include "Control/DOBControlOL.hpp"

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Davide Ciocca, Andrea Calanca
 * @date Oct 2016
 ****************************************************************************/

selibdev::DOBControlBase::DOBControlBase(double KP, double KD, double QcutoffHz, selibdev::ISEHardware *hw) : selibdev::SEControl(hw)
{
    this->KP = KP;
    this->KD = KD;
    this->QcutoffHz = QcutoffHz;

    this->FF = 1.0;

    this->w_q = 2 * M_PI * QcutoffHz;
    this->w_q2 = pow(w_q, 2);

    this->xi_q = 1.4;

    this->dm = 0.0;
    // this->dm = 0.0182; // Viscous friction of the motor
    // this->dm = 0.1;
    this->de = 0.0;
    // this->de = 0.2;

    this->Jl = (selibdev::ConfigurationManager::getInstance())->getConfig<double>(selibdev::ConfigurationManager::LINK_INERTIA);

    addEntryDouble("d_hat", &d_hat);
}

std::string selibdev::DOBControlBase::cb_getLoggerPropertiesString() 
{
    std::ostringstream strs;
    strs << "kp" << KP << "_kd" << KD << "_Qcutoff" << QcutoffHz << "_Qxi" << xi_q; 
    return strs.str(); 
}

selibdev::DOBControlOL::DOBControlOL(double KP, double KD, double QcutoffHz, selibdev::ISEHardware *hw) : selibdev::DOBControlBase(KP, KD, QcutoffHz, hw) {
    this->dynamicFFEnabled = false;

    // Q filter
    double temp_a_Q[3] = {1.0, 2 * xi_q * w_q, pow(w_q, 2)};
    this->a_Q = temp_a_Q;
    double temp_b_Q[3] = {0.0, 0.0, pow(w_q, 2)};
    this->b_Q = temp_b_Q;
    this->Q_filter = new selibdev::AnalogFilter(2, a_Q, b_Q);

    // QPninvQ filter
    double temp_a_PninvQ[3] = {1 / pow(w_q, 2), 2 * xi_q / w_q, 1.0};
    this->a_PninvQ = temp_a_PninvQ;
    double temp_b_PninvQ[3] = {Jm / Kspring, dm / Kspring, 1.0};
    this->b_PninvQ = temp_b_PninvQ;

    this->PninvQ_filter = new selibdev::AnalogFilter(2, a_PninvQ, b_PninvQ);
    this->PninvQ_filterFF = new selibdev::AnalogFilter(2, a_PninvQ, b_PninvQ);

    this->Ud_prev = 0.0;
}

double selibdev::DOBControlOL::___process(double dt)
{
    double U;
    double Q_output, PninvQ_output, PninvQFF_output;

    // Error references
    err = ref - tau;
    derr = dref - dtau;

    // Filter ouput:
    PninvQFF_output = PninvQ_filterFF->process(ref, dt);

    // PD Controller
    if(dynamicFFEnabled) U = KP * err + KD * derr + PninvQFF_output;
    else U = KP * err + KD * derr + ref * FF;

    // Filter outputs:
    Q_output = Q_filter->process(Ud_prev, dt);
    PninvQ_output = PninvQ_filter->process(tau, dt);

    d_hat = PninvQ_output - Q_output;
    Ud = U - d_hat;
    Ud_prev = Ud;
    return Ud;
}

std::string selibdev::DOBControlOL::cb_getLoggerEntityNameString() 
{
    return "dob_ol";
}


selibdev::DOBControlOL3order::DOBControlOL3order(double KP, double KD, double QcutoffHz, selibdev::ISEHardware *hw) : selibdev::DOBControlBase(KP, KD, QcutoffHz, hw)
{
    this->FF = 0.0;

    this->Ud_prev = 0.0;

    // Q filter parameters
    double temp_a_Q[3] = {1.0, 2 * xi_q * w_q, w_q2};
    this->a_Q = temp_a_Q;
    double temp_b_Q[3] = {0.0, 0.0, w_q2};
    this->b_Q = temp_b_Q;
    this->Q_filter = new selibdev::AnalogFilter(2, a_Q, b_Q);

    // PninvQ filter parameters
    double temp_a_PninvQ[4] = {Jl, de + 2 * xi_q * w_q * Jl, w_q * (2 * xi_q * de + w_q * Jl), de * w_q2};
    this->a_PninvQ = temp_a_PninvQ;
    double temp_b_PninvQ[4] = {w_q2 * Jl * Jm, w_q2 * (Jl * dm + Jm * de), w_q2 * (de * dm + Kspring * (Jm + Jl)), w_q2 * Kspring * (dm + de)};
    this->b_PninvQ = temp_b_PninvQ;

    this->PninvQ_filter = new selibdev::AnalogFilter(2, a_PninvQ, b_PninvQ);
    this->PninvQ_FFfilter = new selibdev::AnalogFilter(2, a_PninvQ, b_PninvQ);
}

double selibdev::DOBControlOL3order::___process(double dt) {
    double U;
    double Q_output, PninvQ_output, FF_output;

    // Error references
    err = ref - tau;
    derr = dref - dtau;

    // Filter ouput:
    FF_output = PninvQ_FFfilter->process(ref, dt);

    // PD Controller
    if(dynamicFFEnabled) U = KP * err + KD * derr + FF_output;
    else U = KP * err + KD * derr + ref * FF;

    // Filter outputs:
    Q_output = Q_filter->process(Ud_prev, dt);
    PninvQ_output = PninvQ_filter->process(tau, dt);

    d_hat = PninvQ_output - Q_output;
    Ud = U - d_hat + 0.0 * Jm * ddtheta_e;
    Ud_prev = Ud;

    return Ud;
}

std::string selibdev::DOBControlOL3order::cb_getLoggerEntityNameString() 
{
    return "dob_ol3";
}