#include "Control/AnalogFilter.hpp"
#include "Control/ControlBase.hpp"

#include <algorithm>
#include <iterator>

#define MAX_ORDER 8

using namespace selibdev;

/****************************************************************************
 * Copyright (C) 2015 Andrea Calanca
 * @author Davide Ciocca, Andrea Calanca
 * @date Oct 2016
 ****************************************************************************/

// Class constructor
AnalogFilter::AnalogFilter(int filter_order, double *a, double *b) {
    order = std::min(filter_order, MAX_ORDER);

    alpha = std::vector<double>(order + 1, 0.0);
    beta = std::vector<double>(order + 1, 0.0);
    beta_hat = std::vector<double>(order + 1, 0.0);

    x = std::vector<double>(order, 0.0);
    x_old = std::vector<double>(order, 0.0);
    f = std::vector<double>(order, 0.0);

    for (int i = 0; i <= order; i++) {
        alpha[i] = a[i] / a[0]; // attention!! problems when a[0]=0!!
        beta[i] = b[i] / a[0];
    }

    std::reverse(std::begin(alpha), std::end(alpha));
    std::reverse(std::begin(beta), std::end(beta));

    beta_hat[order] = beta[order];
    for (int i = 0; i < order; i++) { //todo <=???
        beta_hat[i] = beta[i] - alpha[i] * beta_hat[order];
    }

    y = 0;
}

void AnalogFilter::clean_F() {
    for (int i = 0; i < order; i++) {
        f[i] = 0.0;
    }

    y = 0;
}

void AnalogFilter::clean() {
    for (int i = 0; i < order; i++) {
        x[i] = 0.0;
        x_old[i] = 0.0;
        f[i] = 0.0;
    }

    y = 0;
}

// Fn = f[order - 1] -> E.g.: order = 2 -> F2 = f[1]; X2 = x[1]
double AnalogFilter::process(double input, double h) {
    clean_F();

    for (int i = 0; i < order; i++) {
        f[order - 1] = f[order - 1] - alpha[i] * x[i];
    }

    f[order - 1] = f[order - 1] + input;

    //integration of the last equation
    x[order - 1] = x_old[order - 1] + h * f[order - 1];
    x_old[order - 1] = x[order - 1];

    y = beta_hat[order - 1] * x[order - 1] + beta_hat[order] * input;

    // the cycle runs backward to obtain a semi-implicit integration
    for (int i = order - 2; i >= 0; i--) {
        f[i] = x[i + 1];
        x[i] = x_old[i] + h * f[i];
        x_old[i] = x[i];

        y = y + beta_hat[i] * x[i];
    }

    return y;
}


AnalogFilter* AnalogFilter::getDifferentiatorHz(double f)
{
	double b[2] = {1.0, 0.0}; //num
    double a[2] = {1.0/(2*M_PI*f), 1.0}; // den
    return new AnalogFilter(1,a,b);
}

AnalogFilter* AnalogFilter::getDifferentiatorHz2(double f)
{
    double wn = 2*M_PI*f;
	double b[3] = {0.0, wn*wn, 0.0}; //num
    double a[3] = {1.0, 2*wn, wn*wn}; // den
    return new AnalogFilter(2,a,b);
}