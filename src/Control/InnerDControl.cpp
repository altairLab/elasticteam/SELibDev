#include "Control/InnerDControl.hpp"

/****************** PinnerD ************************/

selibdev::PinnerD::PinnerD(selibdev::ISEHardware *hw) :
    selibdev::SEControl(hw),
    kP(0.0),
    kd(0.0),
    ff(1.0),
    _err(0.0)
{
    addEntryDouble("err", &_err);
}

selibdev::PinnerD::PinnerD(double kP, double kd, selibdev::ISEHardware *hw) :
    selibdev::SEControl(hw),
    kP(kP),
    kd(kd),
    ff(1.0),
    _err(0.0)
{
    double alpha = kd;
    double alpha_comp = 0.5; // 1.0 / 2.0*M_PI*0.5;

    double b[2] = {alpha, 1.0};
    double a[2] = {alpha_comp, 1.0};

    pz_comp = new selibdev::AnalogFilter(1, a, b);
    
    addEntryDouble("err", &_err);
}

double selibdev::PinnerD::___process(double dt)
{
    _err = tau - ref;

    out = ff * ref - kP * _err - kd * dtau;
    // out = pz_comp->process(out, dt);
    
    return out;
}

std::string selibdev::PinnerD::cb_getLoggerEntityNameString()
{ return "PinnerD"; }

std::string selibdev::PinnerD::cb_getLoggerPropertiesString()
{ 
    std::ostringstream strs;
    strs << "kP" << kP << "_kd" << kd << "_ff" << ff; 
    return strs.str(); 
}

/****************** SMCinnerD ***********************/

selibdev::SMCinnerD::SMCinnerD(double kd, double lambda, selibdev::ISEHardware* hw)
    : selibdev::SlidingModeForceControlBase(lambda, hw)
{
	this->kd = kd; // this is used for and inner derivative loop which reduces the system to a first order dominant pole
    this->alpha = kd;
    this->kp = - (1 - alpha * lambda);
    
    debug_info("kp:" << kp << "  kd:" << kd);
}

double selibdev::SMCinnerD::slidingSurface(double err, double derr)
{
    // if(fabs(s) > 2*phi) ierr = -err/lambda1;//to avoid the reaching phase
    // else if (__sign(err) != __sign(err_prev)) ierr = 0;
    err_prev = err;
    ierr += err*TS;
    // s = err + lambda1 * ierr;
    s = tau + lambda1 * ierr;
    return s;
}

double selibdev::SMCinnerD::controlLaw()
{

    //sliding strategy
    us = __sign(s);
    //continuous approx
    if(fabs(s) < phi) us =  s / phi;

    out = -kp*err + ref - ni*us;
    out = out - kd*dtau; //conceptually this reduces the system to a first order dominant pole
    return out;
}

std::string selibdev::SMCinnerD::cb_getLoggerEntityNameString()
{ return "SEL_SMCinnerD"; }

std::string selibdev::SMCinnerD::cb_getLoggerPropertiesString()
{ 
    std::ostringstream strs;
    strs << "kp" << kp << "_kd" << kd << "_ni" << ni << "_phi" << phi; 
    return strs.str(); 
}

/****************** PinnerPD ************************/

selibdev::PinnerPD::PinnerPD(selibdev::ISEHardware *hw) :
    selibdev::SEControl(hw),
    kP(0.0),
    kp(0.0),
    kd(0.0),
    ff(1.0),
    _err(0.0)
{
    // ntd
}

selibdev::PinnerPD::PinnerPD(double kP, double kp, double kd, selibdev::ISEHardware *hw) :
    selibdev::SEControl(hw),
    kP(kP),
    kp(kp),
    kd(kd),
    ff(1.0),
    _err(0.0)
{
    // ntd
}

double selibdev::PinnerPD::___process(double dt)
{
    _err = tau - ref;

    out = ff * ref - kP * _err;
    out = out - kd * dtau - kp * tau;
    return out;
}

std::string selibdev::PinnerPD::cb_getLoggerEntityNameString()
{ return "PinnerPD"; }

std::string selibdev::PinnerPD::cb_getLoggerPropertiesString()
{ 
    std::ostringstream strs;
    strs << "kP" << kP << "kp" << kp << "_kd" << kd << "_ff" << ff; 
    return strs.str(); 
}